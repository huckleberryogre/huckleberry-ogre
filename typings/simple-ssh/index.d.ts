/**
 * From https://www.npmjs.com/package/simple-ssh
 */
declare module 'simple-ssh' {
    /**
     * Execute command options.
     *
     * @prop [args] Additional command line arguments (default: null).
     * @prop [in] Input to be sent to stdin.
     * @prop [out] stdout handler.
     * @prop [err] stderr handler.
     * @prop [exit] Exit handler.
     * @prop [pty] Allocates a pseudo-tty, useful for command which require sudo (default: false).
     */
    export interface IExecOptions {
        args?: string[];
        in?: string;
        out?: (stdout?: string) => void;
        err?: (stdout?: string) => void;
        exit?: (code?: string, stdout?: string, stderr?: string) => void;
        pty?: boolean
    }

    /**
     * Constructor options.
     *
     * @prop [host] Hostname.
     * @prop [port] Port number (default: 22).
     * @prop [user] Username.
     * @prop [pass] Password.
     * @prop [timeout] Connection timeout in milliseconds (default: 10000).
     * @prop [key] SSH key.
     * @prop [passphrase] Passphrase.
     * @prop [baseDir] Base directory. If this is set, each command will be preceeded by cd ${this.baseDir}.
     * @prop [agent] Connects with the given SSH agent. If this is set, no need to specify a private key or password.
     * @prop [agentForward] Set to true to connect with agent forwarding.
     */
    export interface ISSHClientOptions {
        host?: string;
        port?: number;
        user?: string;
        pass?: string;
        timeout?: number;
        key?: string;
        passphrase?: string;
        baseDir?: string;
        agent?: string;
        agentForward?: boolean;
    }

    /**
     * @prop success Called on successful connection
     * @prop fail Called if the connection failed
     */
    export interface IStartOptions {
        success: () => void;
        fail: (err?: string) => void;
    }

    /**
     * Ssh client.
     *
     * @prop exec Adds a command to the stack.
     * @prop start Starts executing the commands.
     */
    class SSHClient {
        constructor(options: ISSHClientOptions);

        /**
         * Adds a command to the stack.
         *
         * @prop command Command to be executed.
         * @prop [options] Options.
         */
        exec: (command: string, options?: IExecOptions) => SSHClient;
        /**
         * Starts executing the commands
         */
        start: (options?: IStartOptions) => void;
        /**
         * Ends the SSH session (this is automatically called at the end of a command queue).
         */
        end: () => void;
    }

    export default SSHClient;
}

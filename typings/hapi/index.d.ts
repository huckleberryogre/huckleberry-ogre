import * as catbox from 'catbox';

declare module 'hapi' {
    interface ApplicationState {
        cache?: catbox.Policy;
    }
}


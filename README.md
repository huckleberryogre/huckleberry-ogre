### Huckleberry Ogre playground website

***
#### To run application:
1. `npm run install`
2. `npm run start`

website available on **http://localhost:3001**
***

#### To run separately:
* client (webpack watch service): ```npm run client```

* server: ```npm run server-dev```

***
* some global dependencies that used to be required before:
```
npm i typescript -g
npm i ts-node -g
npm i webpack -g
```
* you may also need ffmpeg which is used to convert gif to webm, get it here http://www.ffmpeg.org/

#### For windows users:
* if node-gyp fails to find Python, run powershell as administrator and execute:
```
npm i -g --production windows-build-tools
```
* if you're behind corporate proxy, having "self signed certificate in certificate chain" error, this will help:
```
npm config set ca "";
npm config set strict-ssl false;
set NODE_TLS_REJECT_UNAUTHORIZED=0
```
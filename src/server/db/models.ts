import {DataTypes, Model, Sequelize} from 'sequelize';
import {IUser} from "../../models";

/**
 * Blog post model definition.
 *
 * @param {Sequelize} sequelize Sequelize instance.
 */
export const getPostModel = (sequelize: Sequelize) => sequelize.define(
    'post',
    {
        title: DataTypes.STRING,
        message: {type: DataTypes.TEXT, validate: {notEmpty: true}},
    },
    {modelName: 'post', paranoid: true}
);

/**
 * File model definition.
 *
 * @param {Sequelize} sequelize Sequelize instance.
 */
export const getFileModel = (sequelize: Sequelize) => sequelize.define(
    'file',
    {
        name: {type: DataTypes.STRING, validate: {notEmpty: true}},
        uuid: {type: DataTypes.UUID, validate: {notEmpty: true}},
        hash: {type: DataTypes.STRING, validate: {notEmpty: true}},
        meta: DataTypes.STRING,
    }
);

export type TUserModel = Model<IUser, Partial<IUser>>;

/**
 * User model definition.
 *
 * @param {Sequelize} sequelize Sequelize instance.
 */
export const getUserModel = (sequelize: Sequelize) => sequelize.define<TUserModel>(
    'user',
    {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        avatarURL: DataTypes.STRING,
        googleId: DataTypes.STRING,
        role: {type: DataTypes.STRING, validate: {notEmpty: true}},
        uuid: {type: DataTypes.STRING, validate: {notEmpty: true}}
    }
);

/**
 * User model definition.
 *
 * @param {Sequelize} sequelize Sequelize instance.
 */
export const getTagModel = (sequelize: Sequelize) => sequelize.define(
    'tag',
    {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        }
    }, {modelName: 'tag'}
);

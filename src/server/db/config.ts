import {Options} from 'sequelize';
import path from 'path';
import {writeFileSync, readFileSync} from 'fs';
import {AppConfig} from '../../server.config';
import chalk from 'chalk';

const {
    IS_PRODUCTION,
    DB_NAME,
    DB_PRODUCTION_FOLDER,
} = AppConfig;

const storage = IS_PRODUCTION ?
    `${DB_PRODUCTION_FOLDER}${DB_NAME}` :
    path.resolve(`./src/server/db/sqlite/${DB_NAME}`);

try {
    readFileSync(storage);
    console.log(chalk.green('Database found'));
} catch (e) {
    console.log(chalk.yellow('No database found on disk'));

    try {
        writeFileSync(storage, Buffer.alloc(0, ''), {flag: 'a'});
        console.log(chalk.green('New database successfully created'));
    } catch (error) {
        console.log(chalk.red('Failed creating database. Error:'));
        throw error;
    }
}

export const DB_CONNECTION_OPTIONS: Options = {
    dialect: 'sqlite',
    storage,
    dialectOptions: {mode: 2},
    logging: (...msg) => {
        console.log(msg[0])
    }
};

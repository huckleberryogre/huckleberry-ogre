import {Sequelize} from 'sequelize';
import {DB_CONNECTION_OPTIONS} from './config';
import {
    getPostModel,
    getFileModel,
    getUserModel,
    getTagModel,
} from './models';
import {umzug} from "./migrations";

/**
 * Sequelize ORM instance.
 */
const sequelize = new Sequelize(DB_CONNECTION_OPTIONS);

/**
 * Sequelize models importing.
 */
export const Posts = getPostModel(sequelize);
export const Files = getFileModel(sequelize);
export const Users = getUserModel(sequelize);
export const Tags = getTagModel(sequelize);

Posts.belongsToMany(Tags, {through: 'PostsTags'});
Tags.belongsToMany(Posts, {through: 'PostsTags'});

const synchronise = () => sequelize.sync().then(
        () => console.log(`db synchronised successful`),
        (error: string) => console.log(`db synchronise error: \n\n${error}`)
    );

/**
 * Make sure migrations is done, then synchronise.
 */
umzug(sequelize)
    .then(synchronise)
    .catch((error) => {
    console.error(error);
    // attempting to synchronise for the case when db has to run for the first time.
    return synchronise();
});

export {sequelize};


import Umzug from 'umzug';
import Sequelize from 'sequelize';
import path from "path";
import {existsSync, mkdirSync} from "fs";
import chalk from 'chalk';

const migrationsDirectory = path.resolve(__dirname, './migrations');

try {
    if (!existsSync(migrationsDirectory)) {
        mkdirSync(migrationsDirectory);
    }
} catch (e) {
    console.log(chalk.red('Failed creating migrations directory'));
    console.error(e);
}


/**
 * Smart migrations performer.
 *
 * @param sequelize Sequelize instance.
 */
export const umzug = (sequelize: Sequelize.Sequelize) => {
    const umzugInstance = new Umzug({
        storage: 'sequelize',
        storageOptions: {
            sequelize
        },
        migrations: {
            params: [
                sequelize.getQueryInterface(),
                sequelize.getDialect()
            ],
            path: path.resolve(__dirname, './migrations'),
            pattern:  /^\d+[\w-]+\.ts$/
        },
        logging: function () {
            console.log.apply(null, arguments);
        },
    });

    const logUmzugEvent = (eventName) => (name) => console.log(`${name} ${eventName}`);

    umzugInstance.on('migrating', logUmzugEvent('migrating'));
    umzugInstance.on('migrated', logUmzugEvent('migrated'));
    umzugInstance.on('reverting', logUmzugEvent('reverting'));
    umzugInstance.on('reverted', logUmzugEvent('reverted'));

    return umzugInstance.up();
};


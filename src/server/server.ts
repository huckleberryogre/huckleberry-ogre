'use strict';
import Hapi, {Request, ServerOptions} from '@hapi/hapi';
import fs from "fs";
import Bell from '@hapi/bell';
import inert from '@hapi/inert';
import HapiSentryPlugin from 'hapi-sentry';
import {HomeRoute} from './routes/home';
import {BlogRoute} from './routes/blog';
import {FilesRoute} from './routes/files/files';
import {AppConfig} from '../server.config';
import {LoginRoute} from './routes/login';
import AuthCookie from '@hapi/cookie';
import hapiGate from 'hapi-gate';
import {ERole} from "./enums";
import {isBoom} from "./utils";
import chalk from 'chalk';

const {
    SERVER_HOSTNAME,
    SERVER_PORT,
    SERVER_HTTPS_PORT,
    AUTHENTICATION,
    IS_GOOGLE_CREDENTIALS_PROVIDED,
    IS_PRODUCTION,
    LOG_REQUESTS,
    LETS_ENCRYPT_TLS_CERT_PATH,
    LETS_ENCRYPT_TLS_KEY_PATH,
    SENTRY_CONNECTION_STRING,
} = AppConfig;

/**
 * Hapi server options.
 */
const serverOptions: ServerOptions = {
    port: SERVER_PORT,
    host: SERVER_HOSTNAME,
};

/**
 * Hapi server instance.
 */
const server = new Hapi.Server(serverOptions);

/**
 * Server starter.
 */
const start = async (server, isHTTP?: boolean) => {

    await server.register(inert);

    let isBellRegistered = false;

    if (IS_GOOGLE_CREDENTIALS_PROVIDED) {
        try {
            await server.register(Bell);
            isBellRegistered = true;
        } catch (error) {
            console.log(chalk.red('Failed registering bell plugin'));
            console.error(error);
        }
    } else {
        console.log(chalk.red('Failed registering bell plugin, check google authenticator credentials in your .env file'));
    }

    await server.register(AuthCookie);

    if (isBellRegistered) {
        server.auth.strategy('google', 'bell', AUTHENTICATION.GOOGLE);
    }

    server.auth.strategy('session', 'cookie', AUTHENTICATION.SESSION);

    await server.register({
        plugin: require('hapijs-status-monitor'), options: {
            title: 'HBOgre status monitor',
            routeConfig: {
                auth: {
                    strategy: 'session',
                    scope: ERole.ADMIN
                }
            }
        }
    },);

    if (isHTTP && IS_PRODUCTION) {
        /** redirects all http to https */
        await server.register({
            ...hapiGate,
            options: {
                https: true
            }
        });
    }

    if (!!SENTRY_CONNECTION_STRING) {
        await server.register({
            plugin: HapiSentryPlugin,
            options: {
                client: {
                    dsn: SENTRY_CONNECTION_STRING,
                    sampleRate: IS_PRODUCTION ? 0.5 : 1.0,
                },
            },
        });
    }

    [HomeRoute, LoginRoute, BlogRoute, FilesRoute].forEach(route => route(server));

    const preResponse = function (request, reply) {
        const response = request.response;

        if (response.isBoom && response.output.statusCode === 500) {
            // setting payload explicitly is the only option to alter 500 error message
            response.output = {
                ...response.output,
                payload: {
                    error: "Internal Server Error",
                    message: response.message,
                    statusCode: 500,
                },
            };

            return response;
        } else {
            return reply.continue;
        }
    };

    server.ext('onPreResponse', preResponse);

    await server.start();
    const cyanString = '\x1b[36m%s\x1b[0m';
    console.log(cyanString, `\nServer running at: ${server.info.uri}\n`);
};

start(server, true);

if (IS_PRODUCTION) {
    const httpsServerOptions: ServerOptions = {
        host: SERVER_HOSTNAME,
        port: SERVER_HTTPS_PORT,
        tls: {
            cert: fs.readFileSync(LETS_ENCRYPT_TLS_CERT_PATH),
            key: fs.readFileSync(LETS_ENCRYPT_TLS_KEY_PATH),
        }
    };
    const httpsServer = new Hapi.Server(httpsServerOptions);

    start(httpsServer);
}

/**
 * Catching errors.
 */
process.on('unhandledRejection', (err) => {
    console.log(err);
});

/**
 * Optional requests logging. Uncomment on demand.
 */
if (!IS_PRODUCTION && LOG_REQUESTS === 'true') {
    server.events.on('response', (request: Request) => {
        const {info: {remoteAddress}, method, path, response} = request;
        const statusCode = isBoom(response) ? '' : ' --> ' + response.statusCode;

        console.log(remoteAddress + ': ' + method.toUpperCase() + ' ' + path + statusCode);
    });
}

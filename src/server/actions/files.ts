import {promisify} from "util";
import {readFile as fsReadFile, unlink} from "fs";
import {IFile} from "../../models";
import {Files, sequelize} from "../db";
import path from "path";
import {PATH} from "../paths";
import sharp from "sharp";
import {changeFileExtension, isConvertibleImage} from "../routes/files/utils";
import {toWebp} from "../routes/files/toWebp";
import {Model} from "sequelize";

const readFile = promisify(fsReadFile);

/**
 * Actions set for File model.
 */
export class FileActions {

    public static getFileInfo = async (findFileOptions: Partial<IFile>) => {
        let file;

        try {
            file = await Files.findOne({where: findFileOptions});
        } catch (error) {
            console.error(error);
            throw error;
        }

        return file;
    };

    public static getFile = async (uuid, format = 'webp') => {
        if (!uuid) {
            throw Error('no uuid provided');
        }

        let file;

        try {
            const filePath = path.join(PATH.FILES, uuid);
            file = await readFile(filePath);
        } catch (error) {
            console.error(error);
            throw error;
        }

        try {
            if (format === 'png') {
                file = await sharp(file)
                    .withMetadata()
                    .png()
                    .rotate()
                    .toBuffer();
            }
        } catch (error) {
            console.error(error);
            throw error;
        }

        return file;
    };

    public static getAllFiles = () => Files.findAll();

    public static newFile = (uuid: string, fileName: string, fileHash: string, meta: string) => Files.create({
        uuid,
        name: fileName,
        hash: fileHash,
        meta
    });

    public static deleteFile = (uuid: string) => Files.destroy({where: {uuid}});

    public static updateFile = (file: Partial<IFile>, findFileOptions: Partial<IFile>) => Files.update(file, {where: findFileOptions});

    public static convertToWebp = async (entry, fileName: string, uuid: string, fileExtension: string) => {
        let fileEntry = entry;
        const oldUuid = `${uuid}.${fileExtension}`;
        const newUuid = changeFileExtension(oldUuid, 'webp');
        const originalFilePath = `${PATH.FILES}/${oldUuid}`;
        const outputFilePath = `${PATH.FILES}/${newUuid}`;

        try {
            if (isConvertibleImage(fileName)) {
                await toWebp(originalFilePath, outputFilePath);

                fileEntry = await sequelize.transaction<Model<IFile>>(async () => {
                    let entry = await FileActions.getFileInfo({uuid: oldUuid});

                    const file = entry.get();

                    file.uuid = newUuid;
                    file.name = changeFileExtension(file.name, 'webp');

                    await FileActions.updateFile(file, {uuid: oldUuid});

                    return await FileActions.getFileInfo({uuid: newUuid});
                });
            }
        } catch (error) {
            // removing any output files due to failed attempt
            unlink(outputFilePath, console.error);
            throw Error('failed converting image to webp');
        }

        return fileEntry;
    };

    public static makeThumbnail = (fileUuid: string) => {
        const originalFilePath = `${PATH.FILES}/${fileUuid}`;
        const thumbnailPath = `${PATH.THUMBNAILS}/${fileUuid}`;

        return sharp(originalFilePath)
            .resize(
                400,
                250,
                {
                    fit: "inside",
                    background: {r: 255, g: 255, b: 255, alpha: 1},
                    withoutEnlargement: true
                }
            ).toFile(thumbnailPath);
    }
}
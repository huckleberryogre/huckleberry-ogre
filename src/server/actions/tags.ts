import {ITag} from "../../models";
import {Tags} from "../db";

/**
 * Actions set for Tag model.
 */
export class TagActions {

    public static getOne = (findTagOptions?: Partial<ITag>) => Tags.findOne({where: findTagOptions});

    public static getAll = () => Tags.findAll({attributes: ['id', 'name']});

    public static create = (createTagOptions: Partial<ITag>) => Tags.create(createTagOptions);

    public static delete = (searchParameters: Partial<ITag>) => Tags.destroy({where: searchParameters});
}
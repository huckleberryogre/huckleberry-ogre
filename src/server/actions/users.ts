import {IUser} from "../../models";
import {Users} from "../db";
import {TUserModel} from "../db/models";

/**
 * Actions set for User model.
 */
export class UserActions {

    public static getUser = (findUserOptions?: Partial<IUser>) => Users.findOne<TUserModel>({where: findUserOptions});

    public static getAllUsers = () => Users.findAll();

    public static newUser = (createUserOptions: Partial<IUser>) => Users.create(createUserOptions);

    public static deleteUser = (searchParameters: Partial<IUser>) => Users.destroy({where: searchParameters});
}
import {IPost, IPostExtended, ITag} from "../../models";
import {Posts, Tags} from "../db";
import {Op} from "sequelize";

/**
 * Actions set for Post model.
 */
export class PostActions {

    public static getAllPosts = async (findPostOptions: Partial<IPost>) => {
        let posts = [];

        try {
            posts = await Posts.findAll(
                {
                    where: findPostOptions,
                    include: [{
                        model: Tags,
                        attributes: ['id', 'name'],
                        through: {attributes: []},
                    }]
                }
            )
        } catch (error) {
            console.error(error);
            throw Error(error);
        }

        return posts;
    };

    public static newPost = async (post: Partial<IPost> & { tags: ITag[] }) => {
        let newPost;
        let tags;

        try {
            tags = await Tags.bulkCreate(post.tags || [], {returning: true, ignoreDuplicates: true});
        } catch (error) {
            console.error(error);
            throw Error('failed creating tags');
        }


        delete post.tags;

        try {
            newPost = await Posts.create(post);
        } catch (error) {
            console.error(error);
            throw Error(`failed creating new post`);
        }

        try {
            if (newPost && tags) {
                newPost.setTags(tags)
            }
        } catch (error) {
            console.error(error);
            throw Error('failed setting tags on post');
        }

        return newPost;
    };

    public static deletePost = (id: number) => Posts.destroy({where: {id}});

    public static updatePost = async (post: Partial<IPostExtended>) => {
        const postInstance = await Posts.findOne({
            where: {id: post.id}
        });

        if (!postInstance) {
            throw Error(`post with id ${post.id} not found`);
        } else {
            let tags = [];
            const postTags = post.tags || [];

            try {
                await Tags.bulkCreate(postTags || [], {returning: true, ignoreDuplicates: true});
                tags = await Tags.findAll({
                    where: {
                        name: {
                            [Op.in]: postTags.map(tag => tag.name)
                        }
                    }
                });
            } catch (error) {
                console.error(error);
                throw Error('failed creating tags');
            }

            try {
                // @ts-ignore
                // todo: fix types via guide https://sequelize.org/master/manual/typescript.html
                postInstance.setTags(tags);
            } catch (error) {
                console.error(error);
                throw Error('failed updating post instance');
            }

            delete post.tags;

            try {
                return postInstance.update(post);
            } catch (error) {
                console.error(error);
                throw Error('failed updating post instance');
            }

        }
    };
}
import {Server, ServerRoute, RequestQuery, Request, ResponseToolkit, AuthCredentials} from '@hapi/hapi';
import {AppConfig, AuthCredentialsSession} from '../../server.config';
import {TUserModel} from '../db/models';
import {generateUUID} from '../utils';
import Boom = require('boom');
import {ERole} from "../enums";
import {UserActions} from "../actions";

const {IS_GOOGLE_CREDENTIALS_PROVIDED} = AppConfig;

/**
 * Google authentication response object.
 */
export interface AuthCredentialsGoogle extends AuthCredentials {
    expiresIn: number;
    query: RequestQuery;
    profile: {
        displayName: string;
        email: string;
        id: string;
        name: {
            // given_name === first name
            given_name: string;
            family_name: string;
        }
        raw: {
            sub: string;
            name: string;
            given_name: string;
            family_name: string;
            profile: string;
            picture: string;
            email: string;
            email_verified: boolean;
            locale: string;
        }
    }
    provider: 'google';
    token: string;
}

/**
 * Common handler for login response.
 *
 * @param request Request object.
 * @param handler ResponseToolkit.
 * @param user User instance.
 * @param redirectTo Redirection string.
 */
const handleLoginResponse = (request: Request, handler: ResponseToolkit, user: TUserModel, redirectTo) => {
    request.cookieAuth.set('sid', user.get().uuid);

    return handler.redirect(redirectTo);
};

const googleSignInRoute = IS_GOOGLE_CREDENTIALS_PROVIDED && {
    method: 'GET',
    path: '/rest/login/google',
    options: {
        auth: 'google',
    },
    handler: (request, handler) => {
        const credentials = request.auth.credentials as AuthCredentialsGoogle;
        const queryParams = credentials.query as RequestQuery;
        const redirectTo = (queryParams.redirectTo || '/') as string;


        return UserActions.getUser({googleId: credentials.profile.id})
            .then(user => {
                if (!user) {
                    return UserActions.getUser().then(
                        firstFoundUser => {
                            const {displayName: name, email, raw: {picture: avatarURL}, id: googleId} = credentials.profile;
                            /** if no user found, then first who log in becomes admin */
                            const role = firstFoundUser ? ERole.USER : ERole.ADMIN;
                            const uuid = generateUUID();

                            return UserActions.newUser({name, email, avatarURL, googleId, role, uuid})
                                .then(user => handleLoginResponse(request, handler, user, redirectTo));
                        }
                    );
                } else {
                    return handleLoginResponse(request, handler, user, redirectTo);
                }
            }).catch(
                error => {
                    request.log('login', error.stack);

                    return Boom.boomify(error)
                }
            );
    }
};

/**
 * Login with google route configuration.
 */
const routes: ServerRoute[] = [
    {
        method: 'GET',
        path: '/rest/login/session',
        options: {
            auth: 'session',
        },
        handler: (request, handler) => {
            const {user} = request.auth.credentials as AuthCredentialsSession;
            const userObject = user.get();

            delete userObject.uuid;
            delete userObject.googleId;

            return handler.response(userObject);
        }
    },
    {
        method: 'GET',
        path: '/rest/logout',
        options: {
            auth: 'session',
        },
        handler: (request, handler) => {
            request.cookieAuth.clear();

            return handler.response();
        }
    }
];

if (IS_GOOGLE_CREDENTIALS_PROVIDED) {
    routes.push(googleSignInRoute);
}

export const LoginRoute = (server: Server) => server.route(routes);

import Boom from 'boom';
import {RequestQuery, Server, ServerRoute} from '@hapi/hapi';
import {isNaN, isNumber} from 'lodash';
import {ERole} from "../enums";
import {IPost} from "../../models";
import {PostActions, TagActions} from "../actions";

/**
 * Blog routes configuration.
 */
const routes: ServerRoute[] = [
    {
        method: 'GET',
        path: '/rest/blog',
        handler: (request, handler) => {
            const findPostOptions: Partial<IPost> = JSON.parse(
                (request?.query as RequestQuery)?.payload as string || '{}'
            );

            return PostActions.getAllPosts(findPostOptions)
                .then(
                    (posts) => handler.response(posts),
                    (error) => handler.response(error)
                );
        }
    },
    {
        method: 'GET',
        path: '/rest/tags',
        handler: (_, handler) => {
            return TagActions.getAll()
                .then(
                    (tags) => handler.response(tags),
                    (error) => handler.response(error)
                );
        }
    },
    {
        method: 'POST',
        path: '/rest/blog',
        options: {
            auth: {
                strategy: 'session',
                scope: [ERole.ADMIN]
            }
        },
        handler: async (request, handler) => {
            const post = JSON.parse(`${request.payload}`);
            let newPost;

            if (post) {
                try {
                    newPost = await PostActions.newPost(post);
                    return handler.response(newPost);
                } catch (error) {
                    console.error(error);
                    return Boom.internal(error);
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/rest/blog',
        options: {
            auth: {
                strategy: 'session',
                scope: ERole.ADMIN
            }
        },
        handler: (request) => {
            const findPostOptions = JSON.parse(`${request.payload}`);

            return PostActions.updatePost(findPostOptions);
        }
    },
    {
        method: 'DELETE',
        path: '/rest/blog',
        options: {
            auth: {
                strategy: 'session',
                scope: ERole.ADMIN
            }
        },
        handler: async (request, handler) => {
            const payload: Partial<IPost> = JSON.parse(`${request.payload}`);
            const id = payload && +payload.id;

            if (isNumber(id) && !isNaN(id)) {
                return PostActions.deletePost(id)
                    .then(
                        (rowsAmount) => handler.response({deletedRows: rowsAmount}),
                        (error) => Boom.badImplementation('Delete post failed for some reason', error)
                    );
            } else {
                return Boom.badRequest('post id must be supplied to perform removal');
            }
        }
    }
];

export const BlogRoute = (server: Server) => server.route(routes);

import sharp from 'sharp';
import {unlink} from 'fs';
import {PATH} from "../../paths";
import {changeFileExtension} from "./utils";
import {sequelize} from "../../db";
import {Model} from "sequelize";
import {IFile} from "../../../models";
import {FileActions} from "../../actions";

/**
 * Sharp converted function.
 *
 * @param originalFilePath Absolute original file path.
 * @param outputFilePath Absolute output file path.
 */
export const toWebp = (originalFilePath: string, outputFilePath: string) => sharp(originalFilePath)
    .withMetadata()
    .webp({quality: 75})
    .rotate()
    .toFile(outputFilePath);

/**
 * Converts images from allowed formats to webp.
 *
 * @param fileUuid File uuid name (the real filename on disk).
 */
export const convertToWebp = (fileUuid: string) => {
    const newUuid = changeFileExtension(fileUuid, 'webp');
    const originalFilePath = `${PATH.FILES}/${fileUuid}`;
    const outputFilePath = `${PATH.FILES}/${newUuid}`;

    return toWebp(originalFilePath, outputFilePath)
        .then(() => {
            return sequelize.transaction<Model<IFile>>(() => {
                return FileActions.getFileInfo({uuid: fileUuid}).then(fileInstance => {
                    const file = fileInstance.get();

                    file.uuid = newUuid;
                    file.name = changeFileExtension(file.name, 'webp');

                    return FileActions.updateFile(file, {uuid: fileUuid})
                        .then(() => FileActions.getFileInfo({uuid: newUuid}))
                })
            });
        }).then((entry) => {
            unlink(originalFilePath, console.error);
            return entry;
        }).catch((error) => {
            console.error(error);
            unlink(outputFilePath, console.error);

            return error;
        })
};

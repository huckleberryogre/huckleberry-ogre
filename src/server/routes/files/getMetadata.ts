import {ExifData} from "exif";
import exif from 'exif-reader';
import sharp, {Metadata, Sharp} from 'sharp';
import {Buffer} from 'buffer';

export interface ParsedMetadata extends Omit<Metadata, 'exif'> {
    exif: ExifData;
}

/**
 * Returns metadata parsed by sharp.
 *
 * @param file Either path to a file or a Sharp instance.
 */
export const getMetadata = async (file: string | Sharp): Promise<ParsedMetadata> => {
    const sharpInstance = typeof file === 'string' ? sharp(file) : file;
    let metadata;

    try {
        metadata = await sharpInstance.metadata();
    } catch (error) {
        console.error(error);
        throw Error('failed parsing file metadata');
    }

    return {
        ...metadata,
        exif: Buffer.isBuffer(metadata.exif) && exif(metadata.exif) || {}
    }
};

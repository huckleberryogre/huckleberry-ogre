/**
 * Changes file extension.
 *
 * @param fileName File name.
 * @param newExtension Given new extension.
 */
export const changeFileExtension = (fileName: string, newExtension: string) => {
    const dotPosition = fileName.lastIndexOf('.');
    let newFileName;

    if (dotPosition === -1) {
        newFileName = `${fileName}.${newExtension}`;
    } else {
        newFileName = `${fileName.slice(0, dotPosition)}.${newExtension}`;
    }

    return newFileName;
};

/**
 * Returns file extension.
 *
 * @param fileName File name.
 */
export const getFileExtension = (fileName: string) => {
    const dotPosition = fileName.lastIndexOf('.');
    let extension;

    if (dotPosition === -1 || !fileName) {
        extension = '';
    } else {
        extension = fileName.substring(fileName.lastIndexOf('.') + 1)
    }

    return extension;
};

/**
 * Image extensions that can be converted to webp by sharpjs.
 */
export const convertibleImageExtensions = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'svg'];

/**
 * Returns true if image can be converted to webp by sharp.
 *
 * @param fileName File name.
 */
export const isConvertibleImage = (fileName: string) => {
    return convertibleImageExtensions.includes(getFileExtension(fileName).toLocaleLowerCase());
};

/**
 * Formats available for thumbnail creation.
 */
export const thumbnailFormats = ['jpg', 'jpeg', 'png', 'svg', 'gif', 'tiff', 'webp'];

/**
 * Returns true if thumbnail can be created for image.
 *
 * @param fileName File name.
 */
export const isThumbnailFormat = (fileName: string) => {
    return thumbnailFormats.includes(getFileExtension(fileName).toLocaleLowerCase());
};

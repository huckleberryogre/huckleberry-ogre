import {RequestQuery, Server, ServerRoute} from '@hapi/hapi';
import {isEmpty} from 'lodash';
import {rename as fsRename, readFile as fsReadFile, unlink} from 'fs';
import {promisify} from 'util';
import crypto from 'crypto';
import Boom = require('boom');
import {getMetadata} from './getMetadata';
import {PATH} from "../../paths";
import {getFileExtension, isThumbnailFormat} from "./utils";
import {ERole} from "../../enums";
import {FileActions} from "../../actions";

const rename = promisify(fsRename);
const readFile = promisify(fsReadFile);
const unlinkPromise = promisify(unlink);

/**
 * Payload in case of file output.
 *
 * @prop {string} [file.filename] Filename.
 * @prop {string} [file.path] Path to locally saved file.
 */
interface IFilePayload {
    file?: {
        filename: string;
        path: string;
    }
}

interface IGetFileQuery extends RequestQuery {
    format: string;
}

/**
 * File manager routes configuration.
 */
const routes: ServerRoute[] = [
    {
        method: 'GET',
        path: '/rest/file/{uuid}',
        options: {
            files: {
                relativeTo: PATH.FILES
            }
        },
        handler: async (request, handler) => {
            const uuid = request.params.uuid;
            const {format = 'webp'} = request?.query as IGetFileQuery || {};

            return handler.response(await FileActions.getFile(uuid, format))
                .header('Content-Disposition', 'inline')
                .header('Content-type', `image/${format}`);
        }
    },
    {
        method: 'GET',
        path: '/rest/logs',
        options: {
            auth: {
                strategy: 'session',
                scope: ERole.ADMIN
            },
            files: {
                relativeTo: PATH.ROOT
            }
        },
        handler: (__, handler) => {
            return handler.file(`./logs.txt`);
        }
    },
    {
        method: 'GET',
        path: '/rest/file/thumb/{uuid}',
        options: {
            files: {
                relativeTo: PATH.THUMBNAILS
            }
        },
        handler: async (request, handler) => {
            const uuid = request.params.uuid;
            const {format = 'webp'} = request?.query as IGetFileQuery || {};

            return handler.response(await FileActions.getFile(uuid, format))
                .header('Content-Disposition', 'inline')
                .header('Content-type', `image/${format}`);
        }
    },
    {
        method: 'GET',
        path: '/rest/files/all',
        options: {
            auth: {
                strategy: 'session',
                scope: ERole.ADMIN
            }
        },
        handler: () => FileActions.getAllFiles()
    },
    {
        method: 'POST',
        path: '/rest/file/save',
        options: {
            payload: {
                output: 'file',
                /** 500 mb */
                maxBytes: 524288000,
                uploads: PATH.FILES,
                /**
                 * Payload reception timeout in milliseconds. Sets the maximum time allowed for the client to transmit
                 * the request payload (body) before giving up and responding with a Request Timeout (408) error response.
                 * Set to false to disable.
                 */
                timeout: false,
                multipart: {
                    output: 'file'
                },
            },
            auth: {
                strategy: 'session',
                scope: ERole.ADMIN
            }
        },
        handler: (request, handler) => {
            const payload: IFilePayload = request.payload as object;
            const file = payload && payload.file;
            const fileName = file && file.filename;
            const savedFilePath = file && file.path;
            const uuid = savedFilePath && savedFilePath.replace(`${PATH.FILES}\\`, '').replace(`${PATH.FILES}/`, '');
            const fileExtension = getFileExtension(fileName);
            const unlinkSavedFileCallback = () => unlink(`${PATH.FILES}/${uuid}.${fileExtension}`, console.error);
            let fileHash: string;

            // todo: refactor idea - put everything into action maybe?
            return readFile(savedFilePath)
                .then(file => {
                    fileHash = crypto.createHash('sha256').update(file).digest('hex');
                    return FileActions.getFileInfo({hash: fileHash});
                })
                .then(async (entry) => {
                    if (isEmpty(entry)) {
                        let fileEntry;
                        const savedFilePathWithExtension = `${savedFilePath}.${fileExtension}`;

                        // if file doesn't exist => save it and create new entry
                        try {
                            // we want file extension to be in filename
                            fileExtension && await rename(savedFilePath, savedFilePathWithExtension);
                        } catch (error) {
                            unlinkSavedFileCallback();
                            throw Error('failed renaming file');
                        }

                        try {
                            const metaData = await getMetadata(savedFilePathWithExtension);

                            fileEntry = FileActions.newFile(
                                `${uuid}${fileExtension ? `.${fileExtension}` : ''}`,
                                fileName,
                                fileHash,
                                JSON.stringify(metaData)
                            );
                        } catch (error) {
                            unlinkSavedFileCallback();
                            console.error(error);
                            throw Error('failed creating new file entry');
                        }

                        try {
                            fileEntry = await FileActions.convertToWebp(entry, fileName, uuid, fileExtension);
                            unlinkSavedFileCallback();
                        } catch (e) {
                        }

                        try {
                            isThumbnailFormat(fileName) && await FileActions.makeThumbnail(fileEntry.get().uuid);
                        } catch (error) {
                            throw Error('failed creating thumbnail');
                        }

                        return handler.response(fileEntry || entry);
                    } else {
                        // if file exist => unlink temporary file and return existing entry
                        unlink(savedFilePath, console.error);

                        return handler.response(entry);
                    }
                })
                .catch((error) => {
                    console.error(error);
                    return handler.response(error)
                });
        }
    },
    {
        method: 'DELETE',
        path: '/rest/file/{uuid}',
        options: {
            auth: {
                strategy: 'session',
                scope: ERole.ADMIN
            }
        },
        handler: async (request, handler) => {
            const uuid = request.params.uuid;

            if (uuid) {
                try {
                    await FileActions.deleteFile(uuid);
                } catch (e) {
                    throw Error(`failed removing file entry with uuid: ${uuid}`);
                }

                const filePath = `${PATH.FILES}/${uuid}`;
                const fileThumbnailPath = `${PATH.THUMBNAILS}/${uuid}`;

                try {
                    await unlinkPromise(filePath);
                } catch (e) {
                    console.error(e);
                    throw Error('failed removing file from file system')
                }

                unlink(fileThumbnailPath, console.error);

                return handler.response();
            } else {
                return handler.response(Boom.badData(`can't find file with given uuid: ${uuid}`))
            }
        }
    },
];

export const FilesRoute = (server: Server) => server.route(routes);

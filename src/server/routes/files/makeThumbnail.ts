import sharp from 'sharp';
import {PATH} from "../../paths";

/**
 * Makes thumbnail for given image.
 *
 * @param fileUuid File uuid name (the real filename on disk).
 */
export const makeThumbnail = (fileUuid: string) => {
    const originalFilePath = `${PATH.FILES}/${fileUuid}`;
    const thumbnailPath = `${PATH.THUMBNAILS}/${fileUuid}`;

    return sharp(originalFilePath)
        .resize(
            400,
            250,
            {
                fit: "inside",
                background: {r: 255, g: 255, b: 255, alpha: 1},
                withoutEnlargement: true
            }
        ).toFile(thumbnailPath);
};

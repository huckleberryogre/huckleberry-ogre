import {Boom} from "@hapi/boom";

/**
 * UUID generator. See https://gist.github.com/jed/982883
 */
export const generateUUID = (a?) => a ? (a ^ Math.random() * 16 >> a / 4).toString(16) : ([1e7].toString() + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, generateUUID);

/**
 * Returns true if given object is Boom error.
 */
export const isBoom = (obj: Object): obj is Boom => {
    return (obj as Boom).isBoom === true;
};

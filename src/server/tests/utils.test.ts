import {changeFileExtension} from '../routes/files/utils';

describe('routes:files', () => {
    test('changeFileExtension:fileName', () => {
        expect(changeFileExtension('someName.someReallyLoooooooooongExtension', 'myExt')).toEqual('someName.myExt');
        expect(changeFileExtension('someName.s', 'myExt')).toEqual('someName.myExt');
        expect(changeFileExtension('someName.', 'myExt')).toEqual('someName.myExt');
        expect(changeFileExtension('someName', 'myExt')).toEqual('someName.myExt');
        expect(changeFileExtension('', 'myExt')).toEqual('.myExt');
    });

    test('changeFileExtension:extension', () => {
        expect(changeFileExtension('someName.someReallyLoooooooooongExtension', 'e')).toEqual('someName.e');
        expect(changeFileExtension('someName.s', 'e')).toEqual('someName.e');
        expect(changeFileExtension('someName.', '')).toEqual('someName.');
        expect(changeFileExtension('someName', '')).toEqual('someName.');
        expect(changeFileExtension('', '')).toEqual('.');
    });
});

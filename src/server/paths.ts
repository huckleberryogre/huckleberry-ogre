import path from "path";
import {existsSync, mkdirSync} from 'fs';
import {AppConfig} from "../server.config";

const {FILES_DIRECTORY} = AppConfig;

/**
 * Absolute paths to directories.
 */
export const PATH = {
    ROOT: path.join(__dirname, '../../'),
    FILES: path.join(__dirname, FILES_DIRECTORY),
    THUMBNAILS: path.join(__dirname, `${FILES_DIRECTORY}/thumbnails`),
};

/**
 * Ensures all directories are exist.
 */
for (const pathName in PATH) {
    if (!existsSync(PATH[pathName])) {
        try {
            mkdirSync(PATH[pathName]);
        } catch (e) {
            console.error('failed to create essential folder:');
            throw Error(e);
        }
    }
}

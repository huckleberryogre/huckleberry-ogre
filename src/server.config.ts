//toDo describe interfaces and extract into separate file
import {BellOptions} from '@hapi/bell';
import dotEnvContents from './utils/dotEnvVariables';
import {AuthCredentials, RouteOptionsAccessObject} from '@hapi/hapi';
import hapiAuthCookie, {ValidateResponse} from '@hapi/cookie';
import {Model} from 'sequelize';
import {IUser} from "./models";
import {UserActions} from "./server/actions";

/**
 * Environment level properties.
 *
 * @prop {string} RUN_MODE - dev/prod.
 * @prop {string} SERVER_PORT - server port number.
 * @prop {string} SERVER_HOSTNAME - server hostname.
 * @prop {string} LOG_REQUESTS - enable requests logging for debugging purpose.
 */
const {
    SERVER_PORT,
    SERVER_HTTPS_PORT,
    SERVER_HOSTNAME,
    DB_NAME,
    DB_PRODUCTION_FOLDER,
    RUN_MODE,
    LOG_REQUESTS,
    SERVER_IP,
    SERVER_USER,
    SERVER_PASSWORD,
    GOOGLE_AUTH_PASSWORD,
    GOOGLE_AUTH_CLIENT_ID,
    GOOGLE_AUTH_CLIENT_SECRET,
    SESSION_PASSWORD,
    LETS_ENCRYPT_TLS_CERT_PATH,
    LETS_ENCRYPT_TLS_KEY_PATH,
    SENTRY_CONNECTION_STRING,
    FILES_DIRECTORY,
    SERVER_PROJECT_ROOT_DIRECTORY,
} = dotEnvContents;

interface IAuthenticationStrategies {
    GOOGLE: BellOptions;
    SESSION: hapiAuthCookie.Options;
}

interface ISessionObject {
    sid: string;
}

export type AuthCredentialsSession = Partial<RouteOptionsAccessObject> & {
    user: Model<IUser>
}

/**
 * Server application config.
 *
 * REST_PATH - path to rest service.
 * SERVER_PORT - port number, 80 for production.
 * SERVER_HTTPS_PORT - usually 443, production only.
 * SERVER_HOSTNAME - set real hostname for production.
 * SERVER_ADDRESS - hostname + port.
 * SERVER_REST_ADDRESS - hostname + port + rest path.
 * SERVER_IP - IP address of host.
 * SERVER_USER - Usually a root user.
 * SERVER_PASSWORD - server user's password.
 * LETS_ENCRYPT_TLS_CERT_PATH - secrets for enabling https, production only.
 * LETS_ENCRYPT_TLS_KEY_PATH - secrets for enabling https, production only.
 * LOG_REQUESTS - flag for requests logging.
 * AUTHENTICATION - authentication configurations for each provider.
 */
export class AppConfig {

    static IS_PRODUCTION = (RUN_MODE === 'production');

    /** ToDo: use rest path on server side. */
    static REST_PATH = 'rest';
    static SERVER_PORT = SERVER_PORT;
    static SERVER_HTTPS_PORT = SERVER_HTTPS_PORT;
    static SERVER_HOSTNAME = SERVER_HOSTNAME;
    static SERVER_PROJECT_ROOT_DIRECTORY = SERVER_PROJECT_ROOT_DIRECTORY;

    static DB_NAME = DB_NAME;
    static DB_PRODUCTION_FOLDER = DB_PRODUCTION_FOLDER;

    static FILES_DIRECTORY = FILES_DIRECTORY;

    static SERVER_IP = SERVER_IP;
    static SERVER_USER = SERVER_USER;
    static SERVER_PASSWORD = SERVER_PASSWORD;

    static LETS_ENCRYPT_TLS_CERT_PATH = LETS_ENCRYPT_TLS_CERT_PATH;
    static LETS_ENCRYPT_TLS_KEY_PATH = LETS_ENCRYPT_TLS_KEY_PATH;

    static SENTRY_CONNECTION_STRING = SENTRY_CONNECTION_STRING;

    static LOG_REQUESTS = LOG_REQUESTS;

    static IS_GOOGLE_CREDENTIALS_PROVIDED  = !!(GOOGLE_AUTH_PASSWORD && GOOGLE_AUTH_CLIENT_ID && GOOGLE_AUTH_CLIENT_SECRET);

    static AUTHENTICATION: IAuthenticationStrategies = {
        GOOGLE: {
            provider: 'google',
            password: GOOGLE_AUTH_PASSWORD,
            clientId: GOOGLE_AUTH_CLIENT_ID,
            clientSecret: GOOGLE_AUTH_CLIENT_SECRET,
            isSecure: RUN_MODE === 'production'
        },
        SESSION: {
            cookie: {
                name: 'sid',
                password: SESSION_PASSWORD,
                isSecure: RUN_MODE === 'production',
                path: '/'
            },
            validateFunc: async (__request, session: ISessionObject) => {
                const user = await UserActions.getUser({uuid: session.sid});
                const result: ValidateResponse = {
                    valid: !!user
                };

                if (result.valid) {

                    result.credentials = {
                        user,
                        scope: [user.get('role') as string]
                    } as AuthCredentials;
                }

                return result;
            }
        }
    };
}

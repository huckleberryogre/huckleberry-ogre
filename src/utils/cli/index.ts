import Vorpal from 'vorpal';
import {toWebp} from "./sharp/toWebp";
import {convertToWebp} from "../../server/routes/files/toWebp";
import {makeThumbnail} from "../../server/routes/files/makeThumbnail";
import chalk from 'chalk';
import {PostActions} from "../../server/actions";

/**
 * Cli framework instance.
 */
const vorpal = new Vorpal();

const noop = () => {};

vorpal.command('sharp', 'process images').action(() => {
    return toWebp(vorpal.activeCommand);
});

vorpal.command('meta', 'gets metadata').action(() => {
    return toWebp(vorpal.activeCommand);
});

vorpal.command('imgToWebp', 'convert single image from db to webp').action(() => {
    return convertToWebp('1572620750758-1868-a904078acc980d03.jpg').then(noop);
});

vorpal.command('thumbnail', 'make thumbnail for given uuid').action(() => {
    return makeThumbnail('1572620750758-1868-a904078acc980d03.webp').then(noop);
});

vorpal.command('getAllPosts', 'outputs all posts').action(async () => {
    const posts = await PostActions.getAllPosts({});
    vorpal.log(chalk.yellow('\nAll posts:\n'), JSON.stringify(posts));
    return;
});

vorpal.command('out [str]').action(async (args) => {
    vorpal.log(JSON.stringify(args));
});

vorpal
    .delimiter('hbogre$')
    .show();
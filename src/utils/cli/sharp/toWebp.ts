import sharp, {OutputInfo} from 'sharp';
import {readdir, statSync} from 'fs';
import path from 'path';
import {promisify} from 'util';
import {CommandInstance} from 'vorpal';
import chalk from 'chalk';
import {round} from 'lodash';

const readdirPromise = promisify(readdir);

const FULL_PATH = {
    ORIGINALS: path.join(__dirname, './originals'),
    COMPRESSED: path.join(__dirname, './compressed'),
};

//todo: probably reorganise cli scripts and actions? also add jsDocs
export const toWebp = (activeCommand: CommandInstance) => readdirPromise(FULL_PATH.ORIGINALS).then(async (files) => {
    let totalOriginalSize = 0;
    let totalOutputSize = 0;

    await Promise.all(
        files.map((file) => {
            const originalFilePath = `${FULL_PATH.ORIGINALS}/${file}`;
            const outputFilePath = `${FULL_PATH.COMPRESSED}/${file}.webp`;
            // getMetadata(originalFilePath).then(meta => activeCommand.log(JSON.stringify(meta)));

            return sharp(originalFilePath)
                .withMetadata()
                .webp({quality: 75})
                .rotate()
                .toFile(outputFilePath)
                .then((info: OutputInfo) => {
                    const originalSize = statSync(`${FULL_PATH.ORIGINALS}/${file}`).size;
                    const outputSize = info.size;
                    const originalSizeMb = chalk.blue(round(originalSize/1000000, 2) + 'mb');
                    const outputSizeMb = chalk.blue(round(info.size/1000000, 2) + 'mb');
                    const diff = `${round(outputSize/originalSize*100 - 100, 2)}`;

                    totalOriginalSize += originalSize;
                    totalOutputSize += outputSize;

                    activeCommand.log(`${file}: ${originalSizeMb} => ${outputSizeMb}. Diff: ${+diff < 0 ? chalk.green(diff) : chalk.red(diff)}%`)
                });
        })
    );
    const totalOriginalSizeMb = chalk.blue(round(totalOriginalSize/1000000, 2) + 'mb');
    const totalOutputSizeMb = chalk.blue(round(totalOutputSize/1000000, 2) + 'mb');
    const totalDiff = `${round(totalOutputSize/totalOriginalSize*100 - 100, 2)}`;
    activeCommand.log(chalk.yellow(`Total original size: ${totalOriginalSizeMb}`));
    activeCommand.log(chalk.yellow(`Total output size: ${totalOutputSizeMb}`));
    activeCommand.log(chalk.yellow(`Total diff: ${+totalDiff < 0 ? chalk.green(totalDiff) : chalk.red(totalDiff)}%`));
    activeCommand.log(chalk.yellow('All done.'));

}).catch(err => console.error(err));
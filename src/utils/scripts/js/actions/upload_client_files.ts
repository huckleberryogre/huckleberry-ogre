import chalk from "chalk";
import path from "path";
const FtpDeploy = require("ftp-deploy");
const ftpDeploy = new FtpDeploy();
import {SERVER_IP, SERVER_USER, SERVER_PASSWORD, SERVER_PROJECT_ROOT_DIRECTORY} from "../../../dotEnvVariables";

console.log(__dirname);
const localRootPath = path.join(__dirname, '../../../../../dist');
console.log(localRootPath);

var config = {
    user: SERVER_USER,
    password: SERVER_PASSWORD,
    host: SERVER_IP,
    port: 21,
    localRoot: localRootPath,
    remoteRoot: `${SERVER_PROJECT_ROOT_DIRECTORY}/dist`,
    include: ["**"],
    // delete ALL existing files at destination before uploading, if true
    deleteRemote: true,
    // Passive mode is forced (EPSV command is not sent)
    forcePasv: true,
    // use sftp or ftp
    sftp: false
};

ftpDeploy
    .deploy(config)
    .then(res => console.log(res, chalk.greenBright("Fresh client version uploaded!\n")))
    .catch(err => console.log(err));

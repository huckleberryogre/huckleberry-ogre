import chalk from "chalk";
import path from "path";

const FtpDeploy = require("ftp-deploy");
const ftpDeploy = new FtpDeploy();
import {SERVER_IP, SERVER_USER, SERVER_PASSWORD, SERVER_PROJECT_ROOT_DIRECTORY} from "../../../dotEnvVariables";

(async () => {

    try {
        /**
         * Common config.
         */
        const generalConfig = {
            user: SERVER_USER,
            password: SERVER_PASSWORD,
            host: SERVER_IP,
            port: 21,
            // delete ALL existing files at destination before uploading, if true
            deleteRemote: true,
            // Passive mode is forced (EPSV command is not sent)
            forcePasv: true,
            // use sftp or ftp
            sftp: false
        };

        /**
         * Src folder.
         */
        const srcConfig = {
            ...generalConfig,
            localRoot: path.join(__dirname, '../../../../../src'),
            remoteRoot: `${SERVER_PROJECT_ROOT_DIRECTORY}/src`,
            include: ["server/**", "utils/**", "models.ts", "server.config.ts"],
            // e.g. exclude sourcemaps, and ALL files in node_modules (including dot files)
            exclude: ["client/**"],
        };

        console.log(await ftpDeploy.deploy(srcConfig));

        /**
         * Typings folder.
         */
        const typingsConfig = {
            ...generalConfig,
            localRoot: path.join(__dirname, '../../../../../typings'),
            remoteRoot: `${SERVER_PROJECT_ROOT_DIRECTORY}/typings`,
            include: ["**"],
        };

        console.log(await ftpDeploy.deploy(typingsConfig));


        /**
         * Rest files from the top level.
         */
        const topLevelConfig = {
            ...generalConfig,
            localRoot: path.join(__dirname, '../../../../../'),
            remoteRoot: SERVER_PROJECT_ROOT_DIRECTORY,
            include: [".env", "package.json", "package-lock.json", "tsconfig.json"],
            exclude: ["dist/**", "node_modules/**", "node_modules/**/.*", ".git/**", ".idea", "src/**", "typings/**"],
            deleteRemote: false,
        };

        console.log(await ftpDeploy.deploy(topLevelConfig));

        console.log(chalk.greenBright("Fresh server version uploaded!"));

    } catch (error) {
        console.log(chalk.red('Failed uploading server executables.'));
        console.error(error);
    }
})();

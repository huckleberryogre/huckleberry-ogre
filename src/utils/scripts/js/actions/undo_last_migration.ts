import {sequelize} from '../../../../server/db';
import Umzug from 'umzug';
import path from "path";

/**
 * Performs down migration of a last executed migration.
 *
 * @param sequelize Sequelize instance.
 */
const umzugInstance = new Umzug({
    storage: 'sequelize',
    storageOptions: {
        sequelize
    },
    migrations: {
        params: [
            sequelize.getQueryInterface(),
            sequelize.getDialect()
        ],
        path: path.resolve(__dirname, '../../../../server/db/migrations'),
        pattern: /^\d+[\w-]+\.ts$/
    },
    logging: function () {
        console.log.apply(null, arguments);
    },
});

const logUmzugEvent = (eventName) => (name) => console.log(`${name} ${eventName}`);

umzugInstance.on('migrating', logUmzugEvent('migrating'));
umzugInstance.on('migrated', logUmzugEvent('migrated'));
umzugInstance.on('reverting', logUmzugEvent('reverting'));
umzugInstance.on('reverted', logUmzugEvent('reverted'));

umzugInstance.down().then(() => console.log('success!')).catch(console.error);


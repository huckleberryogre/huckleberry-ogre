import webpack from "webpack";
import chalk from "chalk";
import {getWebpackConfiguration} from "../../../../../webpack.config";

/**
 * Build production version of client app.
 */
try {
    webpack(getWebpackConfiguration(true)).run((err) => {
        if (err) {
            console.error(err);
        }
        console.log(chalk.greenBright('Client build complete.'));
    });
} catch (error) {
    console.log(chalk.red('Client build failed.'));
    console.error(error);
}

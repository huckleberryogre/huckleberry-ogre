import SSH, {IExecOptions} from 'simple-ssh';
import {SERVER_IP, SERVER_USER, SERVER_PASSWORD, SERVER_PROJECT_ROOT_DIRECTORY} from "../dotEnvVariables";

export const ssh = new SSH({
    host: SERVER_IP,
    user: SERVER_USER,
    pass: SERVER_PASSWORD,
    baseDir: SERVER_PROJECT_ROOT_DIRECTORY,
});

export const DEFAULT_EXEC_OPTIONS: IExecOptions = {
    out: console.log.bind(console),
    err: console.error.bind(console),
    exit: (code, stdout, stderr) => {
        console.log(stdout);

        if (!(+code === 0)) {
            console.log('Exit code ' + code);
            console.log(stderr);

            // in case of error default behavior is to quit further execution
            return false;
        }
    }
};

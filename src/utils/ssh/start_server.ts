import {DEFAULT_EXEC_OPTIONS, ssh} from "./index";

ssh.exec('npm install', DEFAULT_EXEC_OPTIONS);
// todo: teach server to handle logs in a good way
ssh.exec('setsid npm run server-prod', {
    ...DEFAULT_EXEC_OPTIONS, out: () => {
        console.log('Setsid command fired, disconnecting...');
        ssh.end()
    }
});

ssh.start();

import {DEFAULT_EXEC_OPTIONS, ssh} from "./index";

// todo: figure something better than just killing node to stop the server
ssh.exec(`pkill node`, DEFAULT_EXEC_OPTIONS).start();
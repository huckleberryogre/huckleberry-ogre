import DotEnv from "dotenv";
import {isEmpty} from "lodash";

const dotEnvContents = DotEnv.config().parsed;

if (isEmpty(dotEnvContents)) {
    throw Error('.env file not found or empty');
}

export const {
    SERVER_PORT,
    SERVER_HTTPS_PORT,
    SERVER_HOSTNAME,
    DB_NAME,
    DB_PRODUCTION_FOLDER,
    RUN_MODE,
    LOG_REQUESTS,
    SERVER_IP,
    SERVER_USER,
    SERVER_PASSWORD,
    GOOGLE_AUTH_PASSWORD,
    GOOGLE_AUTH_CLIENT_ID,
    GOOGLE_AUTH_CLIENT_SECRET,
    SESSION_PASSWORD,
    LETS_ENCRYPT_TLS_CERT_PATH,
    LETS_ENCRYPT_TLS_KEY_PATH,
    SENTRY_CONNECTION_STRING,
    FILES_DIRECTORY,
    SERVER_PROJECT_ROOT_DIRECTORY,
} = dotEnvContents;

export default dotEnvContents;
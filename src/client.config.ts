import {DotenvParseOutput} from "dotenv";

/**
 * Application config.
 *
 * REST_PATH - path to rest service.
 * SERVER_PORT - port number, set 80 for production.
 * SERVER_HOSTNAME - set real hostname for production.
 * SERVER_ADDRESS - hostname + port.
 * SERVER_REST_ADDRESS - hostname + port + rest path.
 */
export const getAppConfig = (environmentConfig: DotenvParseOutput) => {
    const {SERVER_PORT, SERVER_HOSTNAME, RUN_MODE, SENTRY_CONNECTION_STRING} = environmentConfig;
    const isProduction = RUN_MODE === 'production';

    const REST_PATH = 'rest';
    const SERVER_ADDRESS = `http${isProduction ? 's' : ''}://${SERVER_HOSTNAME}${SERVER_PORT === '80' ? '' : `:${SERVER_PORT}`}`;
    const SERVER_REST_ADDRESS = `${SERVER_ADDRESS}/${REST_PATH}`;

    return (
        {
            RUN_MODE,
            REST_PATH,
            SERVER_PORT,
            SERVER_HOSTNAME,
            SERVER_ADDRESS,
            SERVER_REST_ADDRESS,
            SENTRY_CONNECTION_STRING,
        }
    )
};

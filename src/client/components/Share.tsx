import React from 'react';
import {
    EmailIcon,
    EmailShareButton,
    FacebookIcon,
    FacebookShareButton,
    LinkedinIcon,
    LinkedinShareButton,
    RedditIcon,
    RedditShareButton,
    TelegramIcon,
    TelegramShareButton,
    TwitterIcon,
    TwitterShareButton,
    IconComponentProps,
} from 'react-share';

/**
 * @prop src Link for shared content.
 */
interface IShareProps {
    src: string;
    iconsProps: IconComponentProps;
}

/**
 * Share list component.
 */
export const Share = (props: IShareProps) => {
    const {src, iconsProps} = props;

    return (
        <>
            <EmailShareButton url={src}>
                <EmailIcon {...iconsProps}/>
            </EmailShareButton>
            <FacebookShareButton url={src}>
                <FacebookIcon {...iconsProps}/>
            </FacebookShareButton>
            <LinkedinShareButton url={src}>
                <LinkedinIcon {...iconsProps}/>
            </LinkedinShareButton>
            <RedditShareButton url={src}>
                <RedditIcon {...iconsProps}/>
            </RedditShareButton>
            <TelegramShareButton url={src}>
                <TelegramIcon {...iconsProps}/>
            </TelegramShareButton>
            <TwitterShareButton url={src}>
                <TwitterIcon {...iconsProps}/>
            </TwitterShareButton>
        </>
    )
};
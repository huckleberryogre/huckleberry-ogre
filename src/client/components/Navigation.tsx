import React from 'react';
import {Route, RouteComponentProps, Switch} from 'react-router-dom';
import Samurai_Jack from '../assets/Samurai_Jack.png';
import {Blog} from '../modules/blog/Blog';
import {HOC} from "../core/utils/HOC";
import {LogsReader} from "../modules/logsReader/LogsReader";
import {ElectricForceFieldAsync} from "../modules/electricForceField/ElectricForceFieldLazy";
import {StatusMonitor} from "./StatusMonitor";
import * as Sentry from '@sentry/react';

// Create Custom Sentry Route component
const SentryRoute = Sentry.withSentryRouting(Route);

/**
 * Navigation component.
 */
class NavigationComponent extends React.PureComponent<TRouteProps> {

    render() {
        return (
            <Switch location={this.props.location}>
                <SentryRoute exact path="/" component={Blog}/>
                <SentryRoute path="/blog/:mode?/:postID?" component={Blog}/>
                <SentryRoute path="/electricForceField" component={ElectricForceFieldAsync}/>
                <SentryRoute path="/status" component={StatusMonitor}/>
                <SentryRoute path="/logs" component={LogsReader}/>
                <SentryRoute render={() => (
                    <div className='text-center'>
                        <h1>404 :\</h1>
                        <h3>This page is totally lost!</h3>
                        <img src={Samurai_Jack} alt="just like Jack..."/>
                    </div>
                )}
                />
            </Switch>
        );
    }
}

type TRouteProps = RouteComponentProps<{}>;

export const Navigation = HOC<{}, {}, {}, {}, TRouteProps>(
    NavigationComponent,
    {
        mapStateToProps: null,
        mapDispatchToProps: null,
        isWithRouter: true
    }
);

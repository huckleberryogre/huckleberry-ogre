import {Theme, createStyles} from '@material-ui/core';

export const PreviewModalStyles = (theme: Theme) => createStyles({
    previewContainer: {
        display: 'flex',
        width: '100%',
        height: '100%',
        '& img': {
            position: 'absolute',
            maxWidth: '100vw',
            maxHeight: '100vh',
            top: '50%',
            left: '50%',
            transform: 'translate3d(-50%, -50%, 0)',
        }
    },
    progress: {
        margin: 'auto',
        userSelect: 'none',
    },
    actionPanelIcon: {
        cursor: 'pointer',
    },
    actionPanel: {
        position: 'absolute',
        display: 'flex',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        flexWrap: 'wrap',
        padding: 8,
        minWidth: 250,
        bottom: 20,
        left: '50%',
        transform: 'translate3d(-50%,0,0)',
        backgroundColor: 'rgba(0,0,0,0.5)',
        borderRadius: 30,

        '& .SocialMediaShareButton, & button': {
            margin: theme.spacing(1),
            cursor: 'pointer',
        }
    },
    buttonBack: {
        width: 50,
        height: 50
    }
});

import {Theme, createStyles} from '@material-ui/core';

export const GlobalLayoutStyles = (theme: Theme) => createStyles({
    '@global': {
        // highlight.js lib property, don't remove.
        '.hljs': {
            fontSize: 14,
            borderRadius: 4
        },
        a: {
            color: 'inherit',
            textDecoration: 'none'
        },
        '.position-relative': {
            position: 'relative',
        },
        '.text-uppercase': {
            textTransform: 'uppercase'
        },
        '.text-lowercase': {
            textTransform: 'lowercase'
        },
        '.text-muted': {
            opacity: .6
        },
        '.pull-right': {
            float: 'right'
        },
        '.pull-left': {
            float: 'left'
        },
        '.overflowHidden': {
          overflow: 'hidden !important'
        },
        '.display-none-md-up': {
            [theme.breakpoints.up('md')]: {
                display: 'none'
            }
        },
        '.display-none-sm-down': {
            [theme.breakpoints.down('sm')]: {
                display: 'none'
            }
        },
        '.text-center': {
            textAlign: 'center'
        },
        '.text-left': {
            textAlign: 'left'
        },
        '.text-right': {
            textAlign: 'right'
        },
        '.text-ellipsis': {
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap'
        },
        '.margin-0': {
            margin: `0 !important`
        },
        '.margin-1': {
            margin: `${theme.spacing(2)}px !important`
        },
        '.margin-2': {
            margin: `${theme.spacing(4)}px !important`
        },
        '.margin-top-auto': {
          marginTop: 'auto'
        },
        '.margin-bottom-auto': {
          marginBottom: 'auto'
        },
        '.margin-left-auto': {
          marginLeft: 'auto'
        },
        '.margin-right-auto': {
          marginRight: 'auto'
        },
        '.margin-top-0': {
            marginTop: `0 !important`
        },
        '.margin-right-0': {
            marginRight: `0 !important`
        },
        '.margin-bottom-0': {
            marginBottom: `0 !important`
        },
        '.margin-left-0': {
            marginLeft: `0 !important`
        },
        '.margin-top-1': {
            marginTop: `${theme.spacing(2)}px !important`
        },
        '.margin-right-1': {
            marginRight: `${theme.spacing(2)}px !important`
        },
        '.margin-bottom-1': {
            marginBottom: `${theme.spacing(2)}px !important`
        },
        '.margin-left-1': {
            marginLeft: `${theme.spacing(2)}px !important`
        },
        '.margin-top-2': {
            marginTop: `${theme.spacing(4)}px !important`
        },
        '.margin-right-2': {
            marginRight: `${theme.spacing(4)}px !important`
        },
        '.margin-bottom-2': {
            marginBottom: `${theme.spacing(4)}px !important`
        },
        '.margin-left-2': {
            marginLeft: `${theme.spacing(4)}px !important`
        },
        '.padding-0': {
            padding: `0 !important`
        },
        '.padding-1': {
            padding: `${theme.spacing(2)}px !important`
        },
        '.padding-2': {
            padding: `${theme.spacing(4)}px !important`
        },
        '.padding-top-0': {
            paddingTop: `0 !important`
        },
        '.padding-right-0': {
            paddingRight: `0 !important`
        },
        '.padding-bottom-0': {
            paddingBottom: `0 !important`
        },
        '.padding-left-0': {
            paddingLeft: `0 !important`
        },
        '.padding-top-1': {
            paddingTop: `${theme.spacing(2)}px !important`
        },
        '.padding-right-1': {
            paddingRight: `${theme.spacing(2)}px !important`
        },
        '.padding-bottom-1': {
            paddingBottom: `${theme.spacing(2)}px !important`
        },
        '.padding-left-1': {
            paddingLeft: `${theme.spacing(2)}px !important`
        },
        '.padding-top-2': {
            paddingTop: `${theme.spacing(4)}px !important`
        },
        '.padding-right-2': {
            paddingRight: `${theme.spacing(4)}px !important`
        },
        '.padding-bottom-2': {
            paddingBottom: `${theme.spacing(4)}px !important`
        },
        '.padding-left-2': {
            paddingLeft: `${theme.spacing(4)}px !important`
        },
        '.padding-top-3': {
            paddingTop: `${theme.spacing(6)}px !important`
        },
        '.padding-right-3': {
            paddingRight: `${theme.spacing(6)}px !important`
        },
        '.padding-bottom-3': {
            paddingBottom: `${theme.spacing(6)}px !important`
        },
        '.padding-left-3': {
            paddingLeft: `${theme.spacing(6)}px !important`
        },
        '.fw900': {
            fontWeight: 900
        }
    },
    mainPaper: {
        width: '100%',
        fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
    },
    root: {
        width: '100%',
        height: '100vh',
        zIndex: 1,
        overflow: 'hidden',
        padding: '0 !important'
    },
    appFrame: {
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100%',
    },
    content: {
        position: 'absolute',
        left: 0,
        overflow: 'auto',
        width: '100%',
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
        transition: theme.transitions.create('left', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        height: 'calc(100% - 64px)',
        marginTop: '64px',
        [theme.breakpoints.down('xs')]: {
            padding: theme.spacing(1),
        },
    },
    contentShift: {
        left: 240,
        transition: theme.transitions.create('left', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
});

import {Theme, createStyles} from '@material-ui/core';
import {DRAWER_WIDTH} from "../../modules/mainMenu/styles";

export const ApplicationBarStyles = (theme: Theme) => createStyles({
    appBar: {
        position: 'absolute',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: DRAWER_WIDTH,
        width: `calc(100% - ${DRAWER_WIDTH}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        transform: 'scale(1)',
        transition: '.3s .3s',
        marginLeft: 12,
        marginRight: 20,
        color: theme.palette.secondary.contrastText,
        [theme.breakpoints.down('sm')]: {
            marginLeft: 6,
            marginRight: 10,
        },
    },
    homeButton: {
        transition: '.3s',
        flexGrow: 0.98
    },
    homeButtonShift: {
        transform: 'translate3d(-70px,0,0)'
    },
    hide: {
        transform: 'scale(0)',
        transition: '.3s'
    },
    toolbar: {
        height: 64,
    },
});

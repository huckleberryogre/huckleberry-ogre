import {MuiThemeProvider} from '@material-ui/core/styles';
import React from 'react';
import {Grid, Paper, WithStyles, CssBaseline} from '@material-ui/core';
import {ApplicationTheme} from '../theme';
import {GlobalLayoutStyles} from './styles/GlobalLayout';
import {HOC} from '../core/utils/HOC';
import {IAppState} from '../core/reduxStore';
import {ThunkDispatch} from 'redux-thunk';
import {Action} from 'redux';
import {EBlogViewMode} from '../modules/blog/enums';
import {RouteComponentProps} from 'react-router-dom';
import {ErrorBoundary} from "./ErrorBoundary";
import {ApplicationBar} from "./ApplicationBar";
import {IMainMenuStateProps, MainMenu} from "../modules/mainMenu/MainMenu";
import {Notification} from "../modules/notification/Notification";
import classNames from "classnames";
import {MainMenuActions} from "../modules/mainMenu/actions";
import {FilesManager} from "../modules/filesManager/FilesManager";
import {AppSettingsActions} from "../modules/appSettings/actions";
import {getIsWebpSupported} from "../core/utils/utils";

type TRouteProps = RouteComponentProps<{ mode: EBlogViewMode, postID: string }>;
type TDispatchProps = {
    appSettingsActions: AppSettingsActions,
    mainMenuActions: MainMenuActions,
}
type TStyleProps = WithStyles<typeof GlobalLayoutStyles>;

/**
 * @prop showFilesManager Flag to show files manager.
 */
interface IStateProps extends IMainMenuStateProps {}

type TComponentProps = IStateProps & TStyleProps & TDispatchProps & TRouteProps;

/**
 * First layer component.
 */
class GlobalLayoutComponent extends React.Component<TComponentProps> {
    constructor(props: TComponentProps) {
        super(props);
        const {appSettingsActions} = props;

        appSettingsActions.setIsWebpSupported(getIsWebpSupported());
    }

    render() {
        const {classes, mainMenuActions, location: {pathname}, isDrawerOpen, children} = this.props;
        const isElectricForceFieldPage = pathname === '/electricForceField';
        const mainClassName = classNames(classes.content, {
                [classes.contentShift]: isDrawerOpen,
                'padding-0 overflowHidden': isElectricForceFieldPage
            }
        );

        return (
            <MuiThemeProvider theme={ApplicationTheme}>
                <CssBaseline/>
                <div>
                    <FilesManager />
                    <Grid container>
                        <Paper className={classes.mainPaper} square>
                            <ErrorBoundary>
                                <div className={classes.root}>
                                    <div className={classes.appFrame}>
                                        <ApplicationBar/>
                                        <MainMenu/>
                                        <main className={mainClassName} onClick={mainMenuActions.drawerClose}>
                                            {children}
                                            <Notification/>
                                        </main>
                                    </div>
                                </div>
                            </ErrorBoundary>
                        </Paper>
                    </Grid>
                </div>
            </MuiThemeProvider>
        );
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<IAppState, void, Action>) => {
    return {
        appSettingsActions: new AppSettingsActions(dispatch),
        mainMenuActions: new MainMenuActions(dispatch),
    };
};

const mapStateToProps = ({mainMenuState}: IAppState): IStateProps => mainMenuState;

export const GlobalLayout = HOC<{}, IStateProps, TDispatchProps, TStyleProps, TRouteProps>(
    GlobalLayoutComponent,
    {
        mapStateToProps,
        mapDispatchToProps,
        styles: GlobalLayoutStyles,
        isWithRouter: true
    }
);

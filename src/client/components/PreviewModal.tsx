import {CircularProgress, Fab, Modal, WithStyles} from '@material-ui/core';
import {IconComponentProps} from 'react-share';
import {Share} from './Share';
import {
    ArrowBack as ArrowBackIcon,
    Close as CloseIcon,
    CloudDownload as CloudDownloadIcon,
    Share as ShareIcon,
} from '@material-ui/icons';
import download from 'downloadjs';
import React from 'react';
import {ERequestStatus} from '../core/enums';
import {HOC} from '../core/utils/HOC';
import {get} from '../core/utils/ServiceUtils';
import {PreviewModalStyles} from './styles/Preview';
import {getFileUrl} from "../core/utils/utils";

/**
 * Component state.
 *
 * @prop actionPanelTab Action panel tab.
 * @prop downloadStatus Download request status.
 * @prop loaded Image loaded flag.
 */
interface IState {
    actionPanelTab: EActionPanelTab;
    downloadStatus: ERequestStatus;
    loaded: boolean;
}

/**
 * Component props.
 *
 * @prop isOpen Modal open flag.
 * @prop onClose Modal close handler.
 * @prop [src] If provided, img node with src will be used instead of children.
 * @prop [file] If file is passed then component will skip downloading from src.
 */
interface IOwnProps {
    isOpen: boolean;
    onClose: () => void;
    src?: string;
    file?: Blob;
}

/**
 * Action panel tabs.
 */
enum EActionPanelTab {
    DEFAULT,
    SHARE
}

const iconsProps: IconComponentProps = {
    size: 50,
    round: true
};

type TStyleProps = WithStyles<typeof PreviewModalStyles>;

/**
 * Preview modal component.
 */
class PreviewModalComponent extends React.Component<IOwnProps & TStyleProps, IState> {

    state: IState = {
        actionPanelTab: EActionPanelTab.DEFAULT,
        downloadStatus: ERequestStatus.NONE,
        loaded: false,
    };

    /**
     * Download button click handler.
     */
    handleDownload = async () => {
        this.setState({downloadStatus: ERequestStatus.PENDING});
        const {src, file} = this.props;

        if (file) {
            download(file, src);
            this.setState({downloadStatus: ERequestStatus.SUCCESS});
        } else {
            try {
                const {file} = await get<{file: Blob}>(src, null, {isOriginalRequestUrl: true});
                const fileName = src.slice(src.lastIndexOf('/') + 1);
                download(file, fileName);

                this.setState({downloadStatus: ERequestStatus.SUCCESS});
            } catch (error) {
                console.error(error);
                this.setState({downloadStatus: ERequestStatus.FAIL});
            }
        }
    };

    /**
     * Share tab show handler.
     */
    handleShareTabShow = () => {
        this.setState({actionPanelTab: EActionPanelTab.SHARE});
    };

    /**
     * Share tab hide handler.
     */
    handleShareTabHide = () => {
        this.setState({actionPanelTab: EActionPanelTab.DEFAULT});
    };

    /**
     * Renders default tab.
     */
    renderActionPanelDefaultTab = () => {
        const {src, file, classes, onClose} = this.props;
        const {downloadStatus} = this.state;
        const isDownloading = downloadStatus === ERequestStatus.PENDING;

        return (
            <>
                {!!(src || file) && (
                    <Fab
                        aria-label="Download"
                        className={classes.actionPanelIcon}
                        onClick={this.handleDownload}
                        disabled={isDownloading}
                    >
                        {isDownloading ? <CircularProgress color='secondary'/> : <CloudDownloadIcon color={'action'}/>}
                    </Fab>
                )}
                <Fab
                    aria-label="Share"
                    className={classes.actionPanelIcon}
                    onClick={this.handleShareTabShow}
                >
                    <ShareIcon color={'action'}/>
                </Fab>
                <Fab
                    aria-label="Close"
                    className={classes.actionPanelIcon}
                    onClick={onClose}
                >
                    <CloseIcon color={'action'}/>
                </Fab>
            </>
        );
    };

    /**
     * Renders share tab content.
     */
    renderActionPanelShareTab = () => {
        const {src, classes} = this.props;

        return (
            <>
                <Fab
                    aria-label="Back"
                    className={classes.buttonBack}
                    onClick={this.handleShareTabHide}
                >
                    <ArrowBackIcon/>
                </Fab>
                {src && (<Share src={src} iconsProps={iconsProps}/>)}
            </>
        );
    };

    /**
     * Load end event handler.
     */
    handleLoaded = () => {
        this.setState({loaded: true})
    };

    render() {
        const {src, file, classes, onClose, isOpen} = this.props;
        const {actionPanelTab, loaded} = this.state;
        let actionPanel;

        switch (actionPanelTab) {
            case EActionPanelTab.SHARE:
                actionPanel = this.renderActionPanelShareTab();
                break;
            case EActionPanelTab.DEFAULT:
            default:
                actionPanel = this.renderActionPanelDefaultTab();
        }

        return (
            <Modal
                open={isOpen}
                onEscapeKeyDown={onClose}
                onBackdropClick={onClose}
            >
                <div className={classes.previewContainer}>
                    {!loaded && <CircularProgress className={classes.progress} color="secondary" />}
                    <img
                        src={getFileUrl(file) || src}
                        alt={src && src.slice(src.lastIndexOf('/') + 1)}
                        onLoad={this.handleLoaded}
                    />
                    <div className={classes.actionPanel}>
                        {actionPanel}
                    </div>
                </div>
            </Modal>
        );
    }
}

export const PreviewModal = HOC<IOwnProps, {}, {}, TStyleProps, {}>(
    PreviewModalComponent,
    {
        mapStateToProps: null,
        mapDispatchToProps: null,
        styles: PreviewModalStyles
    }
);

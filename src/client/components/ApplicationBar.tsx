import {
    AppBar,
    IconButton,
    Toolbar,
    Typography,
    WithStyles,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import classNames from 'classnames';
import React from 'react';
import {Link} from 'react-router-dom';
import {Action} from 'redux';
import {ThunkDispatch} from 'redux-thunk';
import {DrawerPanel} from '../modules/drawerPanel/DrawerPanel';
import {ILoginStateProps, Login} from '../modules/login/Login';
import {IAppState} from '../core/reduxStore';
import {HOC} from '../core/utils/HOC';
import {MainMenuActions} from "../modules/mainMenu/actions";
import {ApplicationBarStyles} from "./styles/ApplicationBar";
import {IMainMenuStateProps} from "../modules/mainMenu/MainMenu";

type TStateProps = IMainMenuStateProps & ILoginStateProps;
type TDispatchProps = {
    mainMenuActions: MainMenuActions;
}
type TStyleProps = WithStyles<typeof ApplicationBarStyles>;

/**
 * Application bar component.
 */
class ApplicationBarComponent extends React.Component<TStateProps & TDispatchProps & TStyleProps> {

    render() {
        const {classes, isDrawerOpen, mainMenuActions} = this.props;

        return (
            <AppBar className={classNames(classes.appBar, isDrawerOpen && classes.appBarShift)}>
                <DrawerPanel/>
                <Toolbar className={classes.toolbar} disableGutters={!isDrawerOpen}>
                    <IconButton
                        className={classNames(classes.menuButton, isDrawerOpen && classes.hide)}
                        color="primary"
                        aria-label="open drawer"
                        onClick={mainMenuActions.drawerOpen}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Typography
                        variant="h6"
                        className={classNames(classes.homeButton, isDrawerOpen && classes.homeButtonShift)}
                    >
                        <Link to="/" onClick={mainMenuActions.drawerClose}>
                            {'Huckleberry Ogre Home'}
                        </Link>
                    </Typography>
                    <Login/>
                </Toolbar>
            </AppBar>
        );
    }
}

const mapStateToProps = (state: IAppState) => ({
    ...state.loginState,
    isDrawerOpen: state.mainMenuState.isDrawerOpen
});

const mapDispatchToProps = (dispatch: ThunkDispatch<IAppState, void, Action>) => {
    return {
        mainMenuActions: new MainMenuActions(dispatch),
    };
};

export const ApplicationBar = HOC<{}, TStateProps, TDispatchProps, TStyleProps, {}>(
    ApplicationBarComponent,
    {
        mapStateToProps,
        mapDispatchToProps,
        styles: ApplicationBarStyles,
        isWithRouter: true
    }
);

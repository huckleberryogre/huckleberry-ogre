import React from 'react';
import {Component} from 'react';
import {PreviewModal} from './PreviewModal';
import {store} from "../core/reduxStore";
import {get} from '../core/utils/ServiceUtils';
import {getFileUrl} from "../core/utils/utils";
import {PlaceholderImage} from "../assets/PlaceholderImage";

/**
 * @prop src Image src.
 */
interface IProps {
    src: string
}

/**
 * Image with preview modal.
 */
export class ImageWithPreview extends Component<IProps> {
    constructor(props: IProps) {
        super(props);

        const {src} = props;
        const {appSettingsState} = store.getState();
        const format = appSettingsState.isWebpSupported ? 'webp' : 'png';

        if (src.includes(AppConfig.SERVER_HOSTNAME)) {
            this.src = src + `${src.indexOf('?') > -1 ? '&' : '?'}format=${format}`;
        } else {
            this.src = src;
        }
    }

    src: string;

    placeholderRef = React.createRef<SVGSVGElement>();

    state = {
        isOpen: false,
        file: null,
    };

    componentDidMount(): void {
        if (this.placeholderRef.current) {
            const handleGetImage = (entries) => {
                if (entries[0].isIntersecting) {
                    get<{ file: Blob }>(this.src, null, {isOriginalRequestUrl: true})
                        .then((result) => {
                            this.setState({file: result.file});
                        });
                }
            };
            const observer = new IntersectionObserver(handleGetImage);
            observer.observe(this.placeholderRef.current);
        }
    }

    handlePreviewOpen = () => {
        this.setState({isOpen: true});
    };

    handlePreviewClose = () => {
        this.setState({isOpen: false});
    };

    render() {
        const {src} = this.props;
        const {file} = this.state;

        return (
            <>
                {!file ? <PlaceholderImage ref={this.placeholderRef}/> : (
                    <img
                        src={getFileUrl(file)}
                        alt={src.slice(src.lastIndexOf('/') + 1)}
                        onClick={this.handlePreviewOpen}
                    />
                )}
                <PreviewModal
                    file={file}
                    src={src}
                    isOpen={this.state.isOpen}
                    onClose={this.handlePreviewClose}
                />
            </>
        );
    }
}

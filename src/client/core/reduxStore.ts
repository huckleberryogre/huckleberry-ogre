import {combineReducers, createStore, applyMiddleware, compose, Action} from 'redux';
import {blogState, initBlogState} from '../modules/blog/reducers';
import thunkMiddleware, {ThunkDispatch} from 'redux-thunk';
import {createBrowserHistory, History} from 'history';
import {connectRouter, routerMiddleware, push, RouterState} from 'connected-react-router';
import {IBlogStateProps} from '../modules/blog/Blog';
import {INotificationStateProps} from '../modules/notification/Notification';
import {IFilesManagerStateProps} from '../modules/filesManager/FilesManager';
import {ILoginStateProps} from "../modules/login/Login";
import {initNotificationState, notificationState} from '../modules/notification/reducers';
import {filesManagerState, initFilesManagerState} from '../modules/filesManager/reducers';
import {initLoginState, loginState} from '../modules/login/reducers';
import {initMainMenuState, mainMenuState} from "../modules/mainMenu/reducers";
import {IMainMenuStateProps} from "../modules/mainMenu/MainMenu";
import {save, load} from "redux-localstorage-simple"
import * as Sentry from "@sentry/react";
import {appSettingsState, IAppSettingsStateProps, initAppSettingsState} from "../modules/appSettings/reducers";

/** Browser (hash) history. */
export const history: History = createBrowserHistory();

/**
 * Combined reducers interface.
 *
 * @param {IBlogStateProps} Blog part of app state.
 */
export interface ICombinedReducers {
    router: RouterState;
    blogState: IBlogStateProps;
    notificationState: INotificationStateProps;
    filesManagerState: IFilesManagerStateProps;
    loginState: ILoginStateProps;
    mainMenuState: IMainMenuStateProps;
    appSettingsState: IAppSettingsStateProps;
}

/** Combined reducers. */
const combinedReducers = combineReducers<ICombinedReducers>(
    {
        router: connectRouter(history),
        blogState,
        filesManagerState,
        notificationState,
        loginState,
        mainMenuState,
        appSettingsState,
    }
);

/** Initial store state. */
const initialState: Omit<ICombinedReducers, 'router'> = {
    blogState: initBlogState,
    filesManagerState: initFilesManagerState,
    notificationState: initNotificationState,
    loginState: initLoginState,
    mainMenuState: initMainMenuState,
    appSettingsState: initAppSettingsState,
};

/** Enhancers. See docs at https://github.com/zalmoxisus/redux-devtools-extension **/
const composeEnhancers = (window as Window & { __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: Function }).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/**
 * Main redux store interface.
 * Extends combined app reducers with router state.
 *
 * @prop {RouterState} router Router state.
 */
export interface IAppState extends ICombinedReducers {
    router: RouterState;
}

/**
 * Options for redux state to persist in local storage.
 */
const reduxLocalStorageOptions = {
    namespace: 'redux',
    states: [
        'blogState.posts',
        'blogState.form',
        'blogState.formHistory',
        'blogState.tags',
        'filesManagerState.isFilesManagerShow',
        'filesManagerState.isFilesManagerEnabled',
        'filesManagerState.tabIndex',
    ],
};

/**
 * Sentry redux integration.
 */
const sentryReduxEnhancer = Sentry.createReduxEnhancer();

/**
 * Main redux store.
 */
export const store = createStore(
    combinedReducers,
    load({
        ...reduxLocalStorageOptions,
        preloadedState: initialState,
        disableWarnings: true
    }),
    composeEnhancers(
        applyMiddleware<ThunkDispatch<IAppState, void, Action>>(
            routerMiddleware(history),
            thunkMiddleware,
            save(reduxLocalStorageOptions)
        ),
        sentryReduxEnhancer
    )
);

/**
 * Navigate to given location.
 *
 * @param {string} newLocation Location relative path.
 */
export const navigateTo = (newLocation: string) => {
    store.dispatch(push(newLocation));
};

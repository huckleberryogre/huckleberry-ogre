import parser, {Tag} from 'bbcode-to-react';
import {includes} from 'lodash';
import {parseUrl} from 'query-string';
import React from 'react';
import {ImageWithPreview} from '../../components/ImageWithPreview';
import {EContentType, ERegExp} from '../enums';
import {isMatchRegExp} from './utils';
import {get} from './ServiceUtils';
import Highlight from 'react-highlight'
import "../../../../node_modules/highlight.js/styles/darkula.css";
import {unescape} from 'html-escaper';

/**
 * Implements youtube bbcode tag.
 * Possible params:
 *
 * @param width Defaults to 560px.
 * @param height Defaults to 315px.
 * @param align Defaults to center.
 */
class YoutubeTag extends Tag {
    toReact() {
        let link = this.getContent(true);
        const questionMarkIndex = link.lastIndexOf('?');

        if (questionMarkIndex > -1) {
            const parameters = new URLSearchParams(link.slice(questionMarkIndex + 1));
            const startTime = parameters.get('t');

            if (startTime) {
                link += `&start=${startTime}`
            }
        }

        const attributes = {
            src: link,
            allow: 'accelerometer; encrypted-media; gyroscope; picture-in-picture',
            style: {
                alignSelf: this.params.align || 'center'
            }
        };

        return (
            <iframe
                {...attributes}
                frameBorder="0"
                allowFullScreen
            />
        );
    }
}

/**
 * Implements img bbcode tag with preview wrapper.
 * Takes link from content section of tag.
 */
class ImgTag extends Tag {
    toReact() {
        return <ImageWithPreview src={this.getContent(true)}/>;
    }
}

/**
 * Implements wrapper bbcode tag.
 * Wraps content into a div container with given className.
 */
class WrapperTag extends Tag {
    toReact() {
        return <div className={this.params.className}>{parser.toReact(this.getContent(true))}</div>;
    }
}

/**
 * Implements caption tag.
 */
class CaptionTag extends Tag {
    toReact() {
        return <div role="caption">{this.getContent(true)}</div>;
    }
}

/**
 * Implements list item tag.
 */
class ListItemTag extends Tag {
    toReact() {
        return <li>{parser.toReact(this.getContent(true))}</li>;
    }
}

/**
 * Implements list item tag.
 */
class HighlightTag extends Tag {
    toReact() {
        return <Highlight className={this.params.language || 'react'}>{unescape(this.getContent(true)).replace('\n', '')}</Highlight>;
    }
}

/**
 * Registering tags and exporting parser.
 */
parser.registerTag('youtube', YoutubeTag);
parser.registerTag('img', ImgTag);
parser.registerTag('wrap', WrapperTag);
parser.registerTag('caption', CaptionTag);
parser.registerTag('li', ListItemTag);
parser.registerTag('highlight', HighlightTag);
export {parser};

/**
 * Wrap link with BBCodeTag according to server response.
 *
 * @param link Link to wrap.
 */
export const getUnknownLinkWithBBCode = (link: string): Promise<string> => {
    return get(link, null, {isOriginalRequestUrl: true})
        .then((response) => {
                let wrappedLink: string;
                const contentType = response.response.headers.get('Content-Type');

                const isImage = includes(contentType, EContentType.IMAGE);

                switch (true) {
                    case isImage:
                        wrappedLink = `[img]${link}[/img]\n`;
                        break;
                    default:
                        wrappedLink = `[url]${link}[/url]`;
                }

                return wrappedLink;
            }
        )
        .catch(() => link);
};

/**
 * Youtube link wrapper.
 *
 * @param link Link to wrap.
 */
export const getYoutubeLinkWithBBCode = (link: string): string => {
    const parsed = parseUrl(link);
    const videoID = parsed.query.v || link.slice(link.lastIndexOf('/') + 1);

    if (videoID) {
        return `[youtube]https://www.youtube.com/embed/${videoID}[/youtube]`;
    } else {
        return `[url]${link}[/url]`;
    }
};

/**
 * Checks if provided link is youtube link.
 *
 * @param link Site link.
 */
const isYouTubeLink = (link: string): boolean => includes(link, 'youtube.com') || includes(link, 'youtu.be/');

/**
 * Checks if provided link refers to an image.
 *
 * @param link Site link.
 */
const isImageLink = (link: string): boolean => isMatchRegExp(parseUrl(link).url, ERegExp.IMAGE_LINK);

/**
 * Asynchronously check link response status and content type and wrap link with according BBCode tag.
 * If fail to check - just return given link.
 *
 * @param link Site link to check.
 */
export const getLinkWithBBCode = (link: string): Promise<string> => {

    if (isYouTubeLink(link)) {
        return Promise.resolve(getYoutubeLinkWithBBCode(link));
    } else if (isImageLink(link)) {
        return Promise.resolve(`[img]${link}[/img]\n`);
    } else {
        return getUnknownLinkWithBBCode(link);
    }
};

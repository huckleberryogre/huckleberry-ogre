import React, {ComponentType} from 'react';
import {
    MapStateToPropsParam,
    MapDispatchToPropsParam,
    connect
} from 'react-redux';
import {IAppState} from '../reduxStore';
import {StyleRulesCallback, StyleRules, withStyles, Theme} from '@material-ui/core/styles';
import {PropInjector} from '@material-ui/types';
import {withRouter} from 'react-router-dom';

/**
 * HOC function settings.
 *
 * @param {MapStateToPropsParam | null} mapStateToProps State props.
 * @param {MapDispatchToPropsParam | null} mapDispatchToProps Dispatch props.
 * @param {StyleRulesCallback | StyleRules} [BlogStyles] If provided, wuthStyles hoc added.
 * @param {boolean} [isWithRouter] Should router be connected flag. False by default.
 */
interface IHOCSettings<TOwnProps extends {}, TStateProps, TDispatchProps, TStyleProps, TRouteProps> {
    mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps & TStyleProps & TRouteProps, IAppState> | null,
    mapDispatchToProps: MapDispatchToPropsParam<TDispatchProps, TOwnProps & TStyleProps & TRouteProps> | null,
    styles?: StyleRulesCallback<Theme, TOwnProps> | StyleRules,
    isWithRouter?: boolean
}

/**
 * Higher order component creator function.
 * Can apply following HOCs (according to settings passed):
 * connect - react-redux
 * withRouter - react-router-dom
 * withStyles - material-ui
 *
 * @param {typeof React.Component} Component The Component to be augmented.
 * @param {IHOCSettings} [settings] Settings.
 * todo: find out why HOC allows undeclared TStateProps to pass through (check how Blog's user prop from login branch passes through). https://huckleberrydev.atlassian.net/browse/HBOG-26
 */
export function HOC<TOwnProps, TStateProps, TDispatchProps, TStyleProps, TRouteProps>(
    Component: ComponentType,
    settings: IHOCSettings<TOwnProps, TStateProps, TDispatchProps, TStyleProps, TRouteProps>
): ComponentType<TOwnProps> {
    const {mapStateToProps, mapDispatchToProps, isWithRouter, styles} = settings;
    let ConnectedComponent;

    if (mapStateToProps && mapDispatchToProps) {
        ConnectedComponent = connect<TStateProps, TDispatchProps, TOwnProps & TStyleProps & TRouteProps, IAppState>(
            mapStateToProps,
            mapDispatchToProps
        )(
            (props) => <Component {...props}/>
        );
    } else if (mapStateToProps) {
        ConnectedComponent = connect<TStateProps, {}, TOwnProps & TStyleProps & TRouteProps, IAppState>(
            mapStateToProps
        )(
            (props) => <Component {...props}/>
        );
    } else if (mapDispatchToProps) {
        ConnectedComponent = connect<{}, TDispatchProps, TOwnProps & TStyleProps & TRouteProps, IAppState>(
            null,
            mapDispatchToProps
        )(
            (props) => <Component {...props}/>
        );
    } else {
        ConnectedComponent = Component;
    }

    let WithRouterConnectedComponent = ConnectedComponent;

    if (isWithRouter) {
        WithRouterConnectedComponent = withRouter(
            (props) =>
                <ConnectedComponent {...props}/>
        )
    }

    let WithRouterConnectedStyledComponent = WithRouterConnectedComponent;

    if (styles) {
        const withStylesInjector: PropInjector<TStyleProps, TOwnProps> = withStyles(styles);

        WithRouterConnectedStyledComponent = withStylesInjector(
            (props) =>
                <WithRouterConnectedComponent {...props}/>
        )
    }

    return WithRouterConnectedStyledComponent;
}

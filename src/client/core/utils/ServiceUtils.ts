import {Util} from '@hapi/hapi';
import isEmpty from 'lodash/isEmpty';
import {NotificationActions} from '../../modules/notification/actions';
import {EContentType} from '../enums';
import {parseUrl} from 'query-string';
import {ENotificationVariant} from "../../modules/notification/enums";

/**
 * Proxy link via cors-anywhere.
 *
 * You can deploy a CORS Anywhere server to Heroku in literally just 2-3 minutes, with 5 commands:
 *
 * git clone https://github.com/Rob--W/cors-anywhere.git
 * cd cors-anywhere/
 * npm install
 * heroku create
 * git push heroku master
 *
 * from https://stackoverflow.com/a/47085173
 */
const CORS_ANYWHERE_PROXY = 'https://cors-anywhere.herokuapp.com/';

/**
 * Workaround to bypass cors restrictions.
 *
 * @param link Original link.
 */
export const getProxyLinkBypassCors = (link: string): string => {
    return CORS_ANYWHERE_PROXY + link;
};

/**
 * Basic response interface.
 */
interface IBasicResponse {
    response: Response;
}

/**
 * Response interface for text type.
 */
export interface IResponseText extends IBasicResponse {
    text: string;
}

/**
 * RequestInit parameters of fetch second argument.
 *
 * @prop {any} headers Request headers.
 */
type TRequestSettings = RequestInit & {
    signal?: any
}

/**
 * Options of request methods
 *
 * @prop [isOriginalRequestUrl] If true, request url will be passed to fetch as-is.
 * @prop [noWrap] If true, request body object won't be wrapped with JSON.stringify.
 * @prop [suppressErrorMessage] If true, suppress error messages.
 * @prop [requestSettings] RequestInit parameters of fetch second argument.
 * @prop [clientErrorMessage] Client error message (fallback general message).
 * @prop [notification] Notification message of given type.
 */
interface IRequestMethodOptions {
    isOriginalRequestUrl?: boolean;
    noWrap?: boolean;
    suppressErrorMessage?: boolean;
    requestSettings?: TRequestSettings;
    notification?: {
        [T in ENotificationVariant]?: string;
    }
}

/**
 * Custom error type handler.
 * https://javascript.info/promise-error-handling.
 */
class HttpError extends Error {
    constructor(response: Response, message: string) {
        super(message);
        this.name = 'HttpError';
        this.response = response;
    }

    response: Response;
}

/**
 * Known error codes mapped with corresponding messages.
 */
const KNOWN_ERROR_CODES = {
    401: 'Authorization required',
    403: 'Forbidden action',
    500: 'Internal server error',
    503: 'Service unavailable',
};

/**
 * Handles response before it proceed to the next steps.
 * Regulate status code errors.
 *
 * @param response Response object.
 * @param options Request options object.
 */
const handleResponseInitial = async (response: Response | Error, options: IRequestMethodOptions): Promise<Response> => {
    if (response instanceof Error) {
        const errorMessage = response.message === 'Failed to fetch' ? 'connection lost' : 'unknown error';
        new NotificationActions().error(errorMessage);

        throw response;
    }

    const statusCode = response.status;
    const {
        [ENotificationVariant.ERROR]: errorMessage,
        [ENotificationVariant.SUCCESS]: successMessage,
        [ENotificationVariant.INFO]: infoMessage,
        [ENotificationVariant.WARNING]: warningMessage,
    } = options.notification ?? {};

    if (statusCode === 200) {
        if (successMessage) {
            new NotificationActions().success(successMessage);
        }
        if (infoMessage) {
            new NotificationActions().info(infoMessage);
        }
        if (warningMessage) {
            new NotificationActions().warning(warningMessage);
        }

        return response;
    } else {
        let resultMessage = `${statusCode ? `[${statusCode}] ` : ''}${errorMessage || ''}`;

        if (!options.suppressErrorMessage) {
            let serverMessage = '';
            try {
                const payload = await response.json();
                serverMessage = payload.message;

                if (serverMessage) {
                    resultMessage += `${errorMessage ? `\n(${serverMessage})` : serverMessage}`;
                }
            } catch (e) {
                console.error('failed to parse error response (not json?) ', e);
            } finally {
                if (!errorMessage && !serverMessage) {
                    resultMessage += statusCode ? KNOWN_ERROR_CODES[statusCode] : 'unknown error';
                }
                new NotificationActions().error(resultMessage);
            }
        }

        throw new HttpError(response, resultMessage);
    }
};

/**
 * Remember sites blocked by cors.
 */
const blockedUrlList = [];

/**
 * Service layer helpers.
 */
class ServiceUtils {
    /**
     * Generic request constructor.
     *
     * @param {HTTP_METHODS} method Request method.
     * @param {string} requestURL Any url.
     * @param {any} [body] Request body.
     * @param {IRequestMethodOptions} options Additional options.
     */
    private static genericInitialRequest = (
        method: Util.HTTP_METHODS,
        requestURL: string,
        body?: any,
        options: IRequestMethodOptions = {}
    ): Promise<Response> => {
        const {noWrap, requestSettings, isOriginalRequestUrl} = {
            ...options,
            requestSettings: {
                method,
                credentials: 'same-origin' as RequestCredentials,
                ...options.requestSettings
            },
        };

        let ownServerRequestUrl = `${AppConfig.SERVER_REST_ADDRESS}/${requestURL}`;

        if (method === 'get') {
            if (!isEmpty(body)) {
                ownServerRequestUrl += `?payload=${typeof body === 'string' ? body : JSON.stringify(body)}`;
            }

            delete requestSettings.body;
        } else {
            requestSettings.body = noWrap ? body : JSON.stringify(body);
        }

        const url = isOriginalRequestUrl ? requestURL : ownServerRequestUrl;
        const parsedUrl = parseUrl(url).url;
        const urlFromBlockedList = blockedUrlList.some(blockedUrl => parsedUrl.indexOf(blockedUrl) !== -1);

        return fetch(urlFromBlockedList ? getProxyLinkBypassCors(url) : url, requestSettings)
            .then(
                response => handleResponseInitial(response, options),
                error => {
                    // try proxy if cors blocked request
                    const needsTryWithProxy = error.message === 'Failed to fetch' && !urlFromBlockedList;

                    if (needsTryWithProxy) {
                        console.info('Failed to fetch with regular request. Trying with proxy...');

                        blockedUrlList.push(parsedUrl);

                        if (!requestSettings.headers) {
                            requestSettings.headers = {'X-Requested-With': 'XMLHttpRequest'};
                        } else {
                            requestSettings.headers['X-Requested-With'] = 'XMLHttpRequest';
                        }

                        return fetch(getProxyLinkBypassCors(url), requestSettings)
                            .then(response => handleResponseInitial(response, options))
                            .catch(error => handleResponseInitial(error, options));
                    }

                    return handleResponseInitial(error, options);
                });
    };

    /**
     * Handle readable stream response of fetch.
     *
     * @param {"get" | "post" | "put" | "delete"} method Request method (CRUD).
     * @param {string} requestURL Relative rest path.
     * @param {any} [body] Request body.
     * @param {IRequestMethodOptions} [options] Additional options.
     */
    public static handleGenericRequest = <TResponse>(
        method: 'get' | 'post' | 'put' | 'delete',
        requestURL: string,
        body?: any,
        options?: IRequestMethodOptions
    ): Promise<TResponse & IBasicResponse> => {
        return ServiceUtils
            .genericInitialRequest(method, requestURL, body, options)
            .then<TResponse & IBasicResponse>(
                async response => {
                    // typical contentType look: "application/json; charset=utf-8"
                    // list of possible content-types: https://stackoverflow.com/a/48704300
                    const contentType = response?.headers?.get('content-type')?.split(';')[0].toLowerCase();
                    let result;

                    if (!contentType) {
                        result = response;
                    } else if (contentType === EContentType.JSON.toLowerCase()) {
                        result = await response.json();
                        result && (result.response = response);
                    } else if (contentType.indexOf(EContentType.TEXT) === 0) {
                        result = {text: await response.text(), response};
                    } else if (contentType.indexOf(EContentType.IMAGE) === 0) {
                        result = {file: await response.blob(), response};
                    } else {
                        result = response;
                        if (!options.suppressErrorMessage) {
                            new NotificationActions().warning(`Don't know how to handle content type: ${contentType}. \nAsk author to teach me please.`);
                        }
                        console.warn(`Don't know how to handle content type: ${contentType}`);
                    }

                    return result;
                }
            );
    };

    /**
     * Main request methods.
     */
    public static get = <TResponse>(url: string, body?: any, options?: IRequestMethodOptions) => ServiceUtils.handleGenericRequest<TResponse>('get', url, body, options);
    public static post = <TResponse>(url: string, body?: any, options?: IRequestMethodOptions) => ServiceUtils.handleGenericRequest<TResponse>('post', url, body, options);
    public static put = <TResponse>(url: string, body?: any, options?: IRequestMethodOptions) => ServiceUtils.handleGenericRequest<TResponse>('put', url, body, options);
    public static remove = <TResponse>(url: string, body?: any, options?: IRequestMethodOptions) => ServiceUtils.handleGenericRequest<TResponse>('delete', url, body, options);
}

export const {get, post, put, remove} = ServiceUtils;

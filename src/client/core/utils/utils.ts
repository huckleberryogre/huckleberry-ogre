import {last, round} from 'lodash';
import {history} from '../reduxStore';
import {ELocationContext, ERegExp} from "../enums";

/**
 * Returns an array of indexes with all entries of substring within given string.
 *
 * @param string String to search.
 * @param substring Substring.
 */
export const indexOfAll = (string: string, substring: string): number[] => {
    let result: number[] = [];
    let index = string.indexOf(substring);

    if (index === -1) {
        return result;
    } else {
        result.push(index);
        do {
            index = string.indexOf(substring, last(result) + 1);
            index > -1 && result.push(index);
        } while (index > -1);
    }

    return result;
};

/**
 * Checks if current location is within one of provided context.
 *
 * @param allowedContextArray Array of required contexts.
 */
export const isLocationWithinContext = (allowedContextArray: ELocationContext[]): boolean => {
    return allowedContextArray.some(allowedContext => history.location.pathname.includes(allowedContext));
};

/**
 * Checks if string match with regexp provided.
 *
 * @param testString String to be tested.
 * @param regExpType RegExp to match with.
 */
export const isMatchRegExp = (testString: string, regExpType: ERegExp) => {
    const regExp = new RegExp(regExpType);

    return regExp.test(testString);
};

/**
 * Converts bytes to megabytes (MB).
 *
 * @param size Size in bytes.
 */
export const toMb = (size: number) => round(size / 1000000, 2);

/**
 * Converts bytes to kilobytes (kB).
 *
 * @param size Size in bytes.
 */
export const toKB = (size: number) => round(size / 1000, 2);

/**
 * Converts size to human readable string.
 *
 * @param size Size in bytes.
 */
export const toHumanReadableSizeFormat = (size: number) => {
    if (toMb(size) >= 0.01) {
        return toMb(size) + ' Mb';
    } else if (toKB(size) >= 0.01) {
        return toKB(size) + ' kB';
    } else {
        return size + ' bytes';
    }
};

/**
 * Returns webp supported flag.
 */
export const getIsWebpSupported = (): boolean => {
    const elem = document.createElement('canvas');

    if (!!elem?.getContext?.('2d')) {
        // was able or not to get WebP representation
        // https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/toDataURL
        return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
    }

    // very old browser like IE 8, canvas not supported
    return false;
};

/**
 * Local storage for all of the locally stored blobs (made out of files).
 *
 * Because react creates a new blob for each file on every render, see:
 * https://www.bignerdranch.com/blog/dont-over-react-rendering-binary-data/
 */
export const fileUrlMap = new Map();

/**
 * Gets file url out of map (inserts new entry if not found).
 */
export const getFileUrl = (file) => {
    if (!file) {
        return null;
    }

    if (!fileUrlMap.has(file)) {
        fileUrlMap.set(file, URL.createObjectURL(file));
    }

    return  fileUrlMap.get(file);
};

/**
 * UUID generator. See https://gist.github.com/jed/982883
 */
export const generateUUID = (a?) => a ? (a ^ Math.random() * 16 >> a / 4).toString(16) : ([1e7].toString() + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, generateUUID);

/** Defines request status. */
export enum ERequestStatus {
    NONE,
    PENDING,
    SUCCESS,
    FAIL
}

/** Response content types. */
export enum EContentType {
    JSON = 'application/json',
    IMAGE = 'image',
    VIDEO = 'video',
    TEXT = 'text'
}

/**
 * BBCode tags.
 */
export enum BBCodeTag {
    BOLD = 'b',
    ITALIC = 'i',
    UNDERLINE = 'u',
    STRIKE = 's',
    WRAP = 'wrap',
    CAPTION = 'caption',
    LIST = 'li',
    CODE = 'highlight'
}

/**
 * User role.
 */
export enum ERole {
    ADMIN = 'ADMIN',
    USER = 'USER'
}

/**
 * Possible location context.
 * To be extended...
 */
export enum ELocationContext {
    HOME = '/',
    BLOG = '/blog',
    BLOG_CREATE = '/blog/create',
    BLOG_EDIT = '/blog/edit'
}

/** Match website url, http(s) not required. */
export enum ERegExp {
    SITE_FULL_URL = '^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$',
    IMAGE_LINK = '^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/).*.(jpg|jpeg|png|gif|tif|webp)$'
}

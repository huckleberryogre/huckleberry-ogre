import React from 'react';

export enum EChargeSign {
    POSITIVE,
    NEGATIVE
}

export const ChargeIcon = (sign: EChargeSign) => (
    <svg version="1.1" baseProfile="full" width="100" height="100">
        <defs>
            <radialGradient id="white_spot" cx="0.65" cy="0.7" r="0.75">
                <stop stopColor="#ffffff" offset="0" stopOpacity="0.75"/>
                <stop stopColor="#ffffff" offset="0.1" stopOpacity="0.5"/>
                <stop stopColor="#ffffff" offset="0.6" stopOpacity="0"/>
                <stop stopColor="#000000" offset="0.6" stopOpacity="0"/>
                <stop stopColor="#000000" offset="0.75" stopOpacity="0.05"/>
                <stop stopColor="#000000" offset="0.85" stopOpacity="0.15"/>
                <stop stopColor="#000000" offset="1" stopOpacity="0.55"/>
            </radialGradient>
        </defs>
        <g id="charge_positive" transform="translate(50,50) scale(1,-1)">
            <circle
                style={{fill: sign === EChargeSign.POSITIVE ? '#ff0000' : '#3355ff', stroke: 'none'}}
                cx="0"
                cy="0"
                r="46.5"
            />
            <circle cx="0" cy="0" r="46.5" style={{fill: 'url(#white_spot)', stroke: '#000000', strokeWidth: 7}}/>
            <path
                transform="scale(7,7)"
                style={{fill: '#000000', stroke: 'none'}}
                d={sign === EChargeSign.POSITIVE ? 'M 1,1 V 4 H -1 V 1 H -4 V -1 H -1 V -4 H 1 V -1 H 4 V 1 H 1 Z' : 'M 4,1 H -4 V -1 H 4 V 1 Z'}
            />
        </g>
    </svg>
);
declare module 'bbcode-to-react';
declare module '*.png';
declare module '*.jpg';
declare module '*.svg';
declare module '*.gif';

/**
 * Client application version.
 */
declare const APPLICATION_VERSION: string;

/**
 * Application config.
 *
 * REST_PATH - path to rest service.
 * SERVER_PORT - port number, set 80 for production.
 * SERVER_HOSTNAME - set real hostname for production.
 * SERVER_ADDRESS - hostname + port.
 * SERVER_REST_ADDRESS - hostname + port + rest path.
 * RUN_MODE - development or production.
 * SENTRY_CONNECTION_STRING - sentry secret.
 */
declare interface IAppConfig {
    REST_PATH: string;
    SERVER_PORT: string;
    SERVER_HOSTNAME: string;
    SERVER_ADDRESS: string;
    SERVER_REST_ADDRESS: string;
    RUN_MODE: string;
    SENTRY_CONNECTION_STRING: string;
}

declare const AppConfig: IAppConfig;

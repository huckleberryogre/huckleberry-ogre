import {green, red, lightBlue} from '@material-ui/core/colors';
import {createMuiTheme} from '@material-ui/core';
import {Theme} from '@material-ui/core';

export const ApplicationTheme: Theme = createMuiTheme({
    spacing: 4,
    palette: {
        primary: lightBlue,
        secondary: green,
        error: red,
        type: "light"
    }
});
import * as Sentry from "@sentry/react";
import {Integrations} from "@sentry/tracing";
import {history} from "./core/reduxStore";

const {SENTRY_CONNECTION_STRING} = AppConfig;

Sentry.init({
    dsn: SENTRY_CONNECTION_STRING,
    release: AppConfig.RUN_MODE,
    integrations: [
        new Integrations.BrowserTracing({
            routingInstrumentation: Sentry.reactRouterV5Instrumentation(history)
        }),
    ],
    tracesSampleRate: AppConfig.RUN_MODE === 'development' ? 1.0 : 0.5,
});

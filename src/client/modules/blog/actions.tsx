import {Action} from 'redux';
import {ThunkDispatch} from 'redux-thunk';
import {ERequestStatus} from '../../core/enums';
import {IAppState, navigateTo} from '../../core/reduxStore';
import {getLinkWithBBCode} from '../../core/utils/BBCodeParser';
import {BBCodeTag} from '../../core/enums';
import {get, post, put, remove} from '../../core/utils/ServiceUtils';
import {IBlogStateProps, TForm, messageFieldRef} from './Blog';
import {EBlogViewMode} from './enums';
import {getPost} from './utils';
import {ENotificationVariant} from "../notification/enums";
import {IPost, ITag} from "../../../models";

/**
 * Action types.
 */
export const GET_BLOG_POSTS = 'GET_BLOG_POSTS';
export const GET_ALL_TAGS = 'GET_ALL_TAGS';
export const HANDLE_FORM_INPUT = 'HANDLE_FORM_INPUT';
export const FILL_POST_EDIT_FORM = 'FILL_POST_EDIT_FORM';
export const CLEAR_POST_EDIT_FORM = 'CLEAR_POST_EDIT_FORM';
export const SET_REQUEST_STATUS = 'SET_REQUEST_STATUS';
export const UNDO = 'UNDO';
export const REDO = 'REDO';

/**
 * Blog actions.
 */
export class BlogActions {
    constructor(private dispatch: ThunkDispatch<IAppState, void, Action>) {
    }

    /**
     * Undo last action.
     */
    undo = () => this.dispatch(
        (dispatch, getState) => {
            const prevIndex = getState().blogState.formHistory.index - 1;

            if (prevIndex >= 0) {
                const prevForm = getState().blogState.formHistory.stack[prevIndex];

                dispatch({type: UNDO, payload: {formHistoryIndex: prevIndex, formFromHistory: prevForm}});
            }
        }
    );

    /**
     * Redo last action.
     */
    redo = () => this.dispatch(
        (dispatch, getState) => {
            const nextIndex = getState().blogState.formHistory.index + 1;
            const nextForm = getState().blogState.formHistory.stack[nextIndex];

            if (nextForm) {
                dispatch({type: REDO, payload: {formHistoryIndex: nextIndex, formFromHistory: nextForm}});
            }

        }
    );

    /**
     * Receive all blog posts.
     *
     * @param {IPost[]} posts Blog posts.
     */
    getBlogPosts = (posts: IPost[]) => this.dispatch({
        type: GET_BLOG_POSTS,
        payload: posts
    });

    /**
     * Receive all tags.
     */
    getAllTags = () => this.dispatch((dispatch) => {
        return get<ITag[]>('tags', null, {
            notification: {
                [ENotificationVariant.ERROR]: 'Failed loading tags'
            }
        })
            .then(
                response => {
                    dispatch({
                        type: GET_ALL_TAGS,
                        payload: response
                    });
                    return response;
                }
            );
    });

    /**
     * Clear post form.
     */
    clearPostEditForm = () => this.dispatch({
        type: CLEAR_POST_EDIT_FORM
    });

    /**
     * Submit blog post
     *
     * @param {number} id Post being updated identifier.
     * @param {EBlogViewMode} mode Blog view mode.
     */
    submitBlogPost = (id?: number, mode?: EBlogViewMode) => this.dispatch(
        (_, getState) => {
            const {title, message, tags} = getState().blogState.form;
            let request: Promise<IPost>;

            this.setRequestStatus(ERequestStatus.PENDING, 'submitStatus');

            switch (mode) {
                case EBlogViewMode.EDIT:
                    request = put<IPost>(
                        'blog',
                        {
                            id,
                            title,
                            message,
                            tags,
                        }, {
                            notification: {
                                [ENotificationVariant.SUCCESS]: 'Post updated'
                            }
                        }
                    );
                    break;
                case EBlogViewMode.CREATE:
                default:
                    request = post<IPost>(
                        'blog',
                        {
                            title,
                            message,
                            tags,
                        },
                        {
                            notification: {
                                [ENotificationVariant.SUCCESS]: 'Post created'
                            }
                        }
                    );
            }

            return request
                .then((response) => {
                    this.setRequestStatus(ERequestStatus.SUCCESS, 'submitStatus');
                    this.clearPostEditForm();
                    navigateTo('/blog');
                    return response;
                }).catch(reason => {
                    this.setRequestStatus(ERequestStatus.FAIL, 'submitStatus');

                    return reason;
                }).finally(() => {
                    this.getAllTags();
                });
        }
    );

    /**
     * Submit blog post
     *
     * @param {number} [searchBy.id] Post identifier.
     * @param {string} [searchBy.title] Post title.
     * @param {string} [searchBy.message] Post message.
     */
    requestBlogPosts = (
        searchBy?: { id?: number, title?: string, message?: string },
        matchParams?: { mode?: EBlogViewMode, postID: string }
    ) => this.dispatch(() => {
        this.setRequestStatus(ERequestStatus.PENDING, 'requestPostsStatus');

        return get<IPost[]>('blog', searchBy, {
            notification: {[ENotificationVariant.ERROR]: 'Failed loading posts'}
        })
            .then(
                response => {
                    this.getBlogPosts(response);
                    this.setRequestStatus(ERequestStatus.SUCCESS, 'requestPostsStatus');
                    if (matchParams && matchParams.mode === EBlogViewMode.EDIT && matchParams.postID) {
                        this.fillPostEditForm(+matchParams.postID);
                    }
                    return response;
                }
            );
    });

    /**
     * Remove blog post.
     *
     * @param {string} id Post identifier.
     */
    removePostByID = (id: number) => this.dispatch(
        () => {
            const body = {id};
            return remove('blog', body, {
                notification: {
                    [ENotificationVariant.ERROR]: `Failed removing post ${id}`,
                    [ENotificationVariant.INFO]: `Post ${id} removed`,
                }
            })
                .then(
                    response => {
                        this.requestBlogPosts();
                        return response;
                    }
                );
        }
    );

    /**
     * Handle post edit.
     *
     * @param {number} postID Post identifier.
     */
    fillPostEditForm = (postID: number) => this.dispatch(
        (dispatch, getState) => {
            const post = getPost(getState().blogState.posts, postID);
            post && dispatch({
                type: FILL_POST_EDIT_FORM,
                payload: post
            });
        }
    );

    /**
     * Handle form input events.
     *
     * @param {keyof TForm} fieldName Name of field.
     * @param {string} fieldValue Value.
     */
    handleFormInput = (fieldName: keyof TForm, fieldValue: string) => this.dispatch({
        type: HANDLE_FORM_INPUT,
        payload: {
            fieldName,
            fieldValue
        }
    });

    /**
     * Checks if old string is still on it's position and replace it with given new one.
     *
     * @param oldString Old string to be replaced.
     * @param newString New string to replace with.
     * @param oldSelectionStart Selection start position on paste event.
     */
    checkOldPositionAndReplace = (oldString: string, newString: string, oldSelectionStart: number) => this.dispatch(
        (__, getState) => {
            const message = getState().blogState.form.message;
            const oldStringStillThere = message.substring(oldSelectionStart, oldSelectionStart + oldString.length) === oldString;

            if (oldStringStillThere) {
                this.pasteStringIntoPosition(newString, oldSelectionStart, oldSelectionStart + oldString.length);
            }
        }
    );

    /**
     * Handle url paste for message field.
     *
     * @param link Paste link.
     */
    handleUrlPaste = (link: string) => this.dispatch(
        async () => {
            const selectionStart = messageFieldRef.selectionStart;
            const selectionEnd = messageFieldRef.selectionEnd;
            const selectionEndFinal = link.length + selectionStart;

            this.pasteStringIntoPosition(link, selectionStart, selectionEnd);
            this.focusMessageField(selectionEndFinal, selectionEndFinal);

            const wrappedLink = await getLinkWithBBCode(link);

            if (wrappedLink !== link) {
                this.checkOldPositionAndReplace(link, wrappedLink, selectionStart);
                const selectionEndFinal = wrappedLink.length + selectionStart;
                this.focusMessageField(selectionEndFinal, selectionEndFinal);
            }
        }
    );

    /**
     * Set focus on message field.
     *
     * @param selectionStart Selection start option.
     * @param selectionEnd Selection end option.
     */
    focusMessageField = (selectionStart: number = messageFieldRef.selectionStart, selectionEnd: number = messageFieldRef.selectionEnd) => {
        setTimeout(() => {
            messageFieldRef.setSelectionRange(selectionStart, selectionEnd);
            messageFieldRef.focus();
        }, 0);
    };

    /**
     * Wraps selected text with BBCode tag (or just insert into cursor position if no text selected).
     *
     * @param code BBCode tag.
     * @param [params] Params for given tag.
     */
    wrapSelectionWithBBCode = (code: BBCodeTag, params?: string) => this.dispatch(
        (__, getState) => {
            const selectionStart = messageFieldRef.selectionStart;
            const selectionEnd = messageFieldRef.selectionEnd;
            const message = getState().blogState.form.message;
            const selectionString = message.slice(selectionStart, selectionEnd);
            const tagParams = params ? ' ' + params : '';
            const newLines = code === BBCodeTag.WRAP ? '\n' : '';

            const codeOpenTag = `[${code}${tagParams}]${newLines}`;
            const codeCloseTag = `${newLines}[/${code}]`;
            const wrappedString = codeOpenTag + selectionString + codeCloseTag;

            this.pasteStringIntoPosition(wrappedString, selectionStart, selectionEnd);

            this.focusMessageField(selectionStart + codeOpenTag.length, selectionStart + codeOpenTag.length + selectionString.length);
        }
    );

    /**
     * Helper method to insert given string into message cursor position.
     *
     * @param stringToPaste Given string.
     * @param selectionStart Selection start position.
     * @param selectionEnd Selection end position
     */
    pasteStringIntoPosition = (stringToPaste: string, selectionStart: number = messageFieldRef.selectionStart, selectionEnd: number = messageFieldRef.selectionEnd) => this.dispatch(
        (__, getState) => {
            const message = getState().blogState.form.message;

            this.handleFormInput(
                'message',
                message.slice(0, selectionStart) + stringToPaste + message.slice(selectionEnd)
            );
        }
    );

    /**
     * Set submit status.
     *
     * @param {ERequestStatus} status Status.
     * @param {string} propertyName Name of property to set.
     */
    setRequestStatus = (status: ERequestStatus, propertyName: keyof IBlogStateProps) => this.dispatch({
        type: SET_REQUEST_STATUS,
        payload: {
            status,
            propertyName
        }
    });

}

import {Reducer} from 'redux';
import {
    CLEAR_POST_EDIT_FORM,
    GET_BLOG_POSTS,
    HANDLE_FORM_INPUT,
    FILL_POST_EDIT_FORM,
    SET_REQUEST_STATUS,
    UNDO,
    REDO, GET_ALL_TAGS,
} from './actions';
import {IBlogStateProps} from './Blog';
import {ERequestStatus} from '../../core/enums';

/**
 * Initial blog state.
 */
export const initBlogState: IBlogStateProps = {
    posts: [],
    form: {
        title: '',
        message: ''
    },
    formHistory: {
        index: 0,
        stack: [{
            title: '',
            message: ''
        }]
    },
    submitStatus: ERequestStatus.NONE,
    requestPostsStatus: ERequestStatus.NONE,
    tags: [],
};

/**
 * Blog reducer.
 */
export const blogState: Reducer<IBlogStateProps> = (state: IBlogStateProps = initBlogState, action) => {
    const {status, propertyName, formHistoryIndex, formFromHistory, fieldValue} = action.payload || {} as any;
    const {form, formHistory} = state;

    switch (action.type) {
        case CLEAR_POST_EDIT_FORM:
            return {
                ...state,
                form: initBlogState.form,
                formHistory: initBlogState.formHistory
            };
        case GET_BLOG_POSTS:
            return {
                ...state,
                posts: action.payload
            };
        case GET_ALL_TAGS:
            return {
                ...state,
                tags: action.payload
            };
        case HANDLE_FORM_INPUT:
            const nextForm = {
                ...form,
                [action.payload.fieldName]: fieldValue,
            };

            return {
                ...state,
                form: nextForm,
                formHistory: {
                    index: formHistory.index + 1,
                    stack: [...formHistory.stack.slice(0, formHistory.index + 1), nextForm]
                }
            };
        case FILL_POST_EDIT_FORM:
            return {
                ...state,
                form: {
                    ...action.payload,
                },
                formHistory: {
                    index: 0,
                    stack: [{...action.payload}]
                }
            };
        case SET_REQUEST_STATUS:
            return {
                ...state,
                [propertyName]: status
            };
        case UNDO:
        case REDO:
            return {
                ...state,
                form: formFromHistory,
                formHistory: {
                    index: formHistoryIndex,
                    stack: state.formHistory.stack
                }
            };
        default:
            return state;
    }
};

import find from 'lodash/find';
import {IPost} from "../../../models";

/**
 * Get post by id.
 *
 * @param {IPost[]} posts Array of posts.
 * @param {number} postID Post identifier.
 */
export const getPost = (posts: IPost[], postID: number): IPost | undefined => {
    return find(posts, post => post.id === postID);
};

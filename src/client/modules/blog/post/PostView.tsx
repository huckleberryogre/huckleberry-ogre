import React, {Ref} from "react";
import {orderBy} from 'lodash';
import classNames from "classnames";
import {ERequestStatus, ERole} from "../../../core/enums";
import LinearProgress from "@material-ui/core/LinearProgress";
import {Divider, Fab, Grid, IconButton, Paper, Typography} from "@material-ui/core";
import BorderColorIcon from '@material-ui/icons/BorderColor';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from "@material-ui/icons/Delete";
import {TStyleProps} from "../Blog";
import {ILoginStateProps} from "../../login/Login";
import {PostViewBody} from "./PostViewBody";
import {IPostExtended} from "../../../../models";

/**
 * Component props.
 */
interface IPostViewProps extends TStyleProps, ILoginStateProps {
    collapsedPostIds: Map<string, boolean>;
    requestPostsStatus: ERequestStatus;
    posts: IPostExtended[];
    onCreatePostButtonClick: () => void;
    onPostRemove: (postID: number) => void;
    onPostEdit: (postID: number) => void;
    setPostRef: (ref: Ref<HTMLDivElement>, postID: number) => void;
    toggleCollapsedPostId: (postID: string) => void;
    onPostTitleClick: (postID: number) => void;
}

/**
 * Renders blog post view.
 *
 * @param classes Styles.
 * @param posts Posts.
 * @param requestPostsStatus Status for posts request.
 * @param user User.
 * @param onCreatePostButtonClick Create post callback.
 * @param onPostRemove Remove post callback.
 * @param onPostEdit Edit post callback.
 * @param collapsedPostIds Array of ids of which posts are collapsed.
 * @param setPostRef Sets post ref.
 * @param toggleCollapsedPostId Flush id field for the collapsed post.
 * @param onPostTitleClick Post title click callback.
 */
export const PostView = (
    {
        classes,
        posts,
        requestPostsStatus,
        user,
        onCreatePostButtonClick,
        onPostRemove,
        onPostEdit,
        collapsedPostIds,
        setPostRef,
        toggleCollapsedPostId,
        onPostTitleClick,
    }: IPostViewProps
) => {
    const isAdmin = user && user.role === ERole.ADMIN;

    return (
        <>
            <div
                className={classNames(classes.progress, {['active']: requestPostsStatus === ERequestStatus.PENDING})}
            >
                <LinearProgress/>
            </div>
            {orderBy(posts, 'createdAt', 'desc').map(post => {
                return post.message && (
                    <Paper
                        className={classes.postPaper}
                        ref={(ref: Ref<HTMLDivElement>) => setPostRef(ref, post.id)}
                        key={post.id}
                    >
                        <Grid
                            className={classNames(
                                'postContainer',
                                {[classes.postContainerCollapsed]: collapsedPostIds.get(post.id.toString())}
                            )}
                            container
                        >
                            <Grid item md={isAdmin ? 11 : 12} xs={12}>
                                <PostViewBody
                                    classes={classes}
                                    message={post.message}
                                    title={post.title}
                                    createdAt={post.createdAt}
                                    postID={post.id}
                                    onPostTitleClick={onPostTitleClick}
                                    tags={post.tags}
                                />
                            </Grid>
                            {isAdmin &&
                            <Divider className={classNames(classes.postDivider, 'display-none-md-up')}/>}
                            {isAdmin && (
                                <Grid item md={1} xs={12} className={classes.postActions}>
                                    <IconButton
                                        onClick={(event) => {
                                            event.stopPropagation();
                                            post.id && onPostRemove(post.id);
                                        }}
                                        aria-label="Remove post"
                                        className={classes.postActionButtonIcon}
                                    >
                                        <DeleteIcon/>
                                    </IconButton>
                                    <IconButton
                                        onClick={(event) => {
                                            event.stopPropagation();
                                            post.id && onPostEdit(post.id);
                                        }}
                                        aria-label="Edit post"
                                        className={classes.postActionButtonIcon}
                                    >
                                        <BorderColorIcon/>
                                    </IconButton>
                                </Grid>
                            )}
                        </Grid>
                        {collapsedPostIds.has(post.id.toString()) && (
                            <div className={classNames(classes.showMoreBar, {['position-relative']: !collapsedPostIds.get(post.id.toString())})}
                                 onClick={() => toggleCollapsedPostId(post.id.toString())}>
                                <Typography variant='button' className={classes.showMoreCaption}>
                                    {collapsedPostIds.get(post.id.toString()) ? 'more' : 'less'}
                                </Typography>
                            </div>
                        )}
                    </Paper>
                );
            }

            )}
            {isAdmin && (
                <Fab
                    color="primary"
                    aria-label="edit"
                    className={classes.buttonCreate}
                    onClick={onCreatePostButtonClick}
                >
                    <EditIcon/>
                </Fab>
            )}
        </>
    );
};

import React from "react";
import {Divider, Typography, Chip} from "@material-ui/core";
import {parser} from "../../../core/utils/BBCodeParser";
import {TForm, TStyleProps} from "../Blog";
import moment from "moment";

/**
 * Component props.
 */
type TPostEditPreview = TStyleProps & TForm & {
    createdAt?: string;
    onPostTitleClick?: (postID: number) => void;
    postID?: number;
};

/**
 * Renders post view body.
 *
 * @param classes Styles.
 * @param title Post title.
 * @param message Post message.
 * @param [createdAt] Post creation date.
 * @param [postId] Post id.
 * @param [onPostTitleClick] Title click callback.
 */
export const PostViewBody = (
    {
        classes,
        title,
        message,
        createdAt,
        postID,
        onPostTitleClick,
        tags
    }: TPostEditPreview
) => (
    <>
        <Typography
            variant="h6"
            align={'center'}
            onClick={() => onPostTitleClick && onPostTitleClick(postID)}
            className={classes.postTitle}
        >
            <span className='text-uppercase'>{title ? title + ' · ' : ''}</span>
            {createdAt && <span className='text-muted'>{moment(createdAt).format('D MMM YY')}</span>}
        </Typography>
        <div className={classes.tags}>
            {tags.map(tag => (
                <Chip
                    key={tag.name}
                    className={classes.tagContainer}
                    variant="outlined"
                    size="small"
                    label={<span><strong>#</strong> {tag.name}</span>}
                    color="primary"
                />
            ))}
        </div>
        <Divider className={classes.postDivider}/>
        <Typography variant="body1" component={'div'} className={classes.messageContainer}>
            {parser.toReact(message.replace(/li]\n/g, 'li]'))}
        </Typography>
    </>
);

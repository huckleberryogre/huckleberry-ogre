import React, {Ref} from "react";
import {Button, Grid, Paper, Tab, Tabs, TextField} from "@material-ui/core";
import {
    Camera as CameraIcon,
    Undo as UndoIcon,
    Redo as RedoIcon,
    WrapText as WrapTextIcon,
    Code as CodeIcon,
    ShortText as ShortTextIcon,
    List as ListIcon,
} from '@material-ui/icons';
import {ETabIndex} from "../enums";
import {BBCodeTag} from "../../../core/enums";
import {ERequestStatus} from "../../../core/enums";
import {TForm, IFormHistory, TStyleProps} from "../Blog";
import {BlogActions} from "../actions";
import {PostViewBody} from "./PostViewBody";
import {Autocomplete, createFilterOptions} from "@material-ui/lab";
import {ITag} from "../../../../models";

/**
 * Component props.
 */
interface IPostEditProps extends TStyleProps, TForm {
    tabIndex: number;
    submitStatus: ERequestStatus;
    formHistory: IFormHistory;
    setMessageFieldRef: (ref: Ref<HTMLInputElement>) => void;
    blogActions: BlogActions;
    onMessageKeyPress: (event: React.KeyboardEvent) => void;
    onTabChange: (__: React.ChangeEvent, tabIndex: number) => void;
    onFilesManagerShow: () => void;
    onMessagePaste: any;
    onSubmit: () => void;
    tags: ITag[];
    postTags: ITag[];
}

/**
 * Renders blog post edit.
 *
 * @param classes Styles.
 * @param tabIndex Tab index.
 * @param title Post title.
 * @param message Post message.
 * @param submitStatus Post submit status.
 * @param blogActions Blog actions.
 * @param formHistory Form history.
 * @param setMessageFieldRef Message field ref setter callback.
 * @param onMessageKeyPress Message keypress callback.
 * @param onTabChange Tab change callback.
 * @param onFilesManagerShow Files manager show callback.
 * @param onMessagePaste Message paste callback.
 * @param onSubmit Submit callback.
 * @param createdAt Post creation date.
 * @param tags All tags.
 * @param postTags Post tags.
 */
export const PostEdit = (
    {
        classes,
        tabIndex,
        title,
        message,
        submitStatus,
        blogActions,
        formHistory,
        setMessageFieldRef,
        onMessageKeyPress,
        onTabChange,
        onFilesManagerShow,
        onMessagePaste,
        onSubmit,
        createdAt,
        tags = [],
        postTags = [],
    }: IPostEditProps
) => {
    const handleTagsChange = (_, value, reason) => {
        switch (reason) {
            case 'select-option':
                blogActions.handleFormInput('tags', value.map((tag, index) => tag.id ? tag : {name: index === value.length - 1 ? tag.inputValue : tag.name}));
                break;
            case 'remove-option':
                blogActions.handleFormInput('tags', value);
                break;
            default:
                blogActions.handleFormInput('tags', value.map(tag => ({name: tag.inputValue || tag.name})));
        }
    };

    const filter = createFilterOptions();

    return (
        <div className={`text-center ${classes.postEdit}`} onKeyDown={onMessageKeyPress}>
            <Tabs value={tabIndex} onChange={onTabChange}>
                <Tab label="edit"/>
                <Tab label="preview"/>
            </Tabs>
            <Paper className={classes.postPaper}>
                <Grid container>
                    <Grid item xs={12}>
                        {tabIndex === ETabIndex.EDIT && (
                            <form className={classes.form} noValidate>
                                <TextField
                                    placeholder="Summary"
                                    label="Title"
                                    value={title}
                                    onChange={event => blogActions.handleFormInput('title', event.currentTarget.value)}
                                    margin="normal"
                                    fullWidth
                                    autoComplete="off"
                                    name="title"
                                />
                                <Autocomplete
                                    multiple
                                    handleHomeEndKeys={false}
                                    value={postTags}
                                    onChange={handleTagsChange}
                                    size="small"
                                    options={tags || []}
                                    getOptionLabel={(option: ITag) => option.name}
                                    renderInput={(params) => (
                                        <TextField {...params} variant="outlined" label="tags" placeholder="add tag"/>
                                    )}
                                    getOptionSelected={(option, value) => value.name === option.name}
                                    filterOptions={(options, params) => {
                                        const filtered = filter(options, params);

                                        // Suggest the creation of a new value
                                        if (params.inputValue !== '') {
                                            filtered.push({
                                                inputValue: params.inputValue,
                                                name: `add #${params.inputValue}`,
                                            });
                                        }

                                        return filtered;
                                    }}
                                />
                                <span className={classes.actionPanel + ' pull-left'}>
                                <Button
                                    variant="text"
                                    component="span"
                                    onClick={onFilesManagerShow}
                                    size='small'
                                    className={classes.actionPanelButton}
                                >
                                    {'Add media'}
                                    <CameraIcon className={classes.actionPanelIcon}/>
                                </Button>
                                <Button
                                    variant="text"
                                    component="span"
                                    onClick={() => blogActions.wrapSelectionWithBBCode(BBCodeTag.LIST)}
                                    size='small'
                                    className={classes.actionPanelButton}
                                >
                                    {'List'}
                                    <ListIcon className={classes.actionPanelIcon}/>
                                </Button>
                                <Button
                                    variant="text"
                                    component="span"
                                    onClick={() => blogActions.wrapSelectionWithBBCode(BBCodeTag.BOLD)}
                                    size='small'
                                    className={classes.actionPanelButton}
                                >
                                    <strong className='fw900'>Bold</strong>
                                </Button>
                                <Button
                                    variant="text"
                                    component="span"
                                    onClick={() => blogActions.wrapSelectionWithBBCode(BBCodeTag.ITALIC)}
                                    size='small'
                                    className={classes.actionPanelButton}
                                >
                                    <i>Italic</i>
                                </Button>
                                <Button
                                    variant="text"
                                    component="span"
                                    onClick={() => blogActions.wrapSelectionWithBBCode(BBCodeTag.UNDERLINE)}
                                    size='small'
                                    className={classes.actionPanelButton}
                                >
                                    <u>Underline</u>
                                </Button>
                                <Button
                                    variant="text"
                                    component="span"
                                    onClick={() => blogActions.wrapSelectionWithBBCode(BBCodeTag.STRIKE)}
                                    size='small'
                                    className={classes.actionPanelButton}
                                >
                                    <s>Strike</s>
                                </Button>
                                <Button
                                    variant="text"
                                    component="span"
                                    onClick={() => blogActions.wrapSelectionWithBBCode(BBCodeTag.CODE, `language="react"`)}
                                    size='small'
                                    className={classes.actionPanelButton}
                                >
                                    {'Code'}
                                    <CodeIcon className={classes.actionPanelIcon}/>
                                </Button>
                                <Button
                                    variant="text"
                                    component="span"
                                    onClick={() => blogActions.wrapSelectionWithBBCode(BBCodeTag.WRAP, `className="postContentWrapper"`)}
                                    size='small'
                                    className={classes.actionPanelButton}
                                >
                                    {'Wrap gallery'}
                                    <WrapTextIcon className={classes.actionPanelIcon}/>
                                </Button>
                                <Button
                                    variant="text"
                                    component="span"
                                    onClick={() => blogActions.wrapSelectionWithBBCode(BBCodeTag.CAPTION)}
                                    size='small'
                                    className={classes.actionPanelButton}
                                >
                                    {'Caption'}
                                    <ShortTextIcon className={classes.actionPanelIcon}/>
                                </Button>
                            </span>
                                <div className={classes.actionPanel + ' pull-right'}>
                                    <Button
                                        variant="text"
                                        component="span"
                                        onClick={blogActions.redo}
                                        size='small'
                                        className={classes.actionPanelButton}
                                        disabled={formHistory.index === formHistory.stack.length - 1}
                                    >
                                        {'Redo'}
                                        <RedoIcon className={classes.actionPanelIcon}/>
                                    </Button>
                                    <Button
                                        variant="text"
                                        component="span"
                                        onClick={blogActions.undo}
                                        size='small'
                                        className={classes.actionPanelButton}
                                        disabled={formHistory.index === 0}
                                    >
                                        {'Undo'}
                                        <UndoIcon className={classes.actionPanelIcon}/>
                                    </Button>
                                </div>
                                <TextField
                                    placeholder="Today I learned..."
                                    label="Message"
                                    name="message"
                                    className='margin-top-0'
                                    multiline
                                    fullWidth
                                    value={message}
                                    rows={'21'}
                                    onChange={event => blogActions.handleFormInput('message', event.currentTarget.value)}
                                    margin="normal"
                                    type={'text'}
                                    onPaste={onMessagePaste}
                                    inputRef={setMessageFieldRef}
                                />
                            </form>)
                        }
                        {tabIndex === ETabIndex.PREVIEW && (
                            <PostViewBody
                                classes={classes}
                                message={message}
                                title={title}
                                createdAt={createdAt}
                                tags={postTags}
                            />
                        )}
                    </Grid>
                </Grid>
            </Paper>
            <Button
                onClick={onSubmit}
                variant="contained"
                color="primary"
                className={classes.button}
                disabled={submitStatus === ERequestStatus.PENDING}
            >
                {'Submit'}
            </Button>
        </div>
    );
};

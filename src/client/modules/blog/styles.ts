import {Theme, createStyles} from '@material-ui/core';

export const BlogStyles = (theme: Theme) => createStyles({
    '@global': {
        '.postContentWrapper': {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-around',
            flexWrap: 'wrap'
        },
    },
    form: {
        padding: `0 ${theme.spacing(2)}`
    },
    button: {
        margin: theme.spacing(2),
    },
    buttonCreate: {
        position: 'fixed',
        right: '40px',
        bottom: '40px',
        [theme.breakpoints.down('sm')]: {
            right: '20px',
            bottom: '20px',
        }
    },
    actionPanel: {
        display: 'inline-flex',
        flexWrap: 'wrap',
        textAlign: 'left',
        marginTop: '8px',
        marginBottom: '8px',
        [theme.breakpoints.down('xs')]: {
            flexWrap: 'wrap',
            justifyContent: 'space-evenly'
        },

        '&:hover hr': {
            borderStyle: 'solid'
        }
    },
    actionPanelRightContainer: {
        marginLeft: 'auto',
        [theme.breakpoints.down('xs')]: {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-evenly',
            width: '100%'
        },

    },
    actionPanelButton: {
        borderLeft: '1px solid black',
        borderRight: '1px solid black',
        minWidth: 90,
        marginTop: 5,
        marginBottom: 5,

        '&:not(:last-of-type)': {
            marginRight: 8
        },
        '&:hover': {
            filter: 'drop-shadow(3px 3px 2px rgb(1, 1, 1, 0.3))',
            fontWeight: 600,
            backgroundColor: 'initial',
            textDecoration: 'initial',
        }
    },
    actionPanelIcon: {
        marginLeft: '5px'
    },
    postDivider: {
        width: '90%',
        margin: '8px auto 16px',
    },
    postTitle: {
        cursor: 'pointer',
    },
    postPaper: {
        position: 'relative',
        padding: `${theme.spacing(6)}px ${theme.spacing(8)}px`,
        boxSizing: 'border-box',
        marginBottom: '24px',
        wordWrap: 'break-word',
        minHeight: '200px',
        whiteSpace: 'pre-wrap',

        [theme.breakpoints.down('xs')]: {
            padding: `${theme.spacing(2)}px ${theme.spacing(3)}px`,
        },
        [theme.breakpoints.between('sm', 'md')]: {
            padding: `${theme.spacing(3)}px ${theme.spacing(4)}px`,
        },

        '@global': {
            'div[role="caption"]': {
                width: '100%',
                textAlign: 'center',
                whiteSpace: 'normal',
                color: theme.palette.primary.dark,
                fontStyle: 'italic'
            },
        },
        '& img': {
            alignSelf: 'flex-start',
            margin: `${theme.spacing(3)}px auto`,
            [theme.breakpoints.down('sm')]: {
                maxWidth: '100%',
                maxHeight: 'calc(100vh - 100px)',
            },
            [theme.breakpoints.between('md', 'lg')]: {
                maxWidth: '33%',
                maxHeight: 'calc(100vh - 150px)',
            },
            [theme.breakpoints.up('lg')]: {
                maxWidth: '33%',
                maxHeight: 'calc(100vh - 200px)',
            },
        },
        '& .placeholderImage': {
            alignSelf: 'flex-start',
            margin: `${theme.spacing(3)}px auto`,
            [theme.breakpoints.down('sm')]: {
                width: '100%',
            },
            [theme.breakpoints.between('md', 'lg')]: {
                width: '33%',
            },
            [theme.breakpoints.up('lg')]: {
                width: '33%',
            },
        },
        '&:last-of-type': {
            marginBottom: 'initial'
        },
    },
    postEdit: {
        [theme.breakpoints.down('xs')]: {
            paddingBottom: theme.spacing(15)
        },
    },
    postContainerCollapsed: {
        maxHeight: 'calc(100vh - 100px)',
        overflow: 'hidden'
    },
    showMoreBar: {
        position: 'absolute',
        display: 'flex',
        textAlign: 'center',
        width: '100%',
        height: '75px',
        left: 0,
        bottom: 0,
        background: 'linear-gradient(180deg, transparent 0%, rgba(255,255,255, 0.6) 25%, rgba(255,255,255, 0.8) 50%, rgba(255,255,255) 75%, white)',
        cursor: 'pointer',

        '&:hover': {
            '& $showMoreCaption': {
                color: theme.palette.primary.main,
                filter: 'drop-shadow(3px 3px 2px rgb(1, 1, 1, 0.3))',
            }
        },
    },
    showMoreCaption: {
        margin: '30px auto',
    },
    postActions: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
        [theme.breakpoints.down('sm')]: {
            justifyContent: 'center',
            flexDirection: 'row'
        },
        '& button': {
            width: theme.spacing(12),
            height: theme.spacing(12)
        }
    },
    postActionButtonIcon: {
        color: theme.palette.primary.contrastText,

        '&:hover': {
            color: theme.palette.secondary.dark
        }
    },
    progress: {
        width: '100%',
        textAlign: 'center',
        transform: 'scale3d(1,0,1)',
        transition: '.3s',

        '&.active': {
            transform: 'scale3d(1,1,1)',
            marginBottom: '20px',
        }
    },
    messageContainer: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'left',

        '& strike, & u, & em, & strong, & a': {
            display: 'contents'
        },
        '& a': {
            color: theme.palette.primary.main,
            '&:hover': {
                color: theme.palette.primary.light,
            },
            '&:active': {
                color: theme.palette.primary.dark,
            },
            '&:visited': {
                color: theme.palette.secondary.dark,
            },
        },
        '& iframe': {
            [theme.breakpoints.down('xs')]: {
                width: '100%',
                height: 230
            },
            [theme.breakpoints.up('sm')]: {
                width: 540,
                height: 330
            },
        }
    },
    tags: {
        marginTop: theme.spacing(2),
    },
    tagContainer: {
        marginRight: theme.spacing(2),
    }
});

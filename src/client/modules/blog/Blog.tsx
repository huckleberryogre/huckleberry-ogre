import ResizeObserver from 'resize-observer-polyfill';
import {Grid, WithStyles,} from '@material-ui/core';
import React, {Component} from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {Action} from 'redux';
import {ThunkDispatch} from 'redux-thunk';
import {ERegExp, ERequestStatus, ERole} from '../../core/enums';
import {IAppState, navigateTo} from '../../core/reduxStore';
import {isMatchRegExp} from '../../core/utils/utils';
import {HOC} from '../../core/utils/HOC';
import {FilesManagerActions} from '../filesManager/actions';
import {ILoginStateProps} from '../login/Login';
import {BlogActions} from './actions';
import {EBlogViewMode} from './enums';
import {BlogStyles} from './styles';
import {PostView} from "./post/PostView";
import {PostEdit} from "./post/PostEdit";
import {Location} from "history";
import RouteParser from "route-parser";
import {IPostExtended, ITag} from "../../../models";

/**
 * Form fields set. Equals post model.
 */
export type TForm = IPostExtended;

/**
 * Undo\redo mechanism support interface.
 */
export interface IFormHistory {
    index: number;
    stack: TForm[];
}

/**
 * Blog own props.
 *
 * posts Array of posts from server.
 * form Form fields set.
 * submitStatus Post submit status.
 * requestPostsStatus Posts request status.
 * formHistory Undo\redo mechanism support.
 * tags All tags.
 */
export interface IBlogStateProps {
    posts: IPostExtended[];
    form: TForm;
    submitStatus: ERequestStatus;
    requestPostsStatus: ERequestStatus;
    formHistory: IFormHistory;
    tags: ITag[];
}

/**
 * Blog props.
 */
type TDispatchProps = { blogActions: BlogActions, filesManagerActions: FilesManagerActions }
type TRouteProps = RouteComponentProps<{ mode: EBlogViewMode, postID: string }>;
export type TStyleProps = WithStyles<typeof BlogStyles>;

/**
 * Blog state.
 *
 * @prop {number} tabIndex Tab index.
 * @prop {number[]} collapsedPostIds Map of ids of which posts are collapsed.
 */
export interface IBlogState {
    tabIndex: number;
    collapsedPostIds: Map<string, boolean>;
}

/**
 * Message field reference. To be used anywhere in app.
 */
export let messageFieldRef: HTMLTextAreaElement;

/**
 * Blog.
 */
class BlogComponent extends Component<IBlogStateProps & ILoginStateProps & TRouteProps & TStyleProps & TDispatchProps, IBlogState> {

    constructor(props: IBlogStateProps & ILoginStateProps & TRouteProps & TStyleProps & TDispatchProps) {
        super(props);
        const {history, match: {params: {mode, postID}}, blogActions} = this.props;

        history.listen(location => this.handleLocationChange(location));
        this.requestBlogPosts({mode, postID});
        blogActions.getAllTags();
    }

    static defaultProps = {
        tags: [],
    };

    /**
     * Location change handler.
     *
     * @param {Location} newLocation Location object.
     */
    handleLocationChange = (newLocation: Location) => {
        const {blogActions, location: oldLocation} = this.props;
        const newLocationPathName = newLocation.pathname;
        const oldLocationPathName = oldLocation.pathname;
        const blogPageRoute = new RouteParser('/blog(/:mode)(/:postID)');
        const newLocationBlogMatchParams = blogPageRoute.match(newLocationPathName);
        const oldLocationBlogMatchParams = blogPageRoute.match(oldLocationPathName);

        if (oldLocationBlogMatchParams && oldLocationBlogMatchParams.mode === EBlogViewMode.EDIT) {
            // cheap solution - is to clean edit form before leaving
            // this way i can keep changes for CREATE mode
            // if i want the app to be saving changes for both states i'd need to think of
            // keeping form state for both mode (and this gonna be messy)
            //
            // Lets stick with this solution for now
            blogActions.clearPostEditForm();
        }

        if (newLocationBlogMatchParams || newLocationPathName === '/') {
            const {mode, postID} = newLocationBlogMatchParams || {};
            this.requestBlogPosts({mode, postID});
        }
    };

    /**
     * Requests single post or all of them.
     *
     * @param mode For the EDIT mode form will be filled with the received post.
     * @param [postID] Requested post id.
     */
    requestBlogPosts = ({mode, postID}) => {
        const {blogActions} = this.props;

        if (postID) {
            blogActions.requestBlogPosts({id: +postID}).then(() => {
                if (mode === EBlogViewMode.EDIT) {
                    blogActions.fillPostEditForm(+postID);
                }
            })
        } else {
            blogActions.requestBlogPosts();
        }
    };

    state: IBlogState = {
        tabIndex: 0,
        collapsedPostIds: new Map(),
    };

    /**
     * Post paper nodes map (key === postID).
     */
    postRefs = {};

    /**
     * Post paper resize observer instances (key === postID).
     */
    postResizeObservers = {};

    /**
     * Post resize event handler. Tracking post size until fist interaction (opening full post / more button).
     */
    handlePostResizeEvent = (entries: ReadonlyArray<ResizeObserverEntry>) => {
        for (let entry of entries) {
            const [postId] = Object.entries<HTMLDivElement>(this.postRefs).find(([_, elem]) => elem === entry.target) || [];
            postId !== undefined && this.setCollapsedPostId(postId, entry.contentRect.height);
        }
    };

    componentDidMount(): void {
        Object.keys(this.postRefs).forEach(postID => {
            this.postResizeObservers[postID] = new ResizeObserver(this.handlePostResizeEvent);
            this.postResizeObservers[postID].observe(this.postRefs[postID]);
        });
    }

    /**
     * Checks if post needs to be collapsed and adds it to collapsedPostIds map.
     *
     * @param postID Post id.
     * @param postHeight New post height.
     */
    setCollapsedPostId = (postID: string, postHeight: number) => {
        const {collapsedPostIds} = this.state;
        const newCollapsedPostIds = new Map(collapsedPostIds);

        if (!collapsedPostIds.get(postID)) {
            const windowHeight = window.innerHeight;

            if (postHeight / windowHeight > 0.8) {
                newCollapsedPostIds.set(postID, true);
                this.setState({collapsedPostIds: newCollapsedPostIds});
            }
        }
    };

    /**
     * Disconnects resize observer and removes it from the list.
     * @param postID
     */
    deletePostResizeObserver = (postID: string) => {
        if (this.postResizeObservers[postID]) {
            this.postResizeObservers[postID].disconnect();
            delete this.postResizeObservers[postID];
        }
    };

    /**
     * Read more click handler.
     *
     * @param postID Post id.
     */
    toggleCollapsedPostId = (postID: string) => {
        const {collapsedPostIds} = this.state;
        const newCollapsedPostIds = new Map(collapsedPostIds);

        if (collapsedPostIds.get(postID)) {
            this.deletePostResizeObserver(postID);
            newCollapsedPostIds.set(postID, false);
        } else {
            newCollapsedPostIds.set(postID, true);
        }

        this.setState({collapsedPostIds: newCollapsedPostIds});
    };

    /**
     * Submit button handler.
     */
    handleSubmit = () => {
        const {mode, postID} = this.props.match.params;
        this.props.blogActions.submitBlogPost(+postID, mode);
    };

    /**
     * Create post button click handler.
     */
    handleCreatePostButtonClick = () => {
        navigateTo('/blog/create');
    };

    /**
     * Remove post button click handler.
     *
     * @param {number} postID Identifier of post being removed.
     */
    handlePostRemove = (postID: number) => {
        this.props.blogActions.removePostByID(postID);
    };

    /**
     * Edit post button click handler.
     *
     * @param {number} postID Post identifier.
     */
    handlePostEdit = (postID: number) => {
        this.props.blogActions.fillPostEditForm(postID);
        navigateTo(`/blog/edit/${postID}`);
    };

    /**
     * Tab change handler.
     *
     * @param {ChangeEvent} __ Event.
     * @param {number} tabIndex New tab index.
     */
    handleTabChange = (__: React.ChangeEvent, tabIndex: number) => {
        this.setState({tabIndex});
    };

    /**
     * Paste callback.
     *
     * @param event Event object.
     */
    handleMessagePaste = (event: ClipboardEvent) => {
        const {blogActions, filesManagerActions} = this.props;

        const text = event.clipboardData.getData('text');
        const files = event.clipboardData.files;

        if (isMatchRegExp(text, ERegExp.SITE_FULL_URL)) {
            event.preventDefault();
            const {blogActions} = this.props;

            blogActions.handleUrlPaste(text);
        }
        if (files && files.length > 0) {
            filesManagerActions.uploadFiles(files).then((uploadedFiles) => {
                blogActions.handleUrlPaste(`${AppConfig.SERVER_REST_ADDRESS}/file/${uploadedFiles[0].uuid}`);
            })
        }
    };

    /**
     * Handles key press on message input field.
     *
     * @param {KeyboardEvent} event Keyboard press event.
     */
    handleMessageKeyPress = (event: React.KeyboardEvent) => {
        const {blogActions} = this.props;

        if (event.ctrlKey) {
            if (event.keyCode === 89) {
                event.preventDefault();
                blogActions.redo();
            }
            if (event.keyCode === 90) {
                event.preventDefault();
                event.shiftKey ? blogActions.redo() : blogActions.undo();
            }
        }

    };

    /**
     * Add media button click callback.
     */
    handleFilesManagerShow = () => {
        const {filesManagerActions} = this.props;

        filesManagerActions.show();
    };

    /**
     * Sets message field ref.
     *
     * @param ref The reference.
     */
    setMessageFieldRef = (ref) => {
        messageFieldRef = ref;
    };

    /**
     * Sets message field ref.
     *
     * @param ref The reference.
     * @param postID Post id.
     */
    setPostRef = (ref, postID) => {
        this.postRefs[postID] = ref;
    };

    /**
     * Handles post title click. Redirects to a post read page.
     *
     * @param postID Post id.
     */
    handlePostTitleClick = (postID: number) => {
        const {history: {push}} = this.props;

        push('/blog/read/' + postID);
    };

    /**
     * Edit view mode render method.
     */
    renderPostEdit = () => {
        const {
            classes,
            form: {title, message, createdAt, tags: postTags},
            submitStatus,
            blogActions,
            formHistory,
            tags
        } = this.props;

        const {tabIndex} = this.state;

        return (
            <PostEdit
                tags={tags}
                createdAt={createdAt}
                classes={classes}
                tabIndex={tabIndex}
                title={title}
                message={message}
                postTags={postTags}
                submitStatus={submitStatus}
                blogActions={blogActions}
                formHistory={formHistory}
                setMessageFieldRef={this.setMessageFieldRef}
                onMessageKeyPress={this.handleMessageKeyPress}
                onTabChange={this.handleTabChange}
                onFilesManagerShow={this.handleFilesManagerShow}
                onMessagePaste={this.handleMessagePaste}
                onSubmit={this.handleSubmit}
            />
        )
    };

    /**
     * Read view mode render method.
     */
    renderPostView = () => {
        const {classes, posts, requestPostsStatus, user} = this.props;
        const {collapsedPostIds} = this.state;

        return (
            <PostView
                classes={classes}
                posts={posts}
                requestPostsStatus={requestPostsStatus}
                user={user}
                collapsedPostIds={collapsedPostIds}
                onCreatePostButtonClick={this.handleCreatePostButtonClick}
                onPostRemove={this.handlePostRemove}
                onPostEdit={this.handlePostEdit}
                setPostRef={this.setPostRef}
                toggleCollapsedPostId={this.toggleCollapsedPostId}
                onPostTitleClick={this.handlePostTitleClick}
            />
        );
    };

    render() {
        const mode = this.props.match.params.mode || EBlogViewMode.READ;
        const {user} = this.props;
        const isAdmin = user && user.role === ERole.ADMIN;
        let blogForm: JSX.Element;

        switch (mode) {
            case EBlogViewMode.CREATE:
            case EBlogViewMode.EDIT:
                blogForm = isAdmin ? this.renderPostEdit() : this.renderPostView();
                break;
            case EBlogViewMode.READ:
            default:
                blogForm = this.renderPostView();
        }

        return (
            <Grid container justify={'center'}>
                <Grid item xl={8} lg={10} md={11} sm={12} xs={12}>
                    {blogForm}
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = (state: IAppState) => ({
    ...state.blogState,
    ...state.loginState
});

const mapDispatchToProps = (dispatch: ThunkDispatch<IAppState, void, Action>) => {
    return {
        blogActions: new BlogActions(dispatch),
        filesManagerActions: new FilesManagerActions(dispatch)
    };
};

export const Blog = HOC<{}, IBlogStateProps, TDispatchProps, TStyleProps, TRouteProps>(BlogComponent, {
    mapStateToProps,
    mapDispatchToProps,
    styles: BlogStyles,
    isWithRouter: true
});

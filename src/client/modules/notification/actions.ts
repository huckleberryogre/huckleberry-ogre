import {IAppState, store} from '../../core/reduxStore';
import {ENotificationVariant} from './enums';
import {ThunkDispatch} from 'redux-thunk';
import {Action} from 'redux';
import {INotification} from "./Notification";

/**
 * Action types.
 */
export const NOTIFICATION_SHOW = 'NOTIFICATION_SHOW';
export const NOTIFICATION_HIDE = 'NOTIFICATION_HIDE';
export const NOTIFICATION_HIDE_ALL = 'NOTIFICATION_HIDE_ALL';

/**
 * Blog actions.
 */
export class NotificationActions {
    constructor(private dispatch: ThunkDispatch<IAppState, void, Action> = store.dispatch) {
    }

    /**
     * Show notification.
     *
     * @param {string} message Message.
     * @param {ENotificationVariant} [variant] Notification variant.
     * @param {string} [id] Identity.
     */
    show = ({message, variant, id}: Partial<INotification>) => this.dispatch({
        type: NOTIFICATION_SHOW,
        payload: {
            id,
            message,
            variant: variant || ENotificationVariant.INFO
        }
    });

    /**
     * Notification shortcut for success message variant.
     *
     * @param message Message to display.
     */
    success = (message: string) => this.show({message, variant: ENotificationVariant.SUCCESS});

    /**
     * Notification shortcut for warning message variant.
     *
     * @param message Message to display.
     */
    warning = (message: string) => this.show({message, variant: ENotificationVariant.WARNING});

    /**
     * Notification shortcut for error message variant.
     *
     * @param message Message to display.
     * @param error Any error to be logged.
     */
    error = (message: string, error?: any) => {
        this.show({message, variant: ENotificationVariant.ERROR});
        error && console.error(error)
    };

    /**
     * Notification shortcut for info message variant.
     *
     * @param message Message to display.
     */
    info = (message: string) => this.show({message, variant: ENotificationVariant.INFO});

    /**
     * Hide notification.
     */
    hide = (id: string) => this.dispatch({type: NOTIFICATION_HIDE, payload: {id}});

    /**
     * Hides all notifications.
     */
    hideAll = () =>  this.dispatch({type: NOTIFICATION_HIDE_ALL});
}

import {Reducer} from 'redux';
import {NOTIFICATION_SHOW, NOTIFICATION_HIDE, NOTIFICATION_HIDE_ALL} from './actions';
import {INotificationStateProps} from './Notification';
import {generateUUID} from "../../core/utils/utils";

/**
 * Initial blog state.
 */
export const initNotificationState: INotificationStateProps = {
    notifications: [],
};

/**
 * Blog reducer.
 */
export const notificationState: Reducer<INotificationStateProps> = (
    state: INotificationStateProps = initNotificationState,
    action
) => {
    const {id, message, variant} = action.payload || {};

    switch (action.type) {
        case NOTIFICATION_SHOW:
            return {
                notifications: id ? state.notifications.map(notification => notification.id !== id ? notification : ({
                    ...notification,
                    show: true,
                })) : [
                    ...state.notifications,
                    {
                        id: generateUUID(),
                        show: true,
                        message,
                        variant,
                    }
                ]
            };
        case NOTIFICATION_HIDE:
            return {
                notifications: state.notifications.map(notification => notification.id !== id ? notification : ({
                    ...notification,
                    show: false
                }))
            };
        case NOTIFICATION_HIDE_ALL:
            return {
                notifications: state.notifications.map(notification => ({
                    ...notification,
                    show: false
                }))
            };
        default:
            return state;
    }
};
import React from 'react';
import {Slide, SnackbarContent, WithStyles} from '@material-ui/core';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import WarningIcon from '@material-ui/icons/Warning';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import {NotificationActions} from './actions';
import {ENotificationVariant} from './enums';
import {NotificationStyles} from './styles';
import {HOC} from '../../core/utils/HOC';
import {IAppState} from '../../core/reduxStore';
import {ThunkDispatch} from 'redux-thunk';
import {Action} from 'redux';
import classNames from 'classnames';

/**
 * Single notification props.
 *
 * @prop {string} id Identity.
 * @prop {boolean} show Rendering flag.
 * @prop {ENotificationVariant} variant Notification variant. Info by default.
 * @prop {string} message Message.
 */
export interface INotification {
    id: string;
    show: boolean;
    variant: ENotificationVariant;
    message: string;
}

/**
 * Notification centre state.
 */
export interface INotificationStateProps {
    notifications: INotification[];
}

type TStyleProps = WithStyles<typeof NotificationStyles>;

/**
 * Notification component class.
 */
class NotificationComponent extends React.Component<INotificationStateProps & TStyleProps & TDispatchProps> {
    componentDidMount(): void {
        const {actions} = this.props;

        document.addEventListener('click', (event) => {
            if (event.target instanceof Element && !event.target.closest('[role="notifications"]')) {
                actions.hideAll();
            }
        })
    }

    /**
     * Message renderer.
     */
    renderMessage = (variant: ENotificationVariant, message: string) => {
        const {classes} = this.props;

        const variantIcon = {
            success: CheckCircleIcon,
            warning: WarningIcon,
            error: ErrorIcon,
            info: InfoIcon,
        };
        const Icon = variantIcon[variant];

        return (
            <span className={classes.message}>
                {Icon && <Icon className={classes.icon}/>}
                <span className={classes.messageText}>{message}</span>
            </span>
        );
    };

    renderNotifications = () => {
        const {notifications, classes, actions} = this.props;

        return notifications.map(notification => {
            const {variant, message, show, id} = notification;

            return (
                <Slide direction="down" in={show} mountOnEnter unmountOnExit>
                    <SnackbarContent
                        key={id}
                        className={classNames(classes[variant], classes.snackbarContent)}
                        message={this.renderMessage(variant, message)}
                        onClick={() => actions.hide(id)}
                    />
                </Slide>
            )
        });
    };

    render() {
        const {classes} = this.props;

        return (
            <div role='notifications' className={classes.notificationsPanel}>{this.renderNotifications()}</div>
        )
    }
}

const mapStateToProps = (state: IAppState) => state.notificationState;

const mapDispatchToProps = (dispatch: ThunkDispatch<IAppState, void, Action>) => {
    return {
        actions: new NotificationActions(dispatch)
    };
};

type TDispatchProps = { actions: NotificationActions };

export const Notification = HOC<{}, INotificationStateProps, TDispatchProps, TStyleProps, {}>(
    NotificationComponent,
    {
        mapStateToProps,
        mapDispatchToProps,
        styles: NotificationStyles
    }
);
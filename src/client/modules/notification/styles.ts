import {Theme, createStyles} from '@material-ui/core';
import {green, amber} from '@material-ui/core/colors';

export const NotificationStyles = (theme: Theme) => createStyles({
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.dark,
    },
    warning: {
        backgroundColor: amber[700],
    },
    snackbarContent: {
        marginBottom: theme.spacing(3),

        '&>div': {
            margin: '0 auto'
        }
    },
    icon: {
        fontSize: 20,
        opacity: 0.9,
        marginRight: theme.spacing(2),
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
    messageText: {
        marginTop: '1px',
        whiteSpace: 'pre-line'
    },
    notificationsPanel: {
        position: 'fixed',
        top: theme.spacing(2),
        left: '50%',
        maxHeight: `calc(100vh - ${theme.spacing(3)}px)`,
        zIndex: 10000,
        transform: 'translate3d(-50%, 0, 0)',
        overflowY: 'auto',
    }
});

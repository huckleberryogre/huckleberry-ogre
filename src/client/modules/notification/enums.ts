/**
 * Notification variant.
 */
export enum ENotificationVariant {
    SUCCESS = 'success',
    WARNING = 'warning',
    ERROR = 'error',
    INFO = 'info'
}
import {
    AppBar,
    Button,
    InputAdornment,
    Modal,
    Tab,
    Tabs,
    TextField,
    Typography,
    WithStyles,
    BottomNavigation,
    BottomNavigationAction,
    LinearProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from '@material-ui/core';
import {
    CloudUpload as CloudUploadIcon,
    Add as AddIcon,
    SelectAll as SelectAllIcon,
    Delete as DeleteIcon,
    Height as HeightIcon,
    Refresh as RefreshIcon,
} from '@material-ui/icons';
import classNames from 'classnames';
import React from 'react';
import {Action} from 'redux';
import {ThunkDispatch} from 'redux-thunk';
import {IAppState} from '../../core/reduxStore';
import {HOC} from '../../core/utils/HOC';
import {isLocationWithinContext} from '../../core/utils/utils';
import {BlogActions} from '../blog/actions';
import {FilesManagerActions} from './actions';
import {EDragEventType, EDragPosition, EModalTabIndex} from './enums';
import {FilesManagerStyles} from './styles';
import {isInsideElement} from './utils';
import moment from 'moment';
import {FilesGalleryTab} from "./FilesGalleryTab";
import {ILoginStateProps} from "../login/Login";
import {debounce, isEqual} from 'lodash';
import {ELocationContext, ERole} from "../../core/enums";
import {IFile} from "../../../models";

/**
 * File model used on client side.
 *
 * @prop {string} creationDate Date of creation for the file, if parsed from exif data.
 * @prop {string} creationDayFormatted Year, month and date formatted in readable way.
 */
export interface IClientFile extends IFile {
    creationMoment?: moment.Moment;
    creationDayFormatted: string;
}

/**
 * FilesManager state.
 *
 * @prop {number} uploadProgress Upload progress percent.
 * @prop {boolean} isUploadInProgress Flag - upload in progress.
 * @prop {boolean} isFilesManagerShow Becomes true on drag start.
 * @prop {boolean} isFilesManagerEnabled Prevent showing files manager.
 * @prop {EModalTabIndex} tabIndex Tab index.
 * @prop {IFile[]} files File list from db.
 * @prop {string[]} Selected files UUIDs.
 */
export interface IFilesManagerStateProps extends ILoginStateProps {
    uploadProgress: number;
    isUploadInProgress: boolean;
    isFilesManagerShow: boolean;
    isFilesManagerEnabled: boolean;
    tabIndex: EModalTabIndex;
    files: IClientFile[];
    selectedFiles: string[];
}

type TDispatchProps = { actions: FilesManagerActions, blogActions: BlogActions };

type TStyleProps = WithStyles<typeof FilesManagerStyles>;

/**
 * State.
 *
 * @prop dragPosition Drag object position relative to filesManager #dropZone window.
 * @prop filesPortion A portion of files to show (scroll client pagination).
 * @prop isDeleteDialogOpen Flag for delete dialog state.
 */
interface IState {
    dragPosition: EDragPosition;
    filesPortion: IClientFile[];
    isDeleteDialogOpen: boolean;
}


/**
 * Input url reference.
 */
export let inputUrlRef: HTMLInputElement;

/**
 * FilesManager component.
 */
class FilesManagerComponent extends React.Component<IFilesManagerStateProps & TStyleProps & TDispatchProps, IState> {

    state: IState = {
        dragPosition: EDragPosition.OUTSIDE,
        filesPortion: this.props.files.slice(0, 20),
        isDeleteDialogOpen: false,
    };

    /**
     * Refs.
     */
    filesManager: HTMLDivElement;
    filesInput: HTMLInputElement;
    galleryContainerRef = React.createRef<HTMLDivElement>();

    componentDidUpdate(prevProps: Readonly<IFilesManagerStateProps & TStyleProps & TDispatchProps>): void {
        if (!isEqual(prevProps.files, this.props.files)) {
            this.setState({filesPortion: this.props.files.slice(0, 20)})
        }
    }

    //todo: deal with eventListeners mess https://huckleberrydev.atlassian.net/browse/HBOG-32
    componentDidMount() {
        const {
            actions: {
                show,
                hide,
                enable,
                disable,
                handleTabChange,
                getAllFiles,
            },
            files,
            user
        } = this.props;

        const isAdmin = user?.role === ERole.ADMIN;
        !files.length && isAdmin && getAllFiles();

        // this prevent browser to load dropped files (which closes website). Discovered empirically, do not remove!
        [EDragEventType.OVER, EDragEventType.DROP].forEach(eventType => {
            document.addEventListener(eventType, event => {
                event.preventDefault();
            });
        });
        // showing file manager on drag enter
        document.addEventListener(EDragEventType.ENTER, () => {
            handleTabChange(null, EModalTabIndex.UPLOAD);
            show();
        });
        // hiding file manager on drag leave
        document.addEventListener(EDragEventType.LEAVE, (event) => {
            // I presume here that relatedTarget is null only when element leaves window. It seems to work.
            if (event.relatedTarget === null) {
                hide();
            }
        });
        // disable file manager on drag start, I dont need to upload inner/own content
        document.addEventListener(EDragEventType.START, () => {
            // no need to disable file manager if it is already active
            // don't you dare to remove this.props from here! otherwise isFilesManagerShow flag gonna always be initial!
            !this.props.isFilesManagerShow && disable();
        });
        // enable file manager on drag end, after it was disabled by EDragEventType.START handler
        document.addEventListener(EDragEventType.END, () => {
            enable();
        });
    }

    /**
     * Handles key press on url input field.
     *
     * @param {KeyboardEvent} event Keyboard press event.
     */
    handleUrlKeyPress = (event: React.KeyboardEvent) => {
        if (event.key === 'Enter') {
            this.handleUrlPaste(inputUrlRef.value);
        }
    };

    /**
     * Handles key press on whole modal and overlay.
     *
     * @param {KeyboardEvent} event Keyboard press event.
     */
    handleModalKeyPress = (event: React.KeyboardEvent) => {
        if (event.key === 'Escape') {
            this.props.actions.hide();
        }
    };

    /**
     * Handles drag enter event.
     *
     * @param {DragEvent} event Drag enter event.
     */
    handleDragEnter = (event: React.DragEvent) => {
        if (isInsideElement(event, '#dropZone')) {
            this.setState({dragPosition: EDragPosition.INSIDE});
        }
    };

    /**
     * Handles drag leave event.
     */
    handleDragLeave = () => {
        this.setState({dragPosition: EDragPosition.OUTSIDE});
    };

    /**
     * Save filesManager element ref.
     *
     * @param {HTMLDivElement} ref Element ref.
     */
    filesManagerRef = (ref: HTMLDivElement) => {
        this.filesManager = ref;
    };

    /**
     * Save files input element ref.
     *
     * @param {HTMLInputElement} ref Element ref.
     */
    filesInputRef = (ref: HTMLInputElement) => {
        this.filesInput = ref;
    };

    /**
     * Handles item drop event.
     *
     * @param {DragEvent} event Drop event.
     */
    handleDrop = (event: React.DragEvent) => {
        this.setState({dragPosition: EDragPosition.OUTSIDE});
        if (isInsideElement(event, '#dropZone')) {
            this.props.actions.uploadFiles(event.dataTransfer.files);
        }
    };

    /**
     * Handles files change event on files input element.
     *
     * @param {ChangeEvent} event Change event.
     */
    handleFilesInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.props.actions.uploadFiles(event.target.files);
    };

    /**
     * Helper function to paste provided url into message blog field.
     *
     * @param url Url to paste.
     */
    handleUrlPaste = (url: string) => {
        const {actions, blogActions} = this.props;

        blogActions.handleUrlPaste(url);
        actions.hide(false);
    };


    /**
     * Adds selected files into blog form.
     */
    addSelectedFiles = async () => {
        const {selectedFiles, blogActions, actions} = this.props;

        for (let uuidIndex in selectedFiles) {
            await blogActions.handleUrlPaste(`${AppConfig.SERVER_REST_ADDRESS}/file/${selectedFiles[uuidIndex]}`);
        }

        actions.setSelectedFiles([]);
        actions.hide(false);
    };

    /**
     * Toggle all files selection.
     */
    selectAllFiles = () => {
        const {selectedFiles, actions} = this.props;
        const {filesPortion} = this.state;

        if (selectedFiles.length === filesPortion.length) {
            actions.setSelectedFiles([]);
        } else {
            actions.setSelectedFiles(filesPortion.map(file => file.uuid));
        }
    };

    /**
     * Scroll client pagination.
     */
    handleGalleryScroll = debounce(() => {
        if (this.state.filesPortion.length === this.props.files.length) {
            return;
        }

        const scrollTop = this.galleryContainerRef.current.scrollTop;
        const scrollHeight = this.galleryContainerRef.current.scrollHeight;
        const clientHeight = this.galleryContainerRef.current.clientHeight;

        if (scrollTop / (scrollHeight - clientHeight) > 0.95) {
            this.setState(prevState => ({
                filesPortion: this.props.files.slice(0, prevState.filesPortion.length + 50)
            }))
        }
    }, 100, {leading: false, trailing: true});

    /**
     * Sets files portion to all files.
     */
    expandAll = () => {
        this.setState({
            filesPortion: this.props.files
        });
    };

    /**
     * Closes delete dialog.
     */
    handleDeleteDialogClose = () => {
        this.setState({isDeleteDialogOpen: false});
    };

    /**
     * Opens delete dialog.
     */
    handleDeleteDialogOpen = () => {
        this.setState({isDeleteDialogOpen: true});
    };

    /**
     * Handles selected files deletion.
     */
    handleSelectedFilesDelete = () => {
        const {actions, selectedFiles} = this.props;

        actions.deleteFiles(selectedFiles);
        this.handleDeleteDialogClose();
    };

    render() {
        const {
            isFilesManagerShow,
            isFilesManagerEnabled,
            actions,
            classes,
            tabIndex,
            selectedFiles,
            isUploadInProgress,
            uploadProgress,
            files
        } = this.props;
        const {dragPosition, filesPortion, isDeleteDialogOpen} = this.state;
        const selectedFilesSet = new Set(selectedFiles);
        const isBlogEditContext = isLocationWithinContext([ELocationContext.BLOG_CREATE, ELocationContext.BLOG_EDIT]);

        return (
            isFilesManagerEnabled ?
                (
                    <Modal
                        open
                        aria-labelledby="simple-modal-title"
                        aria-describedby="simple-modal-description"
                        onClose={() => actions.hide()}
                        onDrop={this.handleDrop}
                        onKeyPress={this.handleModalKeyPress}
                        className={classNames(classes.filesManager, {[classes.filesManagerShown]: isFilesManagerShow})}
                        >
                        <div className={classes.paper}>
                            <AppBar position="absolute" color="default" className={classes.modalAppBar}>
                                {isUploadInProgress && (
                                    <LinearProgress color="secondary" variant="determinate" value={uploadProgress}/>
                                )}
                                <Tabs
                                    value={tabIndex}
                                    onChange={actions.handleTabChange}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    variant="fullWidth"
                                >
                                    <Tab label="upload"/>
                                    <Tab label={`gallery (${files.length})`} disabled={isUploadInProgress}/>
                                </Tabs>
                            </AppBar>
                            <div
                                onScroll={this.handleGalleryScroll}
                                ref={this.galleryContainerRef}
                                className={classNames(
                                    classes.modalContent,
                                    {'gallery': tabIndex === EModalTabIndex.GALLERY})
                                }
                            >
                                {tabIndex === EModalTabIndex.UPLOAD && (
                                    <div className={classes.uploadTab}>
                                        {isBlogEditContext && (
                                            <TextField
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment className={classes.adornment} position="start">
                                                            {'Enter URL:'}
                                                        </InputAdornment>
                                                    ),
                                                }}
                                                fullWidth
                                                onKeyPress={this.handleUrlKeyPress}
                                                className='margin-bottom-2'
                                                name='inputURL'
                                                inputRef={(ref) => {
                                                    inputUrlRef = ref;
                                                }}
                                            />
                                        )}
                                        <div
                                            id="dropZone"
                                            className={classNames(classes.dropZone, {active: dragPosition === EDragPosition.INSIDE})}
                                            onDragEnter={this.handleDragEnter}
                                            onDragLeave={this.handleDragLeave}
                                            ref={this.filesManagerRef}
                                        >
                                            <Typography
                                                className={classes.filesManagerDescription}
                                                color='primary'
                                                variant='subtitle1'
                                            >
                                                {'Drop files here or'}
                                                <Button
                                                    variant="contained"
                                                    size='small'
                                                    color="default"
                                                    className='margin-left-1'
                                                    onClick={() => this.filesInput.click()}
                                                >
                                                    {'upload'}
                                                    <CloudUploadIcon className='margin-left-1'/>
                                                </Button>
                                                <input
                                                    className={classes.filesInput}
                                                    id="filesInput"
                                                    multiple
                                                    type="file"
                                                    ref={this.filesInputRef}
                                                    onChange={this.handleFilesInputChange}
                                                />
                                            </Typography>
                                        </div>
                                    </div>
                                )}
                                {tabIndex === EModalTabIndex.GALLERY && (
                                    <FilesGalleryTab
                                        files={filesPortion}
                                        classes={classes}
                                        isBlogEditContext={isBlogEditContext}
                                        actions={actions}
                                        selectedFilesSet={selectedFilesSet}
                                        onUrlPaste={this.handleUrlPaste}
                                    />
                                )}
                            </div>
                            {tabIndex === EModalTabIndex.GALLERY && (
                                <BottomNavigation
                                    showLabels={true}
                                    className={
                                        classNames(classes.bottomNavigation)
                                    }
                                >
                                    <BottomNavigationAction
                                        className={classes.bottomNavigationAction}
                                        label={`Add selected (${selectedFiles.length})`}
                                        icon={<AddIcon/>}
                                        onClick={this.addSelectedFiles}
                                    />
                                    <BottomNavigationAction
                                        className={classes.bottomNavigationAction}
                                        label={`${selectedFiles.length !== filesPortion.length ? 'Select' : 'Deselect'} all visible (${filesPortion.length})`}
                                        icon={<SelectAllIcon/>}
                                        onClick={this.selectAllFiles}
                                    />
                                    <BottomNavigationAction
                                        className={classNames(classes.bottomNavigationAction)}
                                        label={`Delete selected (${selectedFiles.length})`}
                                        icon={<DeleteIcon/>}
                                        onClick={this.handleDeleteDialogOpen}
                                    />
                                    <BottomNavigationAction
                                        className={classNames(classes.bottomNavigationAction)}
                                        label={`Expand all`}
                                        icon={<HeightIcon/>}
                                        onClick={this.expandAll}
                                    />
                                    <BottomNavigationAction
                                        className={classNames(classes.bottomNavigationAction)}
                                        label={`Refresh`}
                                        icon={<RefreshIcon/>}
                                        onClick={actions.refresh}
                                    />
                                </BottomNavigation>
                            )}
                            <Dialog
                                open={isDeleteDialogOpen}
                                onClose={this.handleDeleteDialogClose}
                            >
                                <DialogTitle>{`Delete ${selectedFiles.length} file(s)?`}</DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        Following files will be deleted:
                                    </DialogContentText>
                                    <FilesGalleryTab
                                        files={files.filter(file => selectedFiles.includes(file.uuid))}
                                        classes={classes}
                                        isBlogEditContext={isBlogEditContext}
                                        actions={actions}
                                        selectedFilesSet={selectedFilesSet}
                                        onUrlPaste={this.handleUrlPaste}
                                    />
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.handleDeleteDialogClose}>
                                        Cancel
                                    </Button>
                                    <Button onClick={this.handleSelectedFilesDelete} className={classes.deleteFilesButton} autoFocus>
                                        Delete
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </div>
                    </Modal>
                ) : null
        );
    }
}

const mapStateToProps = ({filesManagerState, loginState}: IAppState): IFilesManagerStateProps => ({
    ...filesManagerState,
    ...loginState
});

const mapDispatchToProps = (dispatch: ThunkDispatch<IAppState, void, Action>) => {
    return {
        actions: new FilesManagerActions(dispatch),
        blogActions: new BlogActions(dispatch)
    };
};

export const FilesManager = HOC<{}, IFilesManagerStateProps, TDispatchProps, TStyleProps, {}>(
    FilesManagerComponent,
    {
        mapStateToProps,
        mapDispatchToProps,
        styles: FilesManagerStyles
    }
);

/**
 * Drag item position relative to #dropZone.
 */
export enum EDragPosition {
    INSIDE,
    OUTSIDE
}

/**
 * Tab index of modal window.
 */
export enum EModalTabIndex {
    UPLOAD,
    GALLERY
}

/**
 * All drag event types.
 * mdn: https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API#Drag_Events
 */
export enum EDragEventType {
    ENTER = 'dragenter',
    START = 'dragstart',
    END = 'dragend',
    LEAVE = 'dragleave',
    OVER = 'dragover',
    DRAG = 'drag',
    DROP = 'drop',
    EXIT = 'dragexit'
}
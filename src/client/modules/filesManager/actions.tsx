import {forEach} from 'lodash';
import React from 'react';
import {Action} from 'redux';
import {ThunkDispatch} from 'redux-thunk';
import {IAppState} from '../../core/reduxStore';
import {get, post, remove} from '../../core/utils/ServiceUtils';
import {BlogActions} from '../blog/actions';
import {messageFieldRef} from '../blog/Blog';
import {NotificationActions} from '../notification/actions';
import {EModalTabIndex} from './enums';
import {IClientFile, inputUrlRef} from './FilesManager';
import {ExifData} from 'exif';
import moment from 'moment';
import {toHumanReadableSizeFormat} from "../../core/utils/utils";
import {round} from 'lodash';
import {IFile} from "../../../models";

/**
 * Action types.
 */
export const DROP_ZONE_SHOW = 'DROP_ZONE_SHOW';
export const DROP_ZONE_HIDE = 'DROP_ZONE_HIDE';
export const DROP_ZONE_ENABLE = 'DROP_ZONE_ENABLE';
export const DROP_ZONE_DISABLE = 'DROP_ZONE_DISABLE';
export const DROP_ZONE_GET_ALL_FILES = 'DROP_ZONE_GET_ALL_FILES';
export const DROP_ZONE_RESET_FILES_LIST = 'DROP_ZONE_RESET_FILES_LIST';
export const DROP_ZONE_ADD_FILE = 'DROP_ZONE_ADD_FILE';
export const DROP_ZONE_DELETE_FILE = 'DROP_ZONE_DELETE_FILE';
export const DROP_ZONE_MODAL_TAB_CHANGE = 'DROP_ZONE_MODAL_TAB_CHANGE';
export const DROP_ZONE_SET_SELECTED_FILES = 'DROP_ZONE_SET_SELECTED_FILES';
export const DROP_ZONE_SET_IS_UPLOAD_IN_PROGRESS = 'DROP_ZONE_SET_IS_UPLOAD_IN_PROGRESS';
export const DROP_ZONE_SET_UPLOAD_PROGRESS = 'DROP_ZONE_SET_UPLOAD_PROGRESS';

/**
 * Blog actions.
 */
export class FilesManagerActions {
    constructor(private dispatch: ThunkDispatch<IAppState, void, Action>) {
    }

    /**
     * Show FilesManager.
     */
    show = () => this.dispatch((dispatch, getState) => {
            const {isFilesManagerShow, isFilesManagerEnabled, files} = getState().filesManagerState;

            if (!isFilesManagerShow && isFilesManagerEnabled) {
                dispatch({type: DROP_ZONE_SHOW});
                !files.length && this.getAllFiles();
            }
        }
    );

    /**
     * Hide FilesManager.
     *
     * @param focusMessageField Flag to focus message field on hide, true by default.
     */
    hide = (focusMessageField: boolean = true) => {
        const blogActions = new BlogActions(this.dispatch);
        this.dispatch({type: DROP_ZONE_HIDE});

        focusMessageField && messageFieldRef && blogActions.focusMessageField();
    };


    /**
     * Enable FilesManager.
     */
    enable = () => this.dispatch((dispatch, getState) => (
            !getState().filesManagerState.isFilesManagerEnabled && dispatch({type: DROP_ZONE_ENABLE})
        )
    );

    /**
     * Disable FilesManager.
     */
    disable = () => this.dispatch((dispatch, getState) => {
            if (getState().filesManagerState.isFilesManagerEnabled) {
                dispatch({type: DROP_ZONE_DISABLE});
            }
            this.hide(false);
        }
    );

    /**
     * Upload files handler.
     *
     * @param {FileList} files File list to upload.
     */
    uploadFiles = (files: FileList) => this.dispatch(() => {
        const notificationActions = new NotificationActions(this.dispatch);

        if (!files.length) {
            notificationActions.warning('No files provided');
            return;
        }

        this.setIsUploadInProgress(true);

        const filesSizeMap = Array.from(files).map(file => file.size);
        const totalFilesSize = filesSizeMap.reduce((total: number, fileSize: number) => total + fileSize, 0);
        let finishedFilesCount = 0;
        let progress = 0;
        let responseArray = [];

        notificationActions.success(`${finishedFilesCount} file(s) uploaded. [0/${toHumanReadableSizeFormat(totalFilesSize)}]`);

        const postFilesRecursive = (file: File) => {
            const formData = new FormData();
            formData.append('file', file, file.name);

            return post<IClientFile>('file/save', formData, {noWrap: true}).then(file => {
                finishedFilesCount++;
                responseArray.push(file);

                progress = filesSizeMap.slice(0, finishedFilesCount).reduce((total: number, fileSize: number) => total + fileSize, 0);

                const stats = `[${toHumanReadableSizeFormat(progress)}/${toHumanReadableSizeFormat(totalFilesSize)}]`;

                this.setProgress(round(progress / totalFilesSize * 100));

                if (finishedFilesCount < files.length) {
                    notificationActions.success(`${finishedFilesCount} file(s) uploaded. ${stats}`);

                    return postFilesRecursive(files[finishedFilesCount]);
                } else {
                    notificationActions.success(`${finishedFilesCount} file(s) uploaded. All done. ${stats}`);

                    return responseArray;
                }
            })
        };

        const allCompleteCallback = (filesArray?: IFile[]) => {
            this.getAllFiles();
            this.handleTabChange(null, EModalTabIndex.GALLERY);
            this.setIsUploadInProgress(false);

            return filesArray;
        };

        return postFilesRecursive(files[0])
            .then(allCompleteCallback)
            .catch((error) => {
                notificationActions.error('File(s) upload error', error);
                return allCompleteCallback(responseArray);
            });
    });

    /**
     * Sets uploading progress.
     *
     * @param progress Progress (percents).
     */
    setProgress = (progress: number) => {
        this.dispatch({type: DROP_ZONE_SET_UPLOAD_PROGRESS, payload: progress})
    };

    /**
     * Sets isUploadInProgress flag.
     *
     * @param isUploadInProgress Flag value.
     */
    setIsUploadInProgress = (isUploadInProgress: boolean) => this.dispatch({
        type: DROP_ZONE_SET_IS_UPLOAD_IN_PROGRESS,
        payload: isUploadInProgress
    });

    /**
     * Delete files handler.
     *
     * @param {string[]} uuids UUIDs.
     */
    deleteFiles = (uuids: string[]) => this.dispatch((dispatch, getState) => {
        const filesCount = uuids.length;
        const notificationActions = new NotificationActions(this.dispatch);
        let selectedFilesSet = new Set(getState().filesManagerState.selectedFiles);
        let promises = [];

        forEach(uuids, uuid => {
            promises.push(
                remove(`file/${uuid}`).then(() => {
                    this.deleteFile(uuid);

                    if (selectedFilesSet.has(uuid)) {
                        selectedFilesSet.delete(uuid);
                        dispatch({type: DROP_ZONE_SET_SELECTED_FILES, payload: Array.from(selectedFilesSet)});
                    }
                })
            );
        });
        Promise.all(promises)
            .then(() => {
                this.getAllFiles();
                notificationActions.success(`${filesCount} file(s) deleted`);
            })
            .catch((error) => {
                this.getAllFiles();
                notificationActions.error(error.message || 'deleting file(s) unexpected error');
            });
    });

    /**
     * Delete file from state.
     *
     * @param {string} uuid File uuid.
     */
    deleteFile = (uuid: string) => this.dispatch({
        type: DROP_ZONE_DELETE_FILE,
        payload: uuid
    });

    /**
     * Sorts client files by date decrease.
     *
     * @param files Files.
     */
    sortFilesByDateDecrease = (files: IClientFile[]): IClientFile[] => files.sort(
        (a: any, b: any) => b.creationMoment - a.creationMoment
    );

    /**
     * Modal tab change handler.
     *
     * @param {ChangeEvent} __ Change event.
     * @param {EModalTabIndex} tabIndex Tab index.
     * @param forceUpdate Force update of all files list when switching to gallery tab, by default false.
     */
    handleTabChange = (__: React.ChangeEvent, tabIndex: EModalTabIndex, forceUpdate = false) => this.dispatch((dispatch, getState) => {
        switch (tabIndex) {
            case EModalTabIndex.GALLERY:
                (!getState().filesManagerState.files.length || forceUpdate) && this.getAllFiles();
                break;
            case EModalTabIndex.UPLOAD:
            default:
                this.focusInputUrl()
        }

        dispatch({
            type: DROP_ZONE_MODAL_TAB_CHANGE,
            payload: tabIndex
        });
    });

    /**
     * Sets focus on input url field.
     */
    focusInputUrl = () => {
        setTimeout(() => {
            inputUrlRef && inputUrlRef.focus();
        }, 0)
    };

    /**
     * Converts server file model to client.
     *
     * @param files Files with server model.
     */
    getClientFiles = (files: IFile[]): IClientFile[] => files.map((file: IClientFile) => {
        try {
            const exif: ExifData = JSON.parse(file.meta).exif;
            const originateDate = exif?.exif?.CreateDate || exif?.exif?.DateTimeOriginal;

            if (moment(originateDate, 'YYYY:MM:DD HH:mm:ss').isValid()) {
                // try get date from metadata
                file.creationMoment = moment(originateDate, 'YYYY:MM:DD HH:mm:ss');
                file.creationDayFormatted = file.creationMoment.format('D MMMM YYYY');
            } else if (moment(file.name, 'YYYYMMDD_HHmmss').isValid()) {
                // else try get date from filename
                file.creationMoment = moment(file.name, 'YYYYMMDD_HHmmss');
                file.creationDayFormatted = file.creationMoment.format('D MMMM YYYY');
            } else {
                // else give up
                file.creationMoment = null;
                file.creationDayFormatted = 'Creation date unknown';
            }

            return file;
        } catch (e) {
            console.log(e);
        }
    });

    /**
     * Files list loading handler.
     */
    getAllFiles = () => {
        get<IClientFile[]>('files/all')
            .then(files => {
                this.dispatch({
                    type: DROP_ZONE_GET_ALL_FILES,
                    payload: this.sortFilesByDateDecrease(this.getClientFiles(files))
                });
            })
            .catch((error) => {
                const notificationActions = new NotificationActions(this.dispatch);
                notificationActions.error('Failed loading files list', error);
            });
    };

    /**
     * Resets files list and starts loading all files again.
     */
    refresh = () => {
        this.dispatch({type: DROP_ZONE_RESET_FILES_LIST});
        this.getAllFiles();
    };

    /**
     * Toggle file selection checkbox.
     * @param uuid Identifier of selected file.
     */
    toggleFileSelect = (uuid: string) => this.dispatch((dispatch, getState) => {
            let selectedFilesSet = new Set(getState().filesManagerState.selectedFiles);

            if (selectedFilesSet.has(uuid)) {
                selectedFilesSet.delete(uuid);
            } else {
                selectedFilesSet.add(uuid);
            }

            dispatch({type: DROP_ZONE_SET_SELECTED_FILES, payload: Array.from(selectedFilesSet)})
        }
    );

    /**
     * Set selected files in gallery tab.
     * @param UUIDs Identifiers of selected files.
     */
    setSelectedFiles = (UUIDs: string[]) => this.dispatch({type: DROP_ZONE_SET_SELECTED_FILES, payload: UUIDs});
}

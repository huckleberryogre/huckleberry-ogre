import {Theme, createStyles} from '@material-ui/core';

export const FilesManagerStyles = (theme: Theme) => createStyles({
    filesManager: {
        visibility: 'hidden',
        pointerEvents: 'none'
    },
    filesManagerShown: {
        visibility: 'visible',
        pointerEvents: 'auto'
    },
    getAllFilesButton: {
        transform: 'translate3d(-50%, -50%, 0)',
        top: '50%',
        left: '50%',
    },
    adornment: {
        whiteSpace: 'nowrap'
    },
    paper: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate3d(-50%, -50%, 0)',
        width: '70%',
        minHeight: '80vh',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        zIndex: 2000,
        borderRadius: '5px',
        overflow: 'hidden',
        [theme.breakpoints.down('sm')]: {
            width: '90%'
        },
        [theme.breakpoints.up('lg')]: {
            width: '60%'
        }
    },
    modalContent: {
        position: 'absolute',
        top: '48px',
        right: '0',
        bottom: '56px',
        left: '0',
        padding: '18px 16px',
        boxSizing: 'border-box',

        '&.gallery': {
            overflow: 'hidden',
            overflowY: 'scroll',
        }
    },
    card: {
        position: 'relative',
        width: '200px',
        display: 'inline-block',
        margin: '8px 8px',
        [theme.breakpoints.down('xs')]: {
            width: '100%'
        }
    },
    cardsBar: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: `${theme.spacing(1)}px 0 ${theme.spacing(5)}px`,
    },
    cardContent: {
        padding: `${theme.spacing(2)}px ${theme.spacing(2)}px 0`,
        boxSizing: 'border-box',
        maxHeight: theme.spacing(12),
        textOverflow: 'ellipsis',
    },
    cardCheckbox: {
        position: 'absolute',
        right: 0,
        bottom: 70,
        color: theme.palette.secondary.main
    },
    cardActions: {
        padding: '0 12px 8px',
        justifyContent: 'space-between'
    },
    cardMedia: {
        paddingTop: '52%',
        backgroundSize: 'contain',
        backgroundColor: theme.palette.grey[50],
        borderBottom: `1px solid ${theme.palette.grey[400]}`
    },
    cardMediaLoading: {
        display: 'flex',
        height: '145px',
        backgroundColor: theme.palette.grey[50],
        borderBottom: `1px solid ${theme.palette.grey[400]}`,

        '&>div': {
            margin: 'auto'
        }
    },
    bottomNavigation: {
        position: 'absolute',
        width: '100%',
        bottom: 0,
        justifyContent: 'inherit',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        transition: 'bottom .3s ease',
        boxShadow: '0px 2px 10px 4px rgba(0,0,0,0.2), 0px 4px 10px 6px rgba(0,0,0,0.14), 0px 1px 10px 8px rgba(0,0,0,0.12)',
        height: 'auto',
        flexWrap: 'wrap',
        maxHeight: 120,
        overflowY: 'auto',
    },
    bottomNavigationHidden: {
        bottom: -56,
    },
    bottomNavigationAction: {
        '&:hover': {
            filter: 'drop-shadow(3px 3px 2px rgb(1, 1, 1, 0.3))',
            fontWeight: 600,
            backgroundColor: 'initial',
            textDecoration: 'initial',
        }
    },
    urlInput: {
        margin: theme.spacing(2),
    },
    modalAppBar: {
        borderTopRightRadius: '5px',
        borderTopLeftRadius: '5px',
    },
    dropZone: {
        display: 'flex',
        flexGrow: 1,
        borderRadius: 5,
        border: `2px dashed ${theme.palette.primary.main}`,

        '&.active': {
            borderColor: theme.palette.secondary.main,
            borderStyle: 'solid',

            '& *': {
                pointerEvents: 'none'
            }
        }
    },
    uploadTab: {
        display: 'flex',
        minHeight: '100%',
        flexDirection: 'column',
    },
    filesManagerDescription: {
        margin: 'auto'
    },
    filesInput: {
        display: 'none'
    },
    creationDayFormatted: {
        borderBottom: `1px solid ${theme.palette.common.black}`,
    },
    deleteFilesButton: {
        color: theme.palette.error.main
    }
});

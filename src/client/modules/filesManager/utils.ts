import React from 'react';
import {EDragEventType} from './enums';

const enableLogging = false;
const allDragEventTypes = [
    EDragEventType.ENTER,
    EDragEventType.START,
    EDragEventType.END,
    EDragEventType.LEAVE,
    EDragEventType.OVER,
    EDragEventType.DRAG,
    EDragEventType.DROP,
    EDragEventType.EXIT
];
// logging all events. To start, switch enableLogging variable to true.
enableLogging && allDragEventTypes.forEach(eventType => {
    document.addEventListener(eventType, event => {
        console.log(`${eventType}: `, event);
    });
});

/**
 * True if event target is inside element of given selector.
 *
 * @param {MouseEvent} event Any mouse event.
 * @param {string} selector Element selector.
 */
export const isInsideElement = (event: React.MouseEvent<any>, selector: string): boolean => (
    !!(event.target instanceof Element && event.target.closest(selector))
);

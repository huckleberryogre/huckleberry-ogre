import React, {useState} from 'react';
import {IClientFile} from "./FilesManager";
import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardMedia,
    Checkbox,
    Typography
} from "@material-ui/core";
import {PreviewModal} from "../../components/PreviewModal";
import {ClassNameMap} from "@material-ui/core/styles/withStyles";
import {FilesManagerActions} from "./actions";
import RefreshIcon from "@material-ui/icons/Refresh";

interface IProps {
    files: IClientFile[];
    classes: ClassNameMap;
    isBlogEditContext: boolean;
    actions: FilesManagerActions;
    selectedFilesSet: Set<string>;
    onUrlPaste: (url: string) => void;
}

export const FilesGalleryTab = (
    {
        files,
        classes,
        selectedFilesSet,
        actions,
        isBlogEditContext,
        onUrlPaste
    }: IProps
) => {
    /** previewOpenFileId File uuid to show in preview modal. */
    const [previewOpenFileId, setPreviewOpenFileId] = useState(null);

    return (
        <>
            {files.reduce((arrayOfElements: JSX.Element[], file: IClientFile, index: number): JSX.Element[] => {
                    const isNewDate = (index === 0 || file.creationDayFormatted !== files[index - 1].creationDayFormatted);
                    const newCardElement = (
                        <Card className={classes.card} key={file.uuid}>
                            <PreviewModal
                                src={`${AppConfig.SERVER_REST_ADDRESS}/file/${file.uuid}`}
                                onClose={() => setPreviewOpenFileId(null)}
                                isOpen={previewOpenFileId === file.uuid}
                            />
                            <CardMedia
                                className={classes.cardMedia}
                                image={`${AppConfig.SERVER_REST_ADDRESS}/file/thumb/${file.uuid}`}
                                onClick={() => setPreviewOpenFileId(file.uuid)}
                            />
                            <Checkbox
                                checked={selectedFilesSet.has(file.uuid)}
                                onChange={() => actions.toggleFileSelect(file.uuid)}
                                className={classes.cardCheckbox}
                            />
                            <CardContent className={classes.cardContent}>
                                <Typography
                                    component="p"
                                    className="text-ellipsis"
                                    title={file.name}
                                >
                                    {file.name}
                                </Typography>
                            </CardContent>
                            <CardActions className={classes.cardActions}>
                                <Button
                                    size="small"
                                    color="primary"
                                    onClick={() => actions.deleteFiles([file.uuid])}
                                >
                                    Delete
                                </Button>
                                {isBlogEditContext && (
                                    <Button
                                        size="small"
                                        color="primary"
                                        onClick={() => onUrlPaste(`${AppConfig.SERVER_REST_ADDRESS}/file/${file.uuid}`)}
                                    >
                                        Add
                                    </Button>
                                )}
                            </CardActions>
                        </Card>
                    );

                    if (isNewDate) {
                        arrayOfElements.push(
                            (
                                <Typography
                                    component="p"
                                    title={file.creationDayFormatted}
                                    className={classes.creationDayFormatted}
                                    key={file.creationDayFormatted + '_title'}
                                >
                                    {file.creationDayFormatted}
                                </Typography>
                            ),
                            (
                                <div className={classes.cardsBar} key={file.creationDayFormatted + '_cards'}>
                                    {newCardElement}
                                </div>
                            )
                        );
                    } else {
                        const lastElement = arrayOfElements[arrayOfElements.length - 1];
                        arrayOfElements[arrayOfElements.length - 1] = (
                            <lastElement.type {...lastElement.props} key={file.creationDayFormatted + '_cards'}>
                                {lastElement.props.children}
                                {newCardElement}
                            </lastElement.type>
                        )
                    }

                    return arrayOfElements;
                },
                []
            )}
            {!files.length && (
                <Button
                    variant="contained"
                    size='small'
                    color="primary"
                    onClick={actions.getAllFiles}
                    className={classes.getAllFilesButton}
                >
                    {'refresh'}
                    <RefreshIcon className='margin-left-1'/>
                </Button>
            )}
        </>
    )
};

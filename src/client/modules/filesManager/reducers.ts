import {Reducer} from 'redux';
import {
    DROP_ZONE_SHOW,
    DROP_ZONE_HIDE,
    DROP_ZONE_ENABLE,
    DROP_ZONE_DISABLE,
    DROP_ZONE_MODAL_TAB_CHANGE,
    DROP_ZONE_GET_ALL_FILES,
    DROP_ZONE_SET_SELECTED_FILES,
    DROP_ZONE_ADD_FILE,
    DROP_ZONE_DELETE_FILE,
    DROP_ZONE_SET_IS_UPLOAD_IN_PROGRESS,
    DROP_ZONE_SET_UPLOAD_PROGRESS, DROP_ZONE_RESET_FILES_LIST,
} from './actions';
import {EModalTabIndex} from './enums';
import {IFilesManagerStateProps} from './FilesManager';
import {initLoginState} from "../login/reducers";

/**
 * Initial FilesManager state.
 */
export const initFilesManagerState: IFilesManagerStateProps = {
    uploadProgress: 0,
    isUploadInProgress: false,
    isFilesManagerShow: false,
    isFilesManagerEnabled: true,
    tabIndex: EModalTabIndex.UPLOAD,
    files: [],
    selectedFiles: [],
    ...initLoginState
};

/**
 * Blog reducer.
 */
export const filesManagerState: Reducer<IFilesManagerStateProps> = (state: IFilesManagerStateProps = initFilesManagerState, action) => {
    switch (action.type) {
        case DROP_ZONE_SHOW:
            return {
                ...state,
                isFilesManagerShow: true
            };
        case DROP_ZONE_HIDE:
            return {
                ...state,
                isFilesManagerShow: false
            };
        case DROP_ZONE_ENABLE:
            return {
                ...state,
                isFilesManagerEnabled: true
            };
        case DROP_ZONE_DISABLE:
            return {
                ...state,
                isFilesManagerEnabled: false
            };
        case DROP_ZONE_MODAL_TAB_CHANGE:
            return {
                ...state,
                tabIndex: action.payload
            };
        case DROP_ZONE_GET_ALL_FILES:
        case DROP_ZONE_ADD_FILE:
            return {
                ...state,
                files: action.payload
            };
        case DROP_ZONE_RESET_FILES_LIST:
            return {
                ...state,
                files: []
            };
        case DROP_ZONE_DELETE_FILE:
            return {
                ...state,
                files: state.files.filter(file => file.uuid !== action.payload)
            };
        case DROP_ZONE_SET_SELECTED_FILES:
            return {
                ...state,
                selectedFiles: action.payload
            };
        case DROP_ZONE_SET_IS_UPLOAD_IN_PROGRESS:
            return {
                ...state,
                isUploadInProgress: action.payload
            };
        case DROP_ZONE_SET_UPLOAD_PROGRESS:
            return {
                ...state,
                uploadProgress: action.payload
            };
        default:
            return state;
    }
};
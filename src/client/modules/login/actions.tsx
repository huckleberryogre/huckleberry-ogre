import {Action} from 'redux';
import {IAppState} from '../../core/reduxStore';
import {ThunkDispatch} from 'redux-thunk';
import {get} from '../../core/utils/ServiceUtils';
import {IUser} from "../../../models";

/**
 * Action types.
 */
export const LOGIN_SET_USER = 'LOGIN_SET_USER';
export const LOGIN_EXIT = 'LOGIN_EXIT';

/**
 * Login actions.
 */
export class LoginActions {
    constructor(private dispatch: ThunkDispatch<IAppState, void, Action>) {
    }

    /**
     * Starts handshaking with Google.
     */
    startLoginWithGoogle = () => {
        window.location.href = `${AppConfig.SERVER_REST_ADDRESS}/login/google?redirectTo=${window.location.pathname}`;
    };

    /**
     * Sets user settings received from server.
     *
     * @param {IUser} user User settings.
     */
    setUser = (user: IUser) => {
        this.dispatch({
            type: LOGIN_SET_USER,
            payload: user
        });
    };

    sessionLogin = () => this.dispatch(
        () => {
            get<IUser>('login/session', null, {suppressErrorMessage: true}).then(user => {
                if (user.response.status === 200 && user.role) {
                    this.setUser(user);
                } else {
                    this.logOut();
                }
            });
        }
    );

    /**
     * Handles user logout.
     */
    logOut = () => {
        this.dispatch({type: LOGIN_EXIT});
        get('logout', null, {suppressErrorMessage: true});
    };

}

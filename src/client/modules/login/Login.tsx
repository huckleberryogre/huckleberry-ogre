import React from 'react';
import {Avatar, IconButton, WithStyles, Tooltip} from '@material-ui/core';
import {LoginStyles} from './styles';
import {HOC} from '../../core/utils/HOC';
import {IAppState} from '../../core/reduxStore';
import {LoginActions} from './actions';
import {ThunkDispatch} from 'redux-thunk';
import {Action} from 'redux';
import {
    AccountCircle as AccountCircleIcon,
    PowerSettingsNew as PowerSettingsNewIcon,
} from '@material-ui/icons';
import {NotificationActions} from "../notification/actions";
import {GoogleIcon} from "../../assets/icons/GoogleIcon";
import classNames from 'classnames';
import {IUser} from "../../../models";

/**
 * Login state.
 *
 * @prop {IUser} user Current user.
 */
export interface ILoginStateProps {
    user: IUser;
}

type TDispatchProps = {
    actions: LoginActions,
    notificationActions: NotificationActions
};
type TStyleProps = WithStyles<typeof LoginStyles>;

/**
 * Login component.
 */
class LoginComponent extends React.Component<ILoginStateProps & TStyleProps & TDispatchProps> {

    componentDidMount() {
        this.props.actions.sessionLogin();
    }

    render() {
        const {actions, user, classes} = this.props;

        return (
            <span className={classes.loginContainer}>
                {user ? (
                    <div>
                        <span className='onHoverHide display-none-sm-down'>{user.name}</span>
                        <span className='onHoverShow display-none-sm-down'>logout</span>
                        <IconButton className={classNames('onHoverHide', classes.iconButton)}>
                            <Avatar src={user.avatarURL}/>
                        </IconButton>
                        <IconButton className={classNames('onHoverShow', 'logoutIcon', classes.iconButton)} onClick={actions.logOut}>
                            <PowerSettingsNewIcon/>
                        </IconButton>
                    </div>
                ) : (
                    <Tooltip title='login via Google'>
                        <IconButton onClick={actions.startLoginWithGoogle}>
                            <AccountCircleIcon className='onHoverHide accountCircleIcon'/>
                            <GoogleIcon className='onHoverShow googleIcon'/>
                        </IconButton>
                    </Tooltip>
                )}
            </span>
        )
    }
}

const mapStateToProps = (state: IAppState) => state.loginState;

const mapDispatchToProps = (dispatch: ThunkDispatch<IAppState, void, Action>) => {
    return {
        actions: new LoginActions(dispatch),
        notificationActions: new NotificationActions(dispatch)
    };
};

export const Login = HOC<{}, ILoginStateProps, TDispatchProps, TStyleProps, {}>(
    LoginComponent,
    {
        mapStateToProps,
        mapDispatchToProps,
        styles: LoginStyles
    }
);

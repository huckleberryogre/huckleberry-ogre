import {Theme, createStyles} from '@material-ui/core';

export const LoginStyles = (theme: Theme) => createStyles({
    iconButton: {
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(2)
        },
    },
    loginContainer: {
        color: 'white',
        width: '300px',
        textAlign: 'right',
        lineHeight: '64px',

        [theme.breakpoints.down('sm')]: {
            width: '56px',
        },
        '& .logoutIcon': {
            color: 'white'
        },
        '& .googleIcon': {
            width: theme.spacing(6),
        },
        '& .accountCircleIcon': {
            transform: 'scale(1.4)'
        },
        '& .onHoverHide': {
            transition: 'opacity .3s ease',
        },
        '& .onHoverShow': {
            opacity: 0,
            transition: 'opacity .3s ease',
            visibility: 'hidden',
            pointerEvents: 'none',
            position: 'absolute',
            right: '-2000px',
        },
        '&:hover': {
            '& .onHoverHide': {
                opacity: 0,
                visibility: 'hidden',
                pointerEvents: 'none',
                position: 'absolute',
                right: '-2000px',
            },
            '& .onHoverShow': {
                opacity: 1,
                visibility: 'visible',
                pointerEvents: 'auto',
                position: 'relative',
                right: '0'
            }
        },
    }
});

import {Action} from 'redux';
import {IAppState} from '../../core/reduxStore';
import {ThunkDispatch} from 'redux-thunk';

/**
 * Action types.
 */
export const SET_IS_WEBP_SUPPORTED = 'SET_IS_WEBP_SUPPORTED';

/**
 * App settings actions.
 */
export class AppSettingsActions {
    constructor(private dispatch: ThunkDispatch<IAppState, void, Action>) {
    }

    /**
     * Sets isWebpSupported flag.
     *
     * @param isWebpSupported Is webp supported flag.
     */
    setIsWebpSupported = (isWebpSupported: boolean) => {
        this.dispatch({
            type: SET_IS_WEBP_SUPPORTED,
            payload: isWebpSupported
        });
    };
}

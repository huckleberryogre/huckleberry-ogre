import {Reducer} from 'redux';
import {SET_IS_WEBP_SUPPORTED} from './actions';

export interface IAppSettingsStateProps {
    isWebpSupported: boolean;
}

/**
 * Initial app settings state.
 */
export const initAppSettingsState: IAppSettingsStateProps = {
    isWebpSupported: false,
};

/**
 * App settings reducer.
 */
export const appSettingsState: Reducer<IAppSettingsStateProps> = (state: IAppSettingsStateProps = initAppSettingsState, action) => {
    switch (action.type) {
        case SET_IS_WEBP_SUPPORTED:
            return {
                ...state,
                isWebpSupported: action.payload
            };
        default:
            return state;
    }
};

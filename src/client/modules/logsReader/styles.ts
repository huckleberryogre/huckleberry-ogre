import {Theme, createStyles} from '@material-ui/core';

export const LogsReaderStyles = (__: Theme) => createStyles({
    logsText: {
        whiteSpace: 'pre-wrap',
        padding: '24px'
    },
    refreshButton: {
        position: 'fixed',
        right: '45px',
    }
});

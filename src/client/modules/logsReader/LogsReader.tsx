import {
    Grid,
    IconButton,
    Paper,
    Typography,
    WithStyles,
} from '@material-ui/core';
import {Refresh as RefreshIcon} from '@material-ui/icons';
import React from 'react';
import {HOC} from '../../core/utils/HOC';
import {get, IResponseText} from '../../core/utils/ServiceUtils';
import {LogsReaderStyles} from "./styles";

type TStyleProps = WithStyles<typeof LogsReaderStyles>;

/**
 * Logs page.
 */
class LogsReaderComponent extends React.Component<TStyleProps, {logs: string}> {

    state = {
        logs: ''
    };

    componentDidMount(): void {
        this.handleLogsRefresh();
    }

    handleLogsRefresh = () => {
        get<IResponseText>('logs').then(response => {
            this.setState({logs: response.text})
        });
    };

    render() {
        const {logs} = this.state;
        const {classes} = this.props;

        return (
            <Paper>
                <Grid container>
                    <Grid item xs={12}>
                        <IconButton onClick={this.handleLogsRefresh} color={'primary'} className={classes.refreshButton}>
                            <RefreshIcon/>
                        </IconButton>
                        <Typography color="textPrimary" variant="h6" className={classes.logsText}>
                            {logs}
                        </Typography>
                    </Grid>
                </Grid>
            </Paper>
        );
    }
}

export const LogsReader = HOC<{}, {}, {}, TStyleProps, {}>(
    LogsReaderComponent,
    {
        mapStateToProps: null,
        mapDispatchToProps: null,
        styles: LogsReaderStyles,
        isWithRouter: false
    }
);

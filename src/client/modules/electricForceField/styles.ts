import {Theme, createStyles} from '@material-ui/core';

export const ElectricForceFieldStyles = (__: Theme) => createStyles({
    field: {
        width: '100%',
        height: '100%',
        position: 'absolute'
    },
    forceLine: {
        pointerEvents: 'none'
    }
});

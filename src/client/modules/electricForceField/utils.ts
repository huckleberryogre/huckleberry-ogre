import {LINE_INCREMENT, START_DELTA} from './consts';
import {IChargeData, IPoint} from './ElectricForceField';
import {EAxis} from './enums';

/**
 * Calculate distance between two points. It can't be less then LINE_INCREMENT (to avoid edge cases).
 *
 * @param pointA First point.
 * @param pointB Second point.
 */
export const getDistance = (pointA: IPoint, pointB: IPoint) => {
    return Math.pow((Math.pow((pointA.x - pointB.x), 2) + Math.pow((pointA.y - pointB.y), 2)), 0.5);
};

/**
 * Calculate electric field value from single charge for target point.
 *
 * @param targetPoint Target point.
 * @param charge Charge.
 */
export const getElectricFieldFromSingleCharge = (targetPoint: IPoint, charge: IChargeData) => {
    const distance = getDistance(targetPoint, charge);

    return charge.value / Math.pow(distance, 2);
};

/**
 * Calculate cos(alpha) between Ox axis and line that goes through charge and target point.
 * If distance between them is 0 or less then axis projection distance, considering alpha is also 0 (and cos = 1).
 *
 *             .Target point
 *            /
 *           /
 *          / ) <- alpha
 * charge C ------------> Ox
 *
 * @param targetPoint Target point.
 * @param charge Single charge.
 */
export const getCosWithSingleCharge = (targetPoint: IPoint, charge: IChargeData) => {
    const distance = getDistance(targetPoint, charge);
    const distanceX = targetPoint.x - charge.x;

    return distanceX / distance;
};

/**
 * Calculate sin(alpha) between Ox axis and line that goes through charge and target point.
 * If distance between them is 0 or less then axis projection distance, considering alpha is also 0 (and sin = 0).
 *
 *             .Target point
 *            /
 *           /
 *          / ) <- alpha
 * charge C ------------> Ox
 *
 * @param targetPoint Target point.
 * @param charge Single charge.
 */
export const getSinWithSingleCharge = (targetPoint: IPoint, charge: IChargeData) => {
    const distance = getDistance(targetPoint, charge);
    const distanceY = targetPoint.y - charge.y;

    return distanceY / distance;
};

/**
 * Calculates total electric field from all nearby charges for given point.
 *
 * @param targetPoint Target point on which electric field is calculated.
 * @param charges All nearby charges.
 */
export const totalElectricField = (targetPoint: IPoint, charges: IChargeData[]) => charges.reduce(
    (total, charge) => total + Math.abs(getElectricFieldFromSingleCharge(targetPoint, charge)),
    0
);

/**
 * Calculates total electric field from all nearby charges for given point along Ox.
 *
 * @param targetPoint Target point on which electric field is calculated.
 * @param charges All nearby charges.
 */
export const totalElectricFieldX = (targetPoint: IPoint, charges: IChargeData[]) => {
    return charges.reduce(
        (total, charge) => total + getElectricFieldFromSingleCharge(targetPoint, charge) * getCosWithSingleCharge(targetPoint, charge),
        0
    );
};

/**
 * Calculates total electric field from all nearby charges for given point along Oy.
 *
 * @param targetPoint Target point on which electric field is calculated.
 * @param charges All nearby charges.
 */
export const totalElectricFieldY = (targetPoint: IPoint, charges: IChargeData[]) => {
    return charges.reduce(
        (total, charge) => total + getElectricFieldFromSingleCharge(targetPoint, charge) * getSinWithSingleCharge(targetPoint, charge),
        0
    );
};

/**
 * Calculate cos(alpha) between Ox axis and accumulative force field vector.
 * If accumulative force field vector length equals 0 or less then it's projection length,
 * considering alpha equals 0 (and cos = 1).
 *
 * @param targetPoint Target point on which electric field is calculated.
 * @param charges All nearby charges.
 */
export const getCosWithAllCharges = (targetPoint: IPoint, charges: IChargeData[]) => {
    return totalElectricFieldX(targetPoint, charges) / totalElectricField(targetPoint, charges);
};
/**
 * Calculate sin(alpha) between Ox axis and accumulative force field vector.
 * If accumulative force field vector length equals 0 or less then it's projection length,
 * considering alpha equals 0 (and sin = 0).
 *
 * @param targetPoint Target point on which electric field is calculated.
 * @param charges All nearby charges.
 */
export const getSinWithAllCharges = (targetPoint: IPoint, charges: IChargeData[]) => {
    return totalElectricFieldY(targetPoint, charges) / totalElectricField(targetPoint, charges);
};

/**
 * Calculate next point coordinates.
 *
 * @param forCharge Root charge for this line.
 * @param point Current point.
 * @param chargeSet All charges.
 * @param axis Along which axis to calculate.
 */
export const getNextPoint = (forCharge: IChargeData, point: IPoint, chargeSet: IChargeData[], axis: EAxis) => {
    switch (axis) {
        case EAxis.X:
            return point[axis] + Math.sign(forCharge.value) * LINE_INCREMENT * getCosWithAllCharges(point, chargeSet);
        case EAxis.Y:
            return point[axis] + Math.sign(forCharge.value) * LINE_INCREMENT * getSinWithAllCharges(point, chargeSet);
    }
};

/**
 * Calculate next point coordinates.
 *
 * @param forCharge Root charge for this line.
 * @param point Current point.
 * @param chargeSet All charges.
 */
export const isPointCloseToCharge = (forCharge: IChargeData, point: IPoint, chargeSet: IChargeData[]) => {
    return chargeSet
        .filter(
            charge => !(charge.x === forCharge.x && charge.y === forCharge.y)
        ).some(
            charge => getDistance(point, charge) < START_DELTA
        );
};


/**
 * Axis.
 */
export enum EAxis {
    X = 'x',
    Y = 'y'
}

/**
 * Side.
 */
export enum ESide {
    LEFT = 'left',
    TOP = 'top',
    RIGHT = 'right',
    BOTTOM = 'bottom'
}
import React, {Suspense} from "react";

/**
 * Lazy loaded ElectricForceField component.
 */
export const ElectricForceFieldAsync = () => {
    const ElectricForceField = React.lazy(() => import('./ElectricForceField'));

    return (
        <Suspense fallback={<div>Component loading...</div>}>
            <ElectricForceField/>
        </Suspense>
    );
};

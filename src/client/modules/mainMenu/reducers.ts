import {Reducer} from 'redux';
import {
    MAIN_MENU_DRAWER_OPEN,
    MAIN_MENU_DRAWER_CLOSE
} from './actions';
import {IMainMenuStateProps} from './MainMenu';

/**
 * Initial blog state.
 */
export const initMainMenuState: IMainMenuStateProps = {
    isDrawerOpen: false
};

/**
 * MainMenu reducer.
 */
export const mainMenuState: Reducer<IMainMenuStateProps> = (state: IMainMenuStateProps = initMainMenuState, action) => {
    switch (action.type) {
        case MAIN_MENU_DRAWER_OPEN:
            return {
                isDrawerOpen: true
            };
        case MAIN_MENU_DRAWER_CLOSE:
            return {
                isDrawerOpen: false
            };
        default:
            return state;
    }
};
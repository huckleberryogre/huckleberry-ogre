import {
    Divider,
    Drawer,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Typography,
    WithStyles,
} from '@material-ui/core';
import {
    BlurOn as BlurOnIcon,
    Subject as SubjectIcon,
    BorderColor as BorderColorIcon,
    CloudDownload as CloudDownloadIcon,
    ChevronLeft as ChevronLeftIcon,
    Timeline as TimelineIcon,
} from '@material-ui/icons';
import React from 'react';
import {Action} from 'redux';
import {ThunkDispatch} from 'redux-thunk';
import {ILoginStateProps} from '../login/Login';
import {IAppState, navigateTo} from '../../core/reduxStore';
import {HOC} from '../../core/utils/HOC';
import {MainMenuActions} from "./actions";
import {FilesManagerActions} from "../filesManager/actions";
import {MainMenuStyles} from "./styles";
import {ERole} from "../../core/enums";

/**
 * Main menu own props.
 *
 * isDrawerOpen Drawer open flag.
 */
export interface IMainMenuStateProps {
    isDrawerOpen: boolean;
}

type TDispatchProps = {
    mainMenuActions: MainMenuActions;
    filesManagerActions: FilesManagerActions;
}

type TStyleProps = WithStyles<typeof MainMenuStyles>;

/**
 * Navigation component.
 */
class MainMenuComponent extends React.Component<IMainMenuStateProps & ILoginStateProps & TDispatchProps & TStyleProps> {

    /**
     * Menu link click handler.
     *
     * @param newLocation URL path part.
     * @param event Event.
     */
    handleListItemClick = (newLocation: string = '/', event: React.SyntheticEvent) => {
        event.preventDefault();
        this.props.mainMenuActions.drawerClose();
        navigateTo(newLocation);
    };

    /**
     * Menu list rendering.
     */
    getNavigationList = () => {
        const {classes, user, filesManagerActions} = this.props;
        const isAdmin = user && user.role === ERole.ADMIN;

        return (
            <List className={classes.list}>
                {isAdmin && (
                    <ListItem
                        className={classes.listItem}
                        onClick={filesManagerActions.show}
                    >
                        <ListItemIcon>
                            <CloudDownloadIcon/>
                        </ListItemIcon>
                        <ListItemText className={classes.listItemText} primary="Files"/>
                    </ListItem>
                )}
                <Divider/>
                <ListItem
                    className={classes.listItem}
                    onClick={(e) => this.handleListItemClick('/blog', e)}
                >
                    <ListItemIcon>
                        <BorderColorIcon/>
                    </ListItemIcon>
                    <ListItemText className={classes.listItemText} primary="Blog"/>
                </ListItem>
                <Divider/>
                <ListItem
                    className={classes.listItem}
                    onClick={(e) => this.handleListItemClick('/electricForceField', e)}
                >
                    <ListItemIcon>
                        <BlurOnIcon/>
                    </ListItemIcon>
                    <ListItemText className={classes.listItemText} primary="Charge field drawer"/>
                </ListItem>
                <Divider/>
                {isAdmin && (
                    <>
                        <ListItem
                            className={classes.listItem}
                            onClick={(e) => this.handleListItemClick('/status', e)}
                        >
                            <ListItemIcon>
                                <TimelineIcon/>
                            </ListItemIcon>
                            <ListItemText className={classes.listItemText} primary="Status monitor"/>
                        </ListItem>
                        <Divider/>
                    </>
                )}
                {isAdmin && (
                    <>
                        <ListItem
                            className={classes.listItem}
                            onClick={(e) => this.handleListItemClick('/logs', e)}
                        >
                            <ListItemIcon>
                                <SubjectIcon/>
                            </ListItemIcon>
                            <ListItemText className={classes.listItemText} primary="Logs"/>
                        </ListItem>
                        <Divider/>
                    </>
                )}
            </List>
        );
    };

    render() {
        const {classes, isDrawerOpen, mainMenuActions} = this.props;

        return (

            <Drawer
                variant="persistent"
                classes={{
                    paper: classes.drawerPaper,
                    docked: classes.drawerDocked
                }}
                open={isDrawerOpen}
            >
                <div>
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={mainMenuActions.drawerClose}>
                            <ChevronLeftIcon/>
                        </IconButton>
                    </div>
                    <Divider/>
                    {this.getNavigationList()}
                </div>
                <Typography align="center" className={classes.applicationVersion} color="textSecondary" variant="h6">
                    {`version ${APPLICATION_VERSION}`}
                </Typography>
            </Drawer>
        );
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<IAppState, void, Action>) => {
    return {
        mainMenuActions: new MainMenuActions(dispatch),
        filesManagerActions: new FilesManagerActions(dispatch),
    };
};

const mapStateToProps = (state: IAppState) => ({
    ...state.loginState,
    isDrawerOpen: state.mainMenuState.isDrawerOpen
});

export const MainMenu = HOC<{}, IMainMenuStateProps, TDispatchProps, TStyleProps, {}>(
    MainMenuComponent,
    {
        mapStateToProps,
        mapDispatchToProps,
        styles: MainMenuStyles,
        isWithRouter: false
    }
);

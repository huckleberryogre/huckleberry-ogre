import {Theme, createStyles} from '@material-ui/core';

/** Menu width. */
export const DRAWER_WIDTH = 240;

export const MainMenuStyles = (theme: Theme) => createStyles({
    drawerPaper: {
        position: 'relative',
        height: 'auto',
        width: DRAWER_WIDTH,
        borderRight: '0px !important',
    },
    drawerDocked: {
        display: 'flex',
        backgroundColor: theme.palette.background.paper
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        height: '64px',
        // ...ApplicationTheme.mixins.toolbar,
    },
    applicationVersion: {
        marginTop: 'auto',
        padding: `${theme.spacing(2)}px 0`,
    },
    listItemText: {
        display: 'inline-block',
        userSelect: 'none',
        padding: '0 8px',
    },
    listItem: {
        padding: '8px 16px !important',
        whiteSpace: 'nowrap',
        '&:hover': {
            backgroundColor: theme.palette.primary.light,
            cursor: 'pointer'
        }
    },
    list: {
        width: '100%',
        padding: '0'
    }
});

import {Action} from 'redux';
import {ThunkDispatch} from 'redux-thunk';
import {IAppState} from '../../core/reduxStore';

/**
 * Action types.
 */
export const MAIN_MENU_DRAWER_OPEN = 'MAIN_MENU_DRAWER_OPEN';
export const MAIN_MENU_DRAWER_CLOSE = 'MAIN_MENU_DRAWER_CLOSE';

/**
 * Main menu actions.
 */
export class MainMenuActions {
    constructor(private dispatch: ThunkDispatch<IAppState, void, Action>) {
    }

    /**
     * Drawer open.
     */
    drawerOpen = () => this.dispatch({
        type: MAIN_MENU_DRAWER_OPEN
    });

    /**
     * Drawer close.
     */
    drawerClose = () => this.dispatch({
        type: MAIN_MENU_DRAWER_CLOSE
    });

}

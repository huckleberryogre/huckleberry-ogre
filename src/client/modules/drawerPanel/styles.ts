import {Theme, createStyles} from '@material-ui/core';

export const DrawerPanelStyles = (theme: Theme) => createStyles({
    drawer: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
    },
    panel: {
        width: '100%',
        minHeight: '50vh',
        position: 'absolute',
        bottom: 0,
    },
    handle: {
        top: -10,
        position: 'absolute',
        right: 0,
        width: 50,
        '& svg': {
            fontSize: 50,
            color: theme.palette.text.primary,
        }
    },
    bookmark: {
        position: 'absolute'
    },
    overlay: {}
});

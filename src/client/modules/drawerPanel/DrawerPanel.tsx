import React from 'react';
import {
    WithStyles
} from '@material-ui/core';
import {DrawerPanelStyles} from './styles';
import {HOC} from '../../core/utils/HOC';

/**
 * State.
 *
 * @prop {EDrawerState} drawerState Drawer state.
 */
interface IState {
    drawerState: EDrawerState;
}

/**
 * Drawer state.
 */
enum EDrawerState {
    OPEN,
    CLOSED
}

type TStyleProps = WithStyles<typeof DrawerPanelStyles>;

/**
 * Drawer panel.
 * todo: develop settings section https://huckleberrydev.atlassian.net/browse/HBOG-30
 */
class DrawerPanelComponent extends React.Component<TStyleProps, IState> {

    state: IState = {
        drawerState: EDrawerState.CLOSED
    };

    render() {
        const {drawer, overlay, handle, panel, bookmark} = this.props.classes;
        bookmark;

        return (
            <div className={drawer}>
                <div className={panel}/>
                <div className={handle}>
                        {/*<BookmarkCustomIcon className={bookmark}/>*/}
                </div>



                <div className={overlay}/>
            </div>
        );
    }
}

export const DrawerPanel = HOC<{}, {}, {}, TStyleProps, {}>(
    DrawerPanelComponent,
    {
        mapStateToProps: null,
        mapDispatchToProps: null,
        styles: DrawerPanelStyles
    }
);

import {ERole} from "./server/enums";

/**
 * Common field set for any model.
 *
 * {number} id Unique identifier
 * {string} createdAt Creation timestamp.
 * {string} updatedAt Last update timestamp.
 */
export interface IModelCommonFields {
    id?: number;
    createdAt?: string;
    updatedAt?: string;
}

/**
 * Blog post model.
 *
 * {string} title Heading of post.
 * {string} message Body of post.
 */
export interface IPost extends IModelCommonFields {
    title?: string;
    message?: string;
}

/**
 * Post model extended.
 *
 * {string} tags Tags.
 */
export interface IPostExtended extends IPost {
    tags?: ITag[];
}

/**
 * Fields of file model.
 *
 * @prop {string} name Name.
 * @prop {string} uuid Uuid.
 * @prop {string} hash Hash.
 * @prop {meta} meta Metadata of the file. Currently only EXIF data supported (if provided). An object within a string (JSON.stringify).
 */
export interface IFile extends IModelCommonFields {
    name: string;
    uuid: string;
    hash: string;
    meta: string;
}

/**
 * Fields of user model.
 *
 * @prop {string} name User name.
 * @prop {string} email User email.
 * @prop {string} avatarURL User avatar link.
 * @prop {string} googleId Google profile id.
 * @prop {ERole} role Hash.
 * @prop {string} uuid Unique id used for authentication.
 */
export interface IUser extends IModelCommonFields {
    name: string;
    email: string;
    avatarURL: string;
    googleId: string;
    role: ERole;
    uuid: string;
}

/**
 * Fields of tag model.
 *
 * @prop {string} name Tag name.
 */
export interface ITag extends IModelCommonFields {
    name: string;
}

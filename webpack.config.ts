import webpack from 'webpack';
import {isEmpty} from 'lodash';
import path from 'path';
import MinifyPlugin from 'babel-minify-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import DotEnv from 'dotenv';
import Package from './package.json';
import {getAppConfig} from "./src/client.config";

/**
 * Paths to frontend directories and files.
 */
const tsFrontDirs = [
    path.resolve(__dirname, 'src/client'),
    path.resolve(__dirname, 'src/server/db/models.ts')
];

/**
 * Currently running script (from npm environment variables).
 */
const runningScript = process.env.npm_lifecycle_script;

/**
 * Way to detect if webpack runs for stats generation mode (with --json flag).
 * E.g. this script is running: webpack --mode=production --profile --json > stats.json
 *
 */
const isStats = runningScript && runningScript.indexOf('--json') > -1;

/**
 * Webpack config.
 */
export const getWebpackConfiguration = (isProduction: boolean): webpack.Configuration => {

    /**
     * Environment level properties.
     * Can be accessed at compile-time across any file via process.env.VARIABLE_NAME syntax on server
     * or from AppConfig global variable on client.
     *
     * See corresponding .env files.
     */
    const environmentConfig = DotEnv.config().parsed;

    if (isEmpty(environmentConfig)) {
        throw Error('Client build failed: .env not found or empty');
    }

    /**
     * Running mode.
     */
    const mode = isProduction ? 'production' : 'development';

    const configuration: webpack.Configuration = {
        mode,
        stats: {
            colors: true
        },
        entry: './src/client/index.tsx',
        devtool: 'inline-source-map',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'bundle.js',
            publicPath: '/'
        },
        optimization: {
          splitChunks: {
              chunks: "all"
          }
        },
        resolve: {
            //ToDo: investigate, why aliases doesn't work on linux (commit number b9b465633e9a94131798464a02f54ac7b579d0df reverted)
            modules: [path.resolve(__dirname, 'src'), 'node_modules'],
            extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
        },
        module: {
            rules: [{
                enforce: 'pre',
                test: /\.ts|\.tsx$/,
                loader: 'source-map-loader',
                include: [path.resolve(__dirname, 'src/client'), path.resolve(__dirname, 'src/server/db/models.ts')]
            }, {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }, {
                test: /\.(eot|ttf|woff|woff2)$/,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            }, {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name]_[hash].[ext]'
                }
            }, {
                test: /\.ts|\.tsx$/,
                loader: 'awesome-typescript-loader',
                options: {
                    configFileName: path.resolve(__dirname, 'src/client/tsconfig.json'),
                    /** need to mute console output to produce proper json (for webpack stats). */
                    silent: isStats
                },
                include: tsFrontDirs,
                exclude: /node_modules/
            }]
        },
        plugins: [
            new CleanWebpackPlugin(['dist']),
            new HtmlWebpackPlugin({
                title: 'Huckleberry Ogre welcomes you, stranger!',
                template: 'index_template.ejs'
            }),
            new webpack.DefinePlugin({
                // do not remove JSON.stringify - only way to pass a string here
                APPLICATION_VERSION: JSON.stringify(Package.version),
                AppConfig: JSON.stringify(getAppConfig(environmentConfig))
            })
        ]
    };

    if (isProduction) {
        configuration.plugins.push(new MinifyPlugin());
    }

    return configuration;
};

/**
 * Development version by default.
 */
export default getWebpackConfiguration(false);
